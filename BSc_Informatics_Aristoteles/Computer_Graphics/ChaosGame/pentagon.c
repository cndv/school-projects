#include <GLUT/GLUT.h>
#include <OpenCL/OpenCL.h>
#include <stdlib.h>
#include <stdio.h>

//globals
int count = 0;
int ww =1000.0, wh=1000.0;
int initialX,initialY;
int currentPoints=50000;
//declarations
void myinit(int right,int top);
void display(int points);
void menu(int id);
void mouse(int btn, int state, int x, int y);
void changeColorAndSize(int id);
void drawChaos (int x , int y);

void 
drawChaos (int x , int y)
{   
    int dfX=x-initialX,dfY=initialY-y;
    typedef GLfloat point2[2];     
    point2 vertices[6] = {  {250.0+dfX , 250.0+dfY },  
                            {125.0 +dfX, 750.0+dfY},
                            {500.0 +dfX, 1000.0 +dfY}, 
                            {875.0 +dfX, 750.0 +dfY}, 
                            {750.0+dfX, 250.0 +dfY}, 
                            };
    int j, k;
    //long rand();       /* standard random number generator */
    point2 p ={75.0,75.0};  /* An srbitrary initial point */
    glClear(GL_COLOR_BUFFER_BIT);  /*clear the window */
    /* computes and plots 50000 new points */
    int totalPoints;
    if (count == 0)
    {
        totalPoints = 50000;//initial points
    }
    else
        totalPoints = currentPoints;
    
    for( k=0; k<totalPoints; k++)
    {
        j=rand()%5; /* pick a vertex at random */
        /* Compute point halfway between vertex and old point */
        p[0] = (p[0]+vertices[j][0])*3/8.0; 
        p[1] = (p[1]+vertices[j][1])*3/8.0;
        /* plot new point */
        glBegin(GL_POINTS);
        glVertex2fv(p); 
        glEnd();
    }
    glClearColor (0.0, 0.0, 0.0, 1.0);
    
    glFlush(); /* clear buffers */
    count++;
    
}

void 
mouse(int btn, int state, int x, int y)
{
    if (btn == GLUT_LEFT_BUTTON && state == GLUT_DOWN) 
    {
        initialX=x;
        initialY=y;
    }
}

void 
changeColorAndSize(int id)
{
    if (id ==0) 
    {
        glColor3ub( (char) random()%256, (char) random()%256, (char) random()%256); 
        currentPoints = 100000;
        display(currentPoints);
    }
    else if (id == 1) 
    {
        glColor3ub( (char) random()%256, (char) random()%256, (char) random()%256); 
        currentPoints = 200000;
        display(currentPoints);
    }
    else if (id == 2) 
    {
        glColor3ub( (char) random()%256, (char) random()%256, (char) random()%256); 
        currentPoints = 400000;
        display(currentPoints);
    }    
}

void 
menu(int id)
{
    if(id ==3 ) exit(0);
    else changeColorAndSize(id);
}

void 
myinit(int right, int top)
{ 
    /* attributes */
    glEnable(GL_BLEND);
    glClearColor( .0, .0, .0, .0); /* black background */
    glColor3ub( (char) random()%256, (char) random()%256, (char) random()%256);     
    /* set up viewing */
    /* 500 x 500 window with origin lower left */
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, right, 0.0, top);
    glMatrixMode(GL_MODELVIEW);
}

void 
display( int points )
{
    /* define a point data type */
    typedef GLfloat point2[2];     
    point2 vertices[5] = {  {250.0, 250.0},  
                            {125.0, 750.0},
                            {500.0 , 1000.0}, 
                            {875.0 , 750.0}, 
                            {750.0, 250.0},                             
                            };
    int j, k;
    //long rand();       /* standard random number generator */
    point2 p ={75.0,75.0};  /* An srbitrary initial point */
    glClear(GL_COLOR_BUFFER_BIT);  /*clear the window */
    /* computes and plots 5000 new points */
    int totalPoints;
    if (count == 0)
    {
        totalPoints = 50000;
    }
    else
        totalPoints = points;
    
    for( k=0; k<totalPoints; k++)
    {
        j=rand()%5; /* pick a vertex at random */
        /* Compute point halfway between vertex and old point */
        p[0] = (p[0]+vertices[j][0])*3/8.0; 
        p[1] = (p[1]+vertices[j][1])*3/8.0;
        /* plot new point */
        glBegin(GL_POINTS);
        glVertex2fv(p); 
        glEnd();
    }
    glFlush(); /* clear buffers */
    count++;
}

int 
main(int argc, char** argv)
{
    /* Standard GLUT initialization */
    glutInit(&argc,argv);
    // glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB); /* default, not needed */
    glutInitWindowSize(1000,1000); /* 1000 x 1000 pixel window */
    glutInitWindowPosition(0,0); /* place window top left on display */
    glutCreateWindow("Chaos Game"); /* window title */
    glutMouseFunc(mouse);
    glutDisplayFunc(display); /* display callback invoked when window opened */
    glutMotionFunc (drawChaos);
    
    myinit(ww,wh); /* set attributes */	
    
    glutCreateMenu(menu);
    glutAddMenuEntry("100000", 0);
    glutAddMenuEntry("200000", 1);
    glutAddMenuEntry("400000", 2);
    glutAddMenuEntry("quit", 3);
    glutAttachMenu(GLUT_RIGHT_BUTTON);
    
    glutMainLoop(); /* enter event loop */
}