/*Konstantinos Chartomatzis 1553 kchartom@csd.auth.gr*/
#include <GLUT/GLUT.h>    

#include <stdio.h>      
#include <stdlib.h>     
#include <math.h>

#define WIDTH  500
#define HEIGHT 500

GLuint face;
GLuint cube;
int scaling = 1;
static GLfloat theta[] = {0.0,0.0,0.0};
static GLint axis = 1;
float time = 100;// time has passed
GLfloat scaleX=0, scaleY=0, scaleZ=0;

void init_scene();
void render_scene();
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid spinCube(void);
GLvoid initGL();
void drawCube(int sideSize);
void drawFace(int sideSize);

void 
spinCube(void)
{
	theta[axis] += .1;
	if( theta[axis] > 360.0 )
    { 
        theta[axis] -= 360.0;
    }
    if (scaling == 1) 
    {
        time += .3;
        if(time > 360)
        {
            scaling = 0;
        }
    }
    else if (scaling == 0)
    {
        time -=.3;
        if (time < 100)
        {
            scaling = 1;
        }
    }
    glFlush();
    glutPostRedisplay();
}

GLvoid 
initGL()	
{
    glClearDepth(1.0);				
    glDepthFunc(GL_LESS);                       
    glEnable(GL_DEPTH_TEST);                    
}

void 
drawFace(int sideSize)
{
    face = glGenLists(2);
    glNewList(face, GL_COMPILE);
    glBegin(GL_POLYGON);
    glVertex3f(0, 0, 0);
    glVertex3f(sideSize, 0, 0);
    glVertex3f(sideSize, sideSize, 0);
    glVertex3f(0, sideSize, 0);  
    glEnd();  
    glEndList();
}

void
drawCube(int sideSize)
{
    int scaleFactor = sideSize/2;
    cube = face+1;
    
    glNewList(cube, GL_COMPILE);
    
    glTranslatef(-.5*sideSize, -.5*sideSize, .5*sideSize);
    glColor3f(.3, 0, 0.1);
    glPushMatrix();
    glScalef(scaleFactor, scaleFactor, scaleFactor);
    glCallList(face);
    glPopMatrix();
    
    glColor3f(.5, .2, 0);
    glPushMatrix();
    glTranslatef(0, 0, -1*sideSize);
    glScalef(scaleFactor, scaleFactor, scaleFactor);
    glCallList(face);
    glPopMatrix();
    
    glColor3f(.8, .1, 0.1);
    glPushMatrix();
    glRotatef(90, 0, 1, 0);
    glScalef(scaleFactor, scaleFactor, scaleFactor);
    glCallList(face);
    glPopMatrix();    
    
    glColor3f(0, 0.3, .5);
    glPushMatrix();
    glTranslatef(1*sideSize, 0, 0);
    glRotatef(90, 0, 1, 0);
    glScalef(scaleFactor, scaleFactor, scaleFactor);
    glCallList(face);
    glPopMatrix();
    
    glColor3f(0, .2, .1);
    glPushMatrix();
    glRotatef(-90, 1, 0, 0);
    glScalef(scaleFactor, scaleFactor, scaleFactor);
    glCallList(face);
    glPopMatrix();
    
    glColor3f(.5, .5, .1);
    glPushMatrix();
    glTranslatef(0, 1*sideSize, 0);
    glRotatef(-90, 1, 0, 0);
    glScalef(scaleFactor, scaleFactor, scaleFactor);
    glCallList(face);
    glPopMatrix();
    
    glEndList();
}

void 
init_scene()
{
    drawFace(2);
    drawCube(18);
}

GLvoid 
window_display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	    
    glLoadIdentity();
    gluLookAt(0, 0, -95, 0, 0, 0, 0, 1, 0); 
    render_scene();
}

GLvoid 
window_reshape(GLsizei width, GLsizei height)
{
    if (height == 0)
    { 
        height = 1; 
    }
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45, (GLdouble)width/(GLdouble)height, 1, 10); 
    glMatrixMode(GL_MODELVIEW);
}

void 
render_scene()
{
    glPushMatrix();
    scaleX = scaleY = scaleZ = time/3000;
    glTranslatef(0, 0, -90);
    glRotatef(theta[1], 1.0, 2.0, 6.0);
    glScalef(scaleX, scaleY, scaleZ);
    glCallList(cube);
    glPopMatrix();
    glutSwapBuffers();
}

int 
main(int argc, char **argv) 
{  
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Single Cube");
    initGL();  
    init_scene();
    glutDisplayFunc(window_display);
    glutReshapeFunc(window_reshape);
    glutIdleFunc(spinCube);
    glutMainLoop();  
    return 1;
}

