/*Konstantinos Chartomatzis 1553 kchartom@csd.auth.gr*/
#include <GLUT/GLUT.h>    

#include <stdio.h>      
#include <stdlib.h>     
#include <math.h>

#define WIDTH  500
#define HEIGHT 500

GLuint face, terrain;
GLuint house, triangle;
int scaling = 1;
int idGlobal;
static GLfloat theta[] = {0.0,0.0,0.0};
static GLint axis = 1;
float time = 100;// time has passed 
GLfloat scaleX=0, scaleY=0, scaleZ=0;

void init_scene();
void render_scene(int id);
GLvoid initGL();
GLvoid window_display();
GLvoid window_reshape(GLsizei width, GLsizei height);
GLvoid spinCube(void);
GLvoid initGL();
void drawHouse(int sideSize);
void drawFace(int sideSize);
void drawTriangle();
void drawTerrain(int sideSize);
void menu(int id);


void 
spinCube(void)
{
	theta[axis] += .1;
	if( theta[axis] > 360.0 )
    { 
        theta[axis] -= 360.0;
    }
    if (scaling == 1) 
    {
        time += .3;
        if(time > 360)
        {
            scaling = 0;
        }
    }
    else if (scaling == 0)
    {
        time -=.3;
        if (time < 100)
        {
            scaling = 1;
        }
    }
    glFlush();
    glutPostRedisplay();
}

GLvoid 
initGL()	
{
    glClearDepth(1.0);				
    glDepthFunc(GL_LESS);                       
    glEnable(GL_DEPTH_TEST);                    
}

void 
drawFace(int sideSize)
{
    face = glGenLists(3);
    glNewList(face, GL_COMPILE);
    glBegin(GL_POLYGON);
    glVertex3f(0, 0, 0);
    glVertex3f(sideSize, 0, 0);
    glVertex3f(sideSize, sideSize, 0);
    glVertex3f(0, sideSize, 0);  
    glEnd();  
    glEndList();
}

void 
drawTriangle()
{
    triangle = face+2;
    glNewList(triangle, GL_COMPILE);
    glBegin(GL_TRIANGLES);
    glVertex3f(0, 0, 0);
    glVertex3f(1, 0, 0);
    glVertex3f(.5, .88, 0);
    glEnd();  
    glEndList();
    
}

void
drawHouse(int sideSize)
{
    int cuboidLength = 20;
    house = face+1;
    glNewList(house, GL_COMPILE);
    

    
    //edafos
    glPushMatrix();
    glTranslatef(-0.5*50, -0.5*sideSize, .5*50);
    glColor3f(0, 1, 0);
    glScalef(50, 50, 50);
    //glTranslatef(-.5*50, -.5*50, 0);
    glRotatef(-90, 1, 0, 0);

    glCallList(face);
    glPopMatrix();

    glTranslatef(-0.5*sideSize, -0.5*sideSize, .5*cuboidLength);
    
    //mpros
    glPushMatrix();    
    glColor3f(.3, 0, 0.1);
    glScalef(sideSize, sideSize, sideSize);
    glCallList(face);
    glPopMatrix();
    
    //piso
    glPushMatrix();
    glColor3f(.5, .2, 0);
    glTranslatef(0, 0, -1*cuboidLength);
    glScalef(sideSize, sideSize, sideSize);
    glCallList(face);
    glPopMatrix();
    
    //aristera
    glPushMatrix();
    glColor3f(.8, .1, .1);
    glRotatef(90, 0, 1, 0);
    glScalef(cuboidLength, sideSize, sideSize);
    glCallList(face);
    glPopMatrix();    
    
    //deksia
    glPushMatrix();
    glColor3f(0, .3, .5);
    glTranslatef(sideSize, 0, 0);
    glRotatef(90, 0, 1, 0);
    glScalef(cuboidLength, sideSize, sideSize);
    glCallList(face);
    glPopMatrix();
    
    //kato
    glPushMatrix();
    glColor3f(0, .2, .1);
    glRotatef(-90, 1, 0, 0);
    glScalef(sideSize, cuboidLength, sideSize);
    glCallList(face);
    glPopMatrix();
    
    //pano
    glPushMatrix();
    glColor3f(.5, .5, .1);
    glTranslatef(0, sideSize, 0);
    glRotatef(-90, 1, 0, 0);
    glScalef(sideSize, cuboidLength, sideSize);
    glCallList(face);
    glPopMatrix();

    //skepi aristera
    glPushMatrix();
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 100);
    glColor3f(.1, .5, .1);
    glTranslatef(0, sideSize, 0);
    glRotatef(90, 0, 1, 0);
    glRotatef(30, 1, 0, 0);
    glScalef(cuboidLength, sideSize, cuboidLength);
    glCallList(face);
    glPopMatrix();
    
    //skepi deksia
    glPushMatrix();
    glColor3f(.1, .1, .5);
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 100);
    glTranslatef(0, sideSize, 0);
    glTranslatef(sideSize, 0, 0);
    glRotatef(90, 0, 1, 0);
    glRotatef(-30, 1, 0, 0);
    glScalef(cuboidLength, sideSize, cuboidLength);
    glCallList(face);
    glPopMatrix();

    //trigono mprosta
    glPushMatrix();
    glColor3f(.2, .2, .5);
    glTranslatef(0, sideSize, 0);
    glScalef(sideSize, sideSize, cuboidLength);
    glCallList(triangle);
    glPopMatrix();
    
    //trigono piso
    glPushMatrix();
    glColor3f(.2, .2, .1);
    glTranslatef(0, 0, -1*cuboidLength);
    glTranslatef(0, sideSize, 0);
    glScalef(sideSize, sideSize, cuboidLength);
    glCallList(triangle);
    glPopMatrix();
    
    glEndList();
}

void drawTerrain(int sideSize)
{
    terrain = face+3;
    glNewList(terrain, GL_COMPILE);

    glTranslatef(-0.5*sideSize, -0.5*sideSize, .5*sideSize);

    glPushMatrix();    
    glColor3f(0, 1, 0);
    glRotatef(-90, 1, 0, 0  );
    glScalef(sideSize, sideSize, sideSize);
   
    glCallList(face);
    glPopMatrix();
    
    glEndList();

}

void 
init_scene()
{    
    drawFace(1);
    drawTriangle();
    drawHouse(10);
    drawTerrain(50);
}

GLvoid 
window_display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	    
    glLoadIdentity();
    render_scene(idGlobal);
}

GLvoid 
window_reshape(GLsizei width, GLsizei height)
{
}

void 
render_scene(int id)
{    
    switch (id) 
    {
        case 0://prosopsi
        {
            glOrtho(-30,30, -30, 30, -50, 50);            
            glPushMatrix();
            glCallList(house);
            glPopMatrix();
            break;
        }
        case 1://katopsi
        {
            glOrtho(-30,30, -30, 30, -50, 50);
            glPushMatrix();
            glRotatef(90, 1, 0, 0);
            glCallList(house);
            glPopMatrix();
            break;
        }
         case 2://deksia opsi
        {
            glOrtho(-30,30, -30, 30, -50, 50);
            glPushMatrix();
            glRotatef(90, 0, 1, 0);
            glCallList(house);
            glPopMatrix();
            break;
        }
        case 3://isometriki opsi
        {
            glViewport(0, 0, 500, 500);
            glOrtho(-30,30, -30, 30, -50, 50);
            glPushMatrix();
            glTranslatef(0, 0, 20);
            gluLookAt(20, 20, 20, 0, 0, 0, 0, 1, 0); // redo ***********************
            glCallList(house);
            glPopMatrix();    
            break;
        }
        case 4://prooptiki provoli 2 simion
        {
            glViewport(0, 0, 500, 500);
           
            gluLookAt(11, 0, 11, 0, 0, 0, 0.0, 1.0, 0.0);
            glPushMatrix();
            glCallList(house);
            glPopMatrix();   
            break;
        }
        case 5://quit
        {
            exit(0);
            break;
        }
        case 6:
        {
            glViewport(0, 0, 500, 500);
            glOrtho(-30,30, -30, 30, -50, 50);
            gluPerspective(90, 1.0, 1, 10);
            glPushMatrix();
           //glTranslatef(0, 0, 20);
            gluLookAt(20, 20, 20, 0, 0, 0, 0, 1, 0); // redo ***********************            
           //glCallList(house);
            glCallList(terrain);
            glPopMatrix();
        }
    }
    glutSwapBuffers();
    glutPostRedisplay(); 
    glFlush();
}

void 
menu(int id)
{
    idGlobal=id;
}

int 
main(int argc, char **argv) 
{  
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutInitWindowPosition(0, 0);
    glutCreateWindow("Single house");
    initGL();  
    init_scene();
    glutDisplayFunc(window_display);
    glutReshapeFunc(window_reshape);
    //glutIdleFunc(spinCube);
    glutCreateMenu(menu);
    glutAddMenuEntry("prosopsi", 0);
    glutAddMenuEntry("katopsi", 1);
    glutAddMenuEntry("deksia opsi", 2);
    glutAddMenuEntry("isometriki opsi", 3);
    glutAddMenuEntry("prooptiki opsi", 4);
    glutAddMenuEntry("quit", 5);
    glutAddMenuEntry("terrain", 6);
    glutAttachMenu(GLUT_RIGHT_BUTTON);    
    glutMainLoop();  
    return 1;
}

