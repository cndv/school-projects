/*
 Project: Αξιολόγηση απόδοσης πρωτοκόλλου ALOHA με σχισμές
 Name: Konstantinos Chartomatzis
 AEM: 1553
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include <time.h> 

#define NumOfNodes 30			//Arithmos stahmon M
#define P 0.0815		//pithan. epanekpompis
#define FinalNumOfSlots 10000		    //telikos arithmos slots R
#define Ds 0.001		//bima gia tin pithanotita genniseon s
#define SMAX 0.999	    //max timi tou s
#define MAXINT 32767	//max int

//returns a random poisson number
double PoissonRandomNumber(const double lambda)
{
	int k=0;                          //Counter
	const int max_k = 100;           //k upper limit
	double p ; //uniform random number
	double pTemp = exp(-lambda);          //probability temp
	double sum=pTemp;  //cumulant
	//double randNum;
	
	//srand(1);
	
	p=(double)(rand()%100)/(double)MAXINT;
	//printf("p %f \n",p);
	
	if (sum>=p) return 0;             //done allready
	for (k=1; k<max_k; ++k) {         //Loop over all k:s
		pTemp*=lambda/(double)k;            //Calc next prob
		sum+=pTemp;                         //Increase cumulant
		if (sum>=p) break;              //Leave loop
	}
	
	return k/(double)MAXINT;                         //return random number
}

int main(void)
{
	FILE *fp;
	double step = 0.001,               //step s
	throughput,			//***throughput S
	inputRate,		//***rithmos eisodou sin 
	Beta,		//mikos ouras (busy)
	delay,			//***kathisterisi
	timeInSlotBewtTwoArivs,			//xroniko diastima se slots metaksy 2 afiksewn A
	numArivPerSlot,			//***arithmos afikseon se ena slot B
	x1,					//aux
	x2;					//aux
	
	int resends,			//epanekpompes i
	creations,			//genniseis j
	slots,			//slots c
	enabledNodes,			//energoi stathmoi
	enabldedNodesOfSlots,			//energoi stathmoi twn c slots Bc
	idleSlots,		//aerga slots idles
	randNum,              //generate rand number z
	success,	//epityxeis epanekpompes
	collisions; //sygkrouseis
	
	fp = fopen("alohaSlotted.txt", "w");
	fprintf(fp," throughput\t delay \t step\n");
	
	//To kyriws loop ekteleitai gia 1000 fores peripou
	while (step < SMAX)
	{
		step += Ds;
		
		
		//initialize
		enabledNodes = 0;    //energoi stathmoi
		enabldedNodesOfSlots = 0;		//energoi stathmoi ton c slots
		idleSlots = 0;		//aerga slots
		success = 0;	// epituxeis apenekpompes
		collisions = 0;		// sygkrouseis
		
		srand(time(NULL));    //Initialize random num
		
		//for R slots
		for (slots = 1; slots < FinalNumOfSlots; slots++) { // c = slots , R max num of slots
			//---------------------------------------------------------------------------------------
			//Ypologismos tou number of arivals per slot 
			numArivPerSlot = 0.0;		//***arithmos afikseon se ena slot
			resends = 0;			//epanekpompes
			
			randNum = rand() % 100;
			x1 = (double) randNum / MAXINT;
			
			while(resends <= enabledNodes) // resends = epanekpompes , enabledNodes = energoi stathmoi
			{
				numArivPerSlot +=PoissonRandomNumber(40.0);
				
				if ((resends == enabledNodes) && (x1 > numArivPerSlot))
					break;
				
				if (x1 > numArivPerSlot)
					resends++;
				else
					break;
			}
			
			//Ypologismos tou creations
            timeInSlotBewtTwoArivs = 0.0;		//xroniko diastima se slots metaksy 2 afiksewn
			creations = 0;			//genniseis
			
			randNum = rand() % 100;
			x2 = (double) randNum / MAXINT;
			
			while (creations <= NumOfNodes - enabledNodes)	// M = arithmos stathmon
			{
				timeInSlotBewtTwoArivs += PoissonRandomNumber(40.0);
				if ((creations == NumOfNodes - enabledNodes) && (x2 > timeInSlotBewtTwoArivs))
					break;
				if (x2 > timeInSlotBewtTwoArivs)
					creations++;
				else
					break;
			}
			//---------------------------------------------------------------------------------------
			
			if (resends==0 && creations==0)  // i = epanekpompes , j = genniseis
				idleSlots++;
			else
				if (resends==1 && creations==0 || resends==0 && creations==1) {
					success++;   // epituxeis apenekpompes
					if (resends==1 && creations==0) enabledNodes--;  // meionei toys energous stathmous
				} 
				else {
					collisions++;
					enabledNodes += creations;  // afxanei toys energous stathmous N kata j genniseis
				}
			enabldedNodesOfSlots += enabledNodes;  //energoi stathmoi twn c slots kata N stathmous
		} //end for
		
		throughput = (double) success / FinalNumOfSlots;  // throughput
		Beta = (double) enabldedNodesOfSlots / FinalNumOfSlots;		//mikos ouras (busy)
		inputRate = (NumOfNodes - Beta) * step;		//rithmos eisodou
		delay = 1.0 + (Beta / throughput);		//delay
		
		fprintf(fp, "%.12f \t %.12f \t %.12f \n", throughput,delay, step);	
	} //end while
	
	fclose(fp);
	return 0;
} //end main
