/*
 * @Project : Implementation of the Extact Algorithm, as described in the paper 
 * "Detecting Distance based Outliers is Streams of Data", 
 * @author : Konstantinos Chartomatzis,
 * @AEM : 1553,
 * @email: kchartom@csd.auth.gr
 */
package outliers;
//  imports using some apache libraries, 
//  which can be found here http://commons.apache.org/io/
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FileUtils;


/**
 * Η κλάση Node αποθηκεύει το id και τις τιμές από κάθε διάνυσμα.
 * καθώς και μετρητές για τους γείτονες
 * όπως επίσης και flags για το αν έχουν γίνει σε κάποια φάση inliers η outliers
 */
class Node {
   private int id;
   private ArrayList<Integer> numbers;
   private int counterAfter;
   private int counterBefore;
   private ArrayList<Integer> nnBefore;
   private boolean outlier;
   private boolean inlier;
  //============================================================================
  //
  //    default constructor
   public Node(){
        id = 0;        
        numbers = new ArrayList<>();
        counterAfter=1;
        counterBefore=0;
        nnBefore = new ArrayList<>();
        outlier = false;
        inlier = false;
   }
   //============================================================================
  //
  //    constructor which accepts id
   public Node(int id){
       this.id = id;
       numbers = new ArrayList<>();
       counterAfter=1;
       counterBefore=0;
       nnBefore = new ArrayList<>();
       outlier = false;
       inlier = false;
   }   
  //============================================================================
  //
  //    set that this vector is an outlier
   void toggleOutlier(){
       outlier = true;
   }
  //============================================================================
  //
  //    check if this vector is an outlier, return true or false
   boolean isOutlier(){
       return outlier;
   }
  //============================================================================
  //
  //    set that this vector is an inlier
   void toggleInlier(){
       inlier = true;
   }
  //============================================================================
  //
  //    check if this vector is an inlier, return true or false
   boolean isInlier(){
       return inlier;
   }
  //============================================================================
  //
  //    increment the counter that tells how many neighbors are after this node
   void incrementCounterAfter(){
       this.counterAfter++;
   }
  //============================================================================
  //
  //    set the counter that tells how many neighbors are after this node
   void setCounterAfter(int counterAfter){
       this.counterAfter = counterAfter;
   }
  //============================================================================
  //
  //    get the counter that tells how many neighbors are after this node
   int counterAfter(){
       return counterAfter;
   }
  //============================================================================
  //
  //    set the counter that tells how many neighbors are before this node
   void setcounterBefore(int counterBefore){
       this.counterBefore = counterBefore;
   }
  //============================================================================
  //
  //    add the id of the neighbor that is before this node to the nnBefore    
  //    array list
   public void addIdToNNBefore(int id){
       nnBefore.add(id);
   }
  //============================================================================
  //
  //   array with the list of ids that are neighbors which have come before 
  //   this node 
   public ArrayList<Integer> nnBefore(){
       return nnBefore;
   }
  //============================================================================
  //
  //    set the nnBefore list
   public void setNNBefore (ArrayList<Integer> nnBefore){
       this.nnBefore = new ArrayList<>(nnBefore);
   }
  //============================================================================
  //
  //    set the counter that tells how many neighbors are before this node
  //    but exclude these which have expired
   public int counterBefore (int threshold){ // threshold = ( t or id ) - w + 1
       for (int i=0; i < nnBefore.size(); i++){
           if (this.nnBefore.get(i) < (threshold)){
               continue;
           }
           else{
               counterBefore = nnBefore.size() - i + 1;
           }
       }
       return counterBefore;
   }
  //============================================================================
  //
  //    set the id of this node
   public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return id;
    }
  //============================================================================
  //
  //     add number to the numbers array of this node
   public void addNumbers(int num){
       numbers.add(num);
   }
  //============================================================================
  //
  //    get the numbers(values) of this node
   public ArrayList<Integer> numbers(){
       return numbers;
   }
  //============================================================================
  //
  //    get the dimension of this vector
   public int dimension(){
       return numbers.size();
   }
  //============================================================================
  //
  //    when printing the Node object , prints the id of the vector
   @Override public String toString() {
    StringBuilder result = new StringBuilder();
    result.append(this.getId());    
    return result.toString();
  }
}


  

/**
 * This class implements the exact algorithm that is described in the paper
 * it includes the window size w, the number of the neighbors k, the distance r
 * the vectors are saved in the points array,
 * the ISB data structure for reasons of simplicity is an array,
 * the outliers of each window are saved in the currnentWindowOutliers array,
 * there three counters for the vectors that are always outliers alwaysOutliers,
 * those who never were outliers neverOutliers, and those who have been in 
 * both states bothStates 
 */
class OutlierDetector {
    private int w,k,r;
    private int numberOfPoints;
    private ArrayList<Node> points;
    private ArrayList<Node> isb;
    private ArrayList<Integer> currentWindowOutliers;
    private int alwaysOutliers, neverOutliers, bothStates;    
  //============================================================================
  //
  //    default constructor
    public OutlierDetector(){
        w = k = r = alwaysOutliers = neverOutliers = bothStates = 0;
        points = new ArrayList<>();
        isb = new ArrayList<>();
    }
  //============================================================================
  //
  //    constructor sets w,k,r, and the vectors    
    public OutlierDetector(int w, int k, int r, ArrayList<Node> points){
        this.w = w;
        this.k = k;
        this.r = r;        
        this.points = new ArrayList<>(points);
        isb = new ArrayList<>() ;
        alwaysOutliers = neverOutliers = bothStates = 0;
    }
  //============================================================================
  //
  //    set the window size       
    public void setW(int w){
        this.w = w;
    }
  //============================================================================
  //
  //    set the number of neighbors   
    public void setK(int k){
        this.k = k;
    }
  //============================================================================
  //
  //    increment the bothState counter 
    public void incrementbothStates(){
        bothStates++;
    }
  //============================================================================
  //
  //    increment the neverOutlier counter 
    public void incrementNeverOutliers(){
        neverOutliers++;
    }
  //============================================================================
  //
  //    increment the alwaysOutlier counter       
    public void incrementAlwaysOutliers(){
        alwaysOutliers++;
    }
  //============================================================================
  //
  //    get the bothState counter        
    public int bothStates(){
        return this.bothStates;
    }
  //============================================================================
  //
  //    get the neverOutliers counter    
    public int neverOutliers(){
        return this.neverOutliers;
    }
  //============================================================================
  //
  //    get the alwaysOutliers counter
    public int alwaysOutliers(){
        return this.alwaysOutliers;
    }
  //============================================================================
  //
  //    set the distance r  
    public void setR(int r){
        this.r = r;
    }
  //============================================================================
  //
  //   get the window size     
    public int w(){
        return this.w;
    }
  //============================================================================
  //
  //   get the number of neighbors     
    public int k(){
        return this.k;
    }
  //============================================================================
  //
  //    get the distance r
    public int r(){
        return this.r;
    }
  //============================================================================
  //
  //    add a vector to the ISB                
    public void addPointToISB(Node p){
        isb.add(p);        
    }
  //============================================================================
  //
  //    get the ISB array
    public ArrayList<Node> isb (){
        return isb;
    }   
  //============================================================================
  //
  //    remove the first element of the ISB    
    public Node popNodeFromISB(){
        return isb.remove(0);
    }
  //============================================================================
  //
  //    add a vector     
    public void addPoint(Node p){
        points.add(p);        
    }
  //============================================================================
  //
  //    set the points array 
    public void setPoints(ArrayList<Node> points){
        this.points = new ArrayList<>(points);
    }
  //============================================================================
  //
  //    return the arary of vectors  
    public ArrayList<Node> points (){
        return points;
    }
  //============================================================================
  //
  //     return the number of vectors  
    public int numberOfPoints(){
        numberOfPoints = points.size();
        return numberOfPoints;                
    }
  //============================================================================
  //
  //     take the eykleidian distance between two vectors   
    public double distance(Node first, Node sec){
        double distance;
        int dimension;
        dimension = first.dimension();
        double squareSum = 0.0;
        double abstraction;
        for (int i=0; i<dimension; i++){
            abstraction = first.numbers().get(i) - sec.numbers().get(i);
            squareSum = squareSum + Math.pow(abstraction, 2.0);
        }
        distance = Math.sqrt(squareSum);
        return distance;
    }
  //============================================================================
  //
  //     the Stream Manager procedure   
    public void streamManager(){
        // threshold is the id - w +1
        int threshold;
        //  iterate through all the vectors
        for (int i=0; i<points.size(); i++){            
            //only true the first time                
            if (isb.isEmpty()){
                isb.add(points.get(i));
            }            
            else{
                // in each iteration update the threshold
                threshold = points.get(i).getId() - w +1;
                // remove the expired vector
                if (isb.get(0).getId()< threshold){
                    // update the final staticts                    
                    updateStatistics(isb.remove(0));
                }
                // assign the current node
                Node currNode = points.get(i);
                // distance
                double d;
                Iterator it=isb.iterator();
                while(it.hasNext()){
                    // take the eykleidian distance with this node
                    Node n = (Node)it.next();
                    d = this.distance(currNode, n);                
                    // if distance is <= R                
                    if ( d <= r ){
                        // increment it.count after
                        n.incrementCounterAfter();
                        // update curr nnbefore list with it id
                        currNode.addIdToNNBefore(n.getId());
                    }                                               
                }
                // add the current node to ISB
                isb.add(currNode);
                // call the query  manager             
                queryManager(threshold);                
            }
        }
        //what left in isb
        Iterator it=isb.iterator();
        while(it.hasNext()){            
            updateStatistics(isb.remove(0));
        }
        
    }
  //============================================================================
  //
  //    the Query Manager procedure  
    public void queryManager(int threshold){
        Iterator it=isb.iterator();       
        currentWindowOutliers = new ArrayList<>();
        // iterate through the ISB
        while(it.hasNext()){              
           Node n  = (Node)it.next();
           int cb = n.counterBefore(threshold);
           // if true is an outlier
            if (n.counterAfter() + cb < k) {                
                // add it to the current list of outliers
                currentWindowOutliers.add(n.getId());
                n.toggleOutlier();
            }
            else{
                // else is a inlier
                n.toggleInlier();
            }                        
        }
        // prints to console the current window elements
        System.out.println("Current window is isb = " + isb);
        // prints to console the current window outliers
        System.out.println("currentWindowOutliers = " + currentWindowOutliers);
        System.out.println("");
        // deletes the elements of the list
        currentWindowOutliers.clear();

    }
  //============================================================================
  //
  //    update the counters that hold the final statistics
    public void updateStatistics(Node removed){
        if (removed.isInlier() && removed.isOutlier()){
            // bothchanged
            this.incrementbothStates();
        }
        else if (removed.isInlier() && !removed.isOutlier()){
            //never been an outlier          
            this.incrementNeverOutliers();
        }
        else if (removed.isOutlier() && !removed.isInlier()){
            //always been an outlier
            this.incrementAlwaysOutliers();
        }
    }
  //============================================================================
  //
  //    print the final counters that hold total statistics 
    public void printStatistics(){
        System.out.println("");
        System.out.println("> " + this.alwaysOutliers + " vectors" +
                " were always outliers. ");
        System.out.println("> " + this.neverOutliers + " vectors" +
                " were never outliers. ");
        System.out.println("> " + this.bothStates + " vectors" + 
                " were both inliers and outliers. ");
        System.out.println("");
        System.out.println("");
    }            
}
  

/*
 * This class is responsible for reading the data from a file
 * saves the vectors to points array
 * reads each line and saves it to the readLines List
 * pathWithFileName: holds the path with the file name (ex. ~/home/test.txt)
 */
 class Reader {
    private ArrayList<Node> points;
    private String pathWithFileName;
    private int dimension;
    private List<String> readLines;
    private File fileHandler;
  //============================================================================
  //
  //    default constructor     
    public Reader(){
        this.points = new ArrayList<>();
        pathWithFileName = null;
        dimension = 0; 
        readLines = null;

    }
  //============================================================================
  //
  //    constructor sets the path  
    public Reader(String pathWithFileName){
        points = new ArrayList<>();
        this.pathWithFileName = pathWithFileName;  
        readLines = null;
    }
  //============================================================================
  //
  //    get the vectors     
    public ArrayList<Node> points(){
        return points;
    }
  //============================================================================
  //
  //    set the vectors     
    public void points(ArrayList<Node> points){
        this.points = new ArrayList<>(points);
    }
  //============================================================================
  //
  //    set the readLines variable  
    public void readlines(List<String> readlines){
        this.readLines = new ArrayList<>(readlines);
    }
  //============================================================================
  //
  //    get the readLines variable     
    public List<String> readlines(){
        return readLines;
    }
  //============================================================================
  //
  //    set the pathWithFileName variable
    public void pathWithFileName(String pathWithFileName){
        this.pathWithFileName = pathWithFileName;
    }
  //============================================================================
  //
  //    get the pathWithFileName variable 
    public String pathWithFileName(){
        return pathWithFileName;
    }
  //============================================================================
  //
  //    set the dimension of the vector        
    public void dimension(int d){
        dimension = d;
    }
  //============================================================================
  //
  //    get the dimension of the vector     
    public int dimension(){        
        return dimension;
    }
  //============================================================================
  //
  //    reads each line from a file     
    public void readFromFile() throws IOException{
        fileHandler = new File(this.pathWithFileName);
        readLines = FileUtils.readLines(fileHandler);
        String [] tempArray;
        tempArray = this.readLines.get(0).split(",");
        dimension = tempArray.length -1;
    }
  //============================================================================
  //
  //    prints to console all the vectors     
    public void printAllPoints(){
        for (Node d: this.points){
            System.out.println(d);
        }   
    }
  //============================================================================
  //
  //    extracts the vectors from the readLine variable and adds them to 
  //    the points array     
    public void populatePoints() throws IOException{
        // read line by line the file
        this.readFromFile();           
        String [] tempStringData;
        // seperate the numbers by using a pattern
        String patternForData = "[,]";
        for (int i=0; i<readLines.size(); i++){
            //  split the numbers
            tempStringData = readLines.get(i).split(patternForData);               
            Node v = new Node();
            for (int j=0; j<tempStringData.length ; j++){
                if (j==0){
                    // at 0 index is the id, convert it to int and set it
                    v.setId(Integer.parseInt(tempStringData[j]));                        
                }
                else{
                    // read,convert to int, and add the number to the array
                    v.addNumbers(Integer.parseInt(tempStringData[j]));
                }
            }
            // finally add the vector to the points array
            this.points.add(v);
        }
    }    
}


/**
 * Handles the Outlier of the program
 */
class Menu {
  //============================================================================
  //
  //    initial menu     
    public void init () throws IOException{        
        while(true){
            int choice;
            choice = printMenuOptions();
            switch(choice){
                case 1: enterParametersAndRunProgram();
                    break;
                 case 2: //exit
                     System.out.println("");                     
                     System.out.println("======== Thank you, program "
                             + "terminated! =========");
                     System.out.println("");
                     return;                     
                 default: errorMessage();                 
                     break;
            }            
        }        
    }
  //============================================================================
  //
  //    reads the path and the file name,the window w, number of neighbors k, 
  //    distance from the user and starts the algorithm
    public void enterParametersAndRunProgram() throws IOException{
        System.out.println("");
        System.out.println("Please, type the complete path and the name "
                + "of the file, ex. ~/user/text.txt ");
        System.out.print("> ");                
        String pathAndFileName;        
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));  
        pathAndFileName = br.readLine();        
        System.out.println("");
        System.out.println("Please, type the w");
        System.out.print("> ");                
        String wInput = br.readLine();
        int w = Integer.parseInt(wInput);
        System.out.println("");
        System.out.println("Please, type the k");
        System.out.print("> ");                        
        String kInput = br.readLine();
        int k = Integer.parseInt(kInput);
        System.out.println("");
        System.out.println("Please, type the r");
        System.out.print("> ");                        
        String rInput = br.readLine();
        int r = Integer.parseInt(rInput);
        
        runProgram(pathAndFileName, w, k, r);
    }
  //============================================================================
  //
  //    prints an error message if the input is incorrect 
    public void errorMessage(){
        System.out.println("");
        System.out.println("Wrong input, please type again.");
        System.out.println("");
    }
  //============================================================================
  //
  //    runs the program with the input from the user     
    public void runProgram(String pathAndFile, int w, int k, int r) 
            throws IOException{        
        // sets the reader object with the path and file name
        Reader read = new Reader(pathAndFile);        
        // reads the file
        read.readFromFile();
        // initialises the array of vectors
        read.populatePoints(); 
        // initilisation 
        OutlierDetector o = new OutlierDetector(w,k,r,read.points());                
        o.streamManager();
        // prints the final statistics
        o.printStatistics();
    }
  //============================================================================
  //
  //    prints to the console the initial menu of options 
    public int printMenuOptions() throws IOException{
        System.out.println("================ Title: Outliers ================");
        System.out.println("");
        System.out.println("> Type 1, to run the program, or");
        System.out.println("> Type 2, to exit the program.");
        System.out.print("> ");                
        int choice;
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in));  
        choice = Integer.parseInt(br.readLine());
        return choice;
    }
}


/**
 * Starts the program logic
 */
public class Outlier {   
  //============================================================================
  //
  //    main function of the program
    public static void main(String[] args) throws IOException {     
       Menu m =  new Menu();
       m.init();
    }
}
