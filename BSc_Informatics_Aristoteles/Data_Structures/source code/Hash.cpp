#include "Hash.h"//definition of the class
#include <iostream>

using namespace std;
/*----------constructor----------*/
Hash::Hash()
{
   numberOfElementsHashTable = 0;
}
/*------destructor------*/
Hash::~Hash()
{
	//empty body
}
/*---------create the hash table---------*/
bool Hash::hashTableCreate(int idNumber, int hashTableSize)
{
	id = idNumber;
	hashTable = new List[hashTableSize];//allocates space
	return true;
}
/*--------------insert function of the hash table--------------*/
bool Hash::hashTableInsert(int newValue)
{
   hashTable[hashTableGetKey(newValue)].listInsert(newValue);
   numberOfElementsHashTable++; //increasing the number of items in the table
   return true;
}
/*-------------search function in the hash table-------------*/
bool Hash::hashTableSearch(int searchNumber)
{
	if(hashTable[hashTableGetKey(searchNumber)].listSearch(searchNumber))
		return true;
	else
		return false;
}
/*-----------deletes an item in the hash table-----------*/
bool Hash::hashTableDelete(int searchKey)
{
	if(hashTableSearch(searchKey))
	{
		hashTable[hashTableGetKey(searchKey)].listDelete(searchKey); 
		numberOfElementsHashTable--; //decreasing the number of items in the table
		return true;
	}
	else
		return false;
}
/*--------------destroy the hash table --------------*/
bool Hash::hashTableDestroy(void)
{
	
	if(isHashTableEmpty())
	{
		return false;
	}
	else
	{
		List* deletePtr;
		for(int i=0; i<hashTableSize; i++)
		{
			deletePtr = &hashTable[i];
			deletePtr->listDestroy();
		}
		numberOfElementsHashTable = 0;
		delete[] hashTable;
		return true;
	}
}
/*-----------------------------------------------------------------------------*/
/*-----------------------------auxiliary functions-----------------------------*/
/*-----------------------------------------------------------------------------*/
/* table key  function*/
int Hash::hashTableGetKey(int searchKey)
{
   int key = searchKey%hashTableSize; // hash  formula
   return key;
} 
/* check if its empty*/
bool Hash::isHashTableEmpty(void)
{
	if(numberOfElementsHashTable == 0)
		return true;
	else
		return false;
}
/*returns the number of elements the hash has*/
int Hash::getNumberOfElementsHashTable()
{
	return numberOfElementsHashTable;
}