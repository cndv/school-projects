#ifndef _Hash//check for redefinition of the class
#define _Hash
	#include "List.h" //definition of the List class 

	class Hash
	{
		private:
			List* hashTable; //definition of the  hash table 
			int numberOfElementsHashTable; //number of elements in the table
			int id;// id of the hash table
			int hashTableSize;//the size of the hash table

		public:
			Hash(); //constructor
			~Hash(); //destructor

			bool hashTableCreate(int idNumber, int hashSize);//create the hash table
			bool hashTableSearch(int searchNumber);//search an item in the hash table
			bool hashTableInsert(int newItem); //adds a new item to the table
			bool hashTableDelete(int searchNumber); //deletes an item from the table
			bool hashTableDestroy(void);// destroy the hash table
			/*auxiliary functions*/
			int hashTableGetKey(int searchNumber); //finds the index of the table,that the number should be located
			bool isHashTableEmpty(void);
			int getNumberOfElementsHashTable(void);
	};//end of class Hash
#endif