#include "HeapMax.h"//heapMax definition ,represents the node of the heapMax
#include <iostream>
#include<cmath>//for the numberOfElementsPerDepth() function

using namespace std;

/*-------------constructor initializes-------------*/
HeapMax::HeapMax()
{
	numberOfElements = 0;
}
/*------------destructor------------*/
HeapMax::~HeapMax()
{
	//empty body
}
/*---------------------------------create the heap FUNCTION---------------------------------*/
bool HeapMax::heapMaxCreate(int id)
{
	root = NULL;
	heapMaxSetId(id);
	return true;
}
/*-------------------------------------insert FUNCTION-------------------------------------*/
bool HeapMax::heapMaxInsert(int number)
{
	int thesis,father,pathCounter=0,i;
	bool path[100];

	//new node declaration
	NodeHeap* newNode;
	newNode = new NodeHeap();
	newNode->setValue(number);
	

	if (isHeapMaxEmpty())//if the heap is empty , initializes the root
	{
		//numberOfElements++;//increase number of elements
		root = newNode;// root points to newNode
		root->setDepthNode(1);// node depth is 1
		depth = 1;// depth tree is 1
		numberOfElements++;//increase number of elements
	}
	else if (!root->getLeftChildFlag())//if the root has no left child , it inserts 
	{


		//numberOfElements++;//increase number of elements
		root->setLeftChild(newNode);	//new root's right child points to newNode
		root->setLeftChildFlag(true);
		newNode->setDepthNode(2);// newNode depth is 2
		numberOfElements++;//increase number of elements

		depth = 2;//depth tree is 2
		int cur;///auxiliary variable for the swap

		if(newNode->getValue()>root->getValue())//if the newNode is bigger then swap
		{
			cur=newNode->getValue();
			newNode->setValue(root->getValue());
			root->setValue(cur);
		}
	}
	else if (!root->getRightChildFlag())//if the root has no right child it inserts
	{


		//numberOfElements++;//increase number of elements
		root->setRightChild(newNode);	//new root's right child points to newNode
		root->setRightChildFlag(true);
		newNode->setDepthNode(2);// newNode depth is 2
		numberOfElements++;//increase number of elements

		depth = 2;//depth tree is 2
		int cur2;//auxiliary variable for the swap

		if(newNode->getValue()>root->getValue())//if the newNode is bigger then swap
		{
			cur2=newNode->getValue();
			newNode->setValue(root->getValue());
			root->setValue(cur2);
		}
	}
	else //for 4 elements and more
	{
		
		thesis = ++numberOfElements;////increase number of elements

		father = thesis /2;
		while(father>1)//find the path for the new element
		{
			
			if(father%2==0)//checks if its odd or even
				path[pathCounter] = false;//for the left -> false
			else
				path[pathCounter] = true;// for the right -> true

 			father = father /2;
			pathCounter++;
		}

		NodeHeap* temp3=root;

		int cur3;//auxiliary variable for the swap

		depth=pathCounter+2;

		for(i=pathCounter-1; i>=0; i--)//finds the path
		{

			if(newNode->getValue()>temp3->getValue())
			{
				cur3 = newNode->getValue();
				newNode->setValue(temp3->getValue());
				temp3->setValue(cur3);
			}
			if(path[i]==false){//false -> left 
				temp3=temp3->getLeftChild();
			}
			else {//true go right
				temp3 = temp3->getRightChild();
			}
		}
		
		//checks where the newNode is going to be placed right or left
		if(thesis%2==0){//left
			temp3->setLeftChild(newNode);
			temp3->setLeftChildFlag(true);
			temp3->setDepthNode(depth);
			
		} 
		else//right
		{
			temp3->setRightChild(newNode);
			temp3->setRightChildFlag(true);
			temp3->setDepthNode(depth);
		}
	}
	return true;
}
/*-----------------------------------read top FUNCTION-----------------------------------*/
int HeapMax::heapMaxReadTop(void)
{
	if(isHeapMaxEmpty()){
		return -1;
	}
	else{
		return root->getValue();
	}
}
/* -----------------------------------delete top FUNCTION -----------------------------------*/
bool HeapMax::heapMaxDeleteTop(void)
{
	int thesis,father,pathCounter=0;
	bool path[100];

	thesis = numberOfElements;
	
	if(isHeapMaxEmpty())
	{
		return false;
	}
	else
	{
		if (numberOfElements==1)//only the root , one size=1
		{
			NodeHeap* deleteNode=root;//the current node that will be deleted
			delete deleteNode;
			root=NULL;
			numberOfElements --;//decreases the size of the heap
			return true;
		}	
		else if (numberOfElements==2)// two elements , size 2
		{
			NodeHeap* deleteNodeLeft;
			deleteNodeLeft=root->getLeftChild();
			root->setValue(deleteNodeLeft->getValue());
			delete deleteNodeLeft;

			root->setLeftChild(NULL);
			root->setLeftChildFlag(false);
			depth=1;
			numberOfElements--;//decreases the size of the heap

		}
		else if (numberOfElements==3)// three elements , size 3
		{
			NodeHeap* deleteNodeRight;
			int temp;
			deleteNodeRight=root->getRightChild();
			root->setValue(deleteNodeRight->getValue());
			delete deleteNodeRight;

			root->setRightChild(NULL);
			root->setRightChildFlag(false);
			if ((root->getLeftChild())->getValue() > root->getValue())//left child's number is bigger than root's one
			{
				temp = (root->getLeftChild())->getValue();
				(root->getLeftChild())->setValue(root->getValue());
				root->setValue((root->getLeftChild())->getValue());
			}
			numberOfElements--;//decreases the size of the heap
		}
		else//more than 3 elements 
		{
			NodeHeap* deleteNode;
			int i;
			
			father = thesis /2;
			while(father>1)//find the path for the new element
			{
			
				if(father%2==0)//checks if its odd or even
					path[pathCounter] = false;//for the left -> false
				else
					path[pathCounter] = true;// for the right -> true

	 			father = father /2;
				pathCounter++;
			}

			NodeHeap* tempNode=root;

			for(i=pathCounter-1; i>=0; i--)//finds the path
			{
				if(path[i]==false){//false -> left 
					tempNode=tempNode->getLeftChild();
				}
				else {//true go right
					tempNode = tempNode->getRightChild();
				}
			}

			//finaly stage for the delete
			if(numberOfElements%2==0){
				deleteNode = tempNode->getLeftChild();//Sets the node in left
				tempNode->setLeftChild(NULL);//set the left child null
				tempNode->setLeftChildFlag(false);//set the flag false
			}
			else{
				deleteNode = tempNode->getRightChild();//set the delete node 
				tempNode->setRightChild(NULL);//set right child null
				tempNode->setRightChildFlag(false);//set the flag false
			}
			//delets the node
			root->setValue(deleteNode->getValue());//sets the new value for the root
			delete deleteNode;//deletes the node
			numberOfElements--;//decreases the size of the heap

			/*---------------------------final changes---------------------------*/

			tempNode=root;
			int tempNum;
		
			while(true)//while check
			{
				// left child is bigger than right child 
				if( (tempNode->getLeftChild())->getValue() > (tempNode->getRightChild())->getValue() )
				{
					// parent node is less than left child so needs swap
					if( tempNode->getValue() < (tempNode->getLeftChild())->getValue() )
					{
						tempNum = tempNode->getValue();
						tempNode->setValue(tempNode->getLeftChild()->getValue());
						tempNode->getLeftChild()->setValue(tempNum);
						//checks if there is left child
						if(!tempNode->getLeftChild()){
							tempNode = tempNode->getLeftChild();
						}
						else{//no more elements
							break;
						}

					}
					else{//no need for swap
						break;
					}
				}
				//right child is bigger than left
				else if ( (tempNode->getRightChild())->getValue() > (tempNode->getLeftChild())->getValue() )
				{
					//parent node less than right child so needs swap
					if( tempNode->getValue() < (tempNode->getRightChild())->getValue() )
					{
						tempNum = tempNode->getValue();
						tempNode->setValue(tempNode->getRightChild()->getValue());
						tempNode->getRightChild()->setValue(tempNum);
						//checks if there is right child
						if(!tempNode->getRightChild()){
							tempNode = tempNode->getRightChild();
						}
						else{// no more elements
							break;
						}
					}
					else{//no need for swap
						break;
					}
				}
			}

			
		}
		return true;
		
	}

	return true;
}
/*--------------destroy the heap FUNCTION--------------*/
bool HeapMax::heapMaxDestroy(void)
{
	if(isHeapMaxEmpty()){//is empty
		return false;
	}
	else
	{
		int t=numberOfElements;
		for(int i=0; i<=t; i++)
		{
			heapMaxDeleteTop();
		}
	}
	return true;
}

/*---------------------------------------------------------------------------------*/
/*-------------------------------AUXILIARY FUNCTIONS-------------------------------*/
/*---------------------------------------------------------------------------------*/

/*---------------get the number of elements in the heap---------------*/
int HeapMax::getNumberOfElements()
{
	return numberOfElements;
}
/*calculates the NUMBER of elements that the heap would have , if it would be COMPLETE due to the DEPTH*/
int HeapMax::numberOfElementsPerDepth(int depth)
{
	return (numOfElementsPerDepth = pow(2.0,(double)depth) - 1);
}
/*-----------------------is heap empty-----------------------*/
bool HeapMax::isHeapMaxEmpty()
{
	if (numberOfElements==0)//is empty
		return true;
	else
		return false;
}
/*--------------gets the id--------------*/
int HeapMax::heapMaxReadId(void)
{
	return heapMaxId;
}
/*----------------------sets the id of the heap----------------------*/
void HeapMax::heapMaxSetId(int id)
{
	heapMaxId = id;
}

/*--------------------set the depth of the heap--------------------*/
void HeapMax::setDepth(int dp)
{
	depth = dp;
}
/*-----------------------get the depth of the heap-----------------------*/
int HeapMax::getDepth(void)
{
	return depth;
}