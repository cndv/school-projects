#ifndef _HeapMax//checks for redeclaration of the class
	#define _HeapMax
	#include "NodeHeap.h"//definition of the Nodeheap class

		class HeapMax
		{
			private :

				int numberOfElements;//it is the number of the elements in the heap		  
				int numOfElementsPerDepth;
				int heapMaxId;// the heap id
				int depth;// the depth of the heap
				NodeHeap* root;//declaration of the root

			public :

				HeapMax();//constructor
				~HeapMax();//destructor

				bool heapMaxCreate(int id);//create the heapMax 
				bool heapMaxInsert(int number);//insert a number in the heap
				int heapMaxReadTop(void);//read the top number in the heap
				bool heapMaxDeleteTop(void);//delete the top node 
				bool heapMaxDestroy(void);// destrory the heap

				bool isHeapMaxEmpty();//checks if the heap is empty
				int heapMaxReadId(void);//read the  id of the heap
				void heapMaxSetId(int id);//sets the id of the heap
				int getNumberOfElements(void);//number of elements in the heap
				int numberOfElementsPerDepth(int depth);//calculates the number of elements per depth 
				void setDepth(int dp);// set the depth of the heap
				int getDepth(void);//get the depth of the heap
		};//end HeapMax class
#endif