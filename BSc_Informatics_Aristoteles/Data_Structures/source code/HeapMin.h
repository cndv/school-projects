#ifndef _HeapMin//checks for redeclaration of the class
	#define _HeapMin
	#include "NodeHeap.h"//definition of the Nodeheap class

		class HeapMin
		{
			private :

				int numberOfElements;//it is the number of the elements in the heap		  
				int numOfElementsPerDepth;
				int heapMaxId;// the heap id
				int depth;// the depth of the heap
				NodeHeap* root;//declaration of the root

			public :

				HeapMin();//constructor
				~HeapMin();//destructor

				bool heapMinCreate(int id);//create the heapMin 
				bool heapMinInsert(int number);//insert a number in the heap
				int heapMinReadTop(void);//read the top number in the heap
				bool heapMinDeleteTop(void);//delete the top node 
				bool heapMinDestroy(void);// destrory the heap

				bool isHeapMinEmpty();//checks if the heap is empty
				int heapMinReadId(void);//read the  id of the heap
				void heapMinSetId(int id);//sets the id of the heap
				int getNumberOfElements(void);//number of elements in the heap
				int numberOfElementsPerDepth(int depth);//calculates the number of elements per depth 
				void setDepth(int dp);// set the depth of the heap
				int getDepth(void);//get the depth of the heap
		};//end HeapMin class
#endif