#include "List.h"//definition of the List class

#include <iostream>
/*---------constructor---------*/
List::List()
{
	head = NULL;
	listNumberOfElements = 0;
}
/*-----destructor-----*/
List::~List()
{
	//empty body
}
/*------------------insert element in the list------------------*/
bool List::listInsert(int number)
{
	ListNode* newNode;
	newNode = new ListNode();
	newNode->setListNodeValue(number);
	
	
	if(listIsEmpty())//list is empty
	{
		newNode->setNext(head);
		head = newNode;
	}
	else
	{
		if(!listSearch(number))//doesn't find the number
		{
			ListNode* temp;
			temp = head;
			while(temp->getNext() != NULL)
			{	
				temp = temp->getNext();
			}
			//it adds it in end of the list
			newNode->setNext(temp->getNext());
			temp->setNext(newNode);
		}
	}
	listNumberOfElements++;//increases the size of the list 
	return true;
}
/*---------------------Search in the list for an element---------------------*/
bool List::listSearch(int number)
{
	if(listIsEmpty())//list is empty
	{
		return false;
	}
	else
	{
		ListNode* temp=head;
		while(temp->getListNodeValue() != number && temp->getNext() != NULL)//stops if it's equal with the number																	//
		{	                                                                //or traverse all the list
			temp = temp->getNext();
		}
		if(temp->getListNodeValue() == number)
			return true;
		else
			return false;
	}
}
/*--------------------------Delete an element--------------------------*/
bool List::listDelete(int number)
{
	ListNode* deleteNode;
	if(listIsEmpty())//is empty
	{
		return false;
	}
	else if (!listSearch(number)){
		return false;
	}
	else
		if(head->getListNodeValue() == number)//is in the head node
		{
			deleteNode = head;
			head = head->getNext();
			delete deleteNode;
			listNumberOfElements--;//decreases the size of the list
			return true;
		}
		else
		{
			ListNode* temp=head;//temp points to the head
			ListNode* temp2;
			temp2 = temp->getNext();//temp2 points always one node after temp
		
			//bool isDeleted=false;
	
			while(temp2 != NULL)
			{
				if(temp2->getListNodeValue() == number)
				{
					deleteNode = temp2;
					temp->setNext(deleteNode->getNext());
					delete deleteNode;//deletes the node

					listNumberOfElements--; //decreases the size of the list
	
					return true;
				}
				temp2= temp2->getNext();//moves on the pointer
				temp = temp->getNext();//move on the pointer
		}
			if(temp2==NULL)
				return false;
	}
}
/*------------destory the list------------*/
bool List::listDestroy(void)
{
	if(listIsEmpty())//is empty
	{
		return false;
	}
	else
	{
		ListNode* deleteNode;
		deleteNode = head;
		while(head!=NULL)
		{
			head = head->getNext();
			delete deleteNode;//deletes the node
			deleteNode = head;

		}
		return true;
	}
}
/*-------------------Auxiliary functions-------------------*/
/*------------shows if the list is empty------------*/
bool List::listIsEmpty()
{
	if(listNumberOfElements == 0)
		return true;
	else
		return false;
}