#ifndef _List//checks for redeclaration
#define _List
	#include "ListNode.h"//definition of the class ListNode
	class List
		{
			private:
				ListNode* head;//head of the list
				int  listNumberOfElements;//size of the list
			public : 
				List();//constructor
				~List();//destructor
				bool listInsert(int number);//inserts a number in the list
				bool listSearch(int number);// checks if there is the number in the list
				bool listDelete(int number);//deletes an number from the list
				bool listDestroy(void);//removes the list
				/*auxiliary functions*/
				void setListNumberOfElements(int num);// sets the current size of the list
				int getListNumberOfElements(void);//returns the size of the list
				bool listIsEmpty();//checks if the list is empty
		};//end class
#endif