#include "ListNode.h"//definition of the ListNode class

#include <iostream>
/*---------constructor---------*/
ListNode::ListNode()
{
	value = 0;
	next = NULL;
}
/*--------------overloaded constructor--------------*/
ListNode::ListNode(int valueTemp)
{
	value = valueTemp;
	next = NULL;
}
/*-----destructor-----*/
ListNode::~ListNode()
{
	//empty body
}
/*--------------overloaded constructor --------------*/
ListNode::ListNode(int valueTemp,ListNode* nextPtr)
{
	value = valueTemp;
	next = nextPtr;
}
/*--------sets the number of the node--------*/
void ListNode::setListNodeValue(int v)
{
	value = v;
}	
/*---------returns the number of the node---------*/
int ListNode::getListNodeValue(void)
{
	return value;
}
/*---------returns the next pointer of the node---------*/
ListNode* ListNode::getNext()
{
	return next;
}
/*---------sets the next pointer of the node---------*/
void ListNode::setNext(ListNode* nextPtr)
{
	next = nextPtr;
}