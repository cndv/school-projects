//class definitions
#ifndef _ListNode//checks for redeclaration of the class
	#define _ListNode
	class ListNode
	{
		private :
			int value;//the number of the node
			ListNode* next;//node's next pointer

		public :
			ListNode();//constructor
			ListNode(int valueTemp);//overloaded constructor
			ListNode(int valueTemp,ListNode* nextPtr);//overloaded constructor
			~ListNode();//destructor

			void setListNodeValue(int v);//sets the nunber of the node
			int getListNodeValue(void);//returns the value of the node
			ListNode* getNext();// returns the node's next pointer
			void setNext(ListNode* nextPtr);//sets the node'e next pointer
	};//end class
#endif