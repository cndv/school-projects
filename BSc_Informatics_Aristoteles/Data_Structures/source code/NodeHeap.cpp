#include "NodeHeap.h"//definition of the NodeHeap class

#include <iostream>

/*--------------constructor initializes--------------*/
NodeHeap::NodeHeap()
{
	
	rightChild = NULL;//starts with no child
	leftChild = NULL;//starts with no child

	//  false -> no_child ,true -> yes_child 
	leftChildFlag = false;//starts with no child
	rightChildFlag = false;//starts with no child
}

/*--------destructor--------*/
NodeHeap::~NodeHeap()
{
	//empty body
}

/*-----------sets the value of the node-----------*/
void NodeHeap::setValue(int data)
{
	value = data;
}

/*------------- returns the value of the node-------------*/
int NodeHeap::getValue(void)
{
	return value;
}

/*-----------------sets the  left child of the node-----------------*/
void NodeHeap::setLeftChild(NodeHeap* ptr)
{
	leftChild = ptr;
}
/*----------returns  the left child pointer----------*/
NodeHeap* NodeHeap::getLeftChild(void)
{
	return leftChild;
}
/*----------------sets the  right child pointer----------------*/
void NodeHeap::setRightChild(NodeHeap* ptr)
{
	rightChild = ptr;
}
/*--------------gets the right child pointer--------------*/
NodeHeap* NodeHeap::getRightChild(void)
{
	return rightChild;
}
/*----------------sets left child FLAG----------------*/
void NodeHeap::setLeftChildFlag(bool left)
{
	leftChildFlag = left;
}
/*------------- gets left childs flag-------------*/
bool NodeHeap::getLeftChildFlag()
{
	return leftChildFlag;
}
/*-------------------sets right child FLAG-------------------*/
void NodeHeap::setRightChildFlag(bool right)
{
	rightChildFlag = right;
}
/*------------------ gets right childs flag------------------*/
bool NodeHeap::getRightChildFlag()
{
	return rightChildFlag;
}
/*-----------------sets the depth of the node-----------------*/
void NodeHeap::setDepthNode(int dp)
{
	depthNode = dp;
}
/*-----------------returns the depth of the node -----------------*/
int NodeHeap::getDepthNode()
{
	return depthNode;
} 