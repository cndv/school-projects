//class definitions
#ifndef _NodeHeap
#define _NodeHeap
	class NodeHeap
	{
		private :

			int value;//the value of the node
			int depthNode;//the depth of the node
			bool leftChildFlag,rightChildFlag; //flag to show if the node has of not child
			NodeHeap* leftChild;//leftchild pointer
			NodeHeap* rightChild;//right child pointer

		public : 

			NodeHeap();//constructor
			~NodeHeap();//destructor
			void setValue(int data);//set the value of the node
			int getValue(void);//get the value of the node
			void setLeftChildFlag(bool left);//set the left child flag 
			bool getLeftChildFlag();//get the left child flag 
			void setRightChildFlag(bool right);//set the left child flag
			bool getRightChildFlag();//get the left child flag
			void setLeftChild(NodeHeap* ptr);//set left child node
			NodeHeap* getLeftChild(void);//get left child node
			void setRightChild(NodeHeap* ptr);//set right child node
			NodeHeap* getRightChild(void);//get right child node
			void setDepthNode(int dp);//set the depth of the node
			int getDepthNode();//get the depth of the node
	};//end class
#endif