#include "NodeStack.h"
/*---------constructor---------*/
NodeStack::NodeStack()
{
	//empty body
}
/*---------overloaded constructor---------*/
NodeStack::NodeStack(int n,NodeStack* ptr)
{
	value = n;//sets the value of the node
	next = ptr;//sets the next pointer
}
/*-----------destructor-----------*/
NodeStack::~NodeStack()
{
	//empty body
}
/*-------------sets the value of the node-------------*/
void NodeStack::setValue(int data)
{
	value = data;
}
/*------------returns the value of the node------------*/
int NodeStack::getValue(void)
{
	return value;
}
/*------------sets the next pointer------------*/
void NodeStack::setNext(NodeStack* ptr)
{
	next = ptr;
}
/*---------------returns the next pointer---------------*/
NodeStack* NodeStack::getNext(void)
{
	return next;
}

