#ifndef _NodeStack//checks for redeclaration
	#define _NodeStack
	class NodeStack
	{
		private :
			int value;//the number of the node
			NodeStack* next;//auxiliary next pointer
		public : 
			NodeStack();//constructor
			NodeStack(int n , NodeStack* ptr);//overloaded constructor
			~NodeStack();//destructor
			void setValue(int data);//sets the number of the node
			int getValue(void);//returns the value of the node
			NodeStack* getNext(void);//returns the next pointer of the node
			void  setNext(NodeStack* ptr);//sets the next pointer of the node
	};//end class NodeStack
#endif