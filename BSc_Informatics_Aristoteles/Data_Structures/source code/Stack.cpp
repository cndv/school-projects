#include "Stack.h"//definition of the Stack class
#include <iostream>

using namespace std;
/*----constructor----*/
Stack::Stack()
{
	numOfElemsInStuck=0;//initializes to zero
}
/*----destructor----*/
Stack::~Stack()
{
	//empty body
}
/*--------------create the stuck--------------*/
void Stack::stackCreate(int id)
{
	top = NULL;//initializes the top of the stuck to null
	stackIdNumber = id;//sets the id of the stuck
}
/*-----------------------------push function , adds new item in the stuck-----------------------------*/
void Stack::stackPush(int number)
{
	NodeStack* newCurrentNode;// declare the new node
	newCurrentNode = new NodeStack();
	newCurrentNode->setValue(number);//sets the value of the node

	newCurrentNode->setNext(top);
	top = newCurrentNode;// the top pointer points to the new node
	numOfElemsInStuck++;//increases the number of the items of the stuck
}
/*---------------read top function , returns the number in the top of the stuck ---------------*/
int Stack::stackReadTop()
{
	if (isStackEmpty())
		return -1;//returns negative if the stuck has no elements
	else
		return top->getValue();//returns the number
}
/*--------------- pop funtcion , removes the top number of the stuck---------------*/
int Stack::stackPop()
{
	if (isStackEmpty())//checks if there is no item in the stuck
	{
		cout<<"stack is empty";
		return -1;
	}
	else
	{
		int data;
		NodeStack* deleteNode;//auxiliary node for the deletion
		
		deleteNode = top;
		top = top->getNext();//moves the top to the next item
		data = deleteNode->getValue();//holds the data

		delete deleteNode;//deletes the node
		numOfElemsInStuck--;//decreases the items

		return data;//returns the number
	}
}
/*------------------------deletes completely the stuck------------------------*/
void Stack::stackDelete()
{
	int stackItems = numOfElemsInStuck;

	for(int i=0; i < stackItems; i++)
	{
		stackPop();//pops all the items
	}
}
/*-------------------------------------------------------------------------------*/
/*------------------------------auxiliary functions------------------------------*/
/*-------------------------------------------------------------------------------*/
/*---------returns the id of the stuck---------*/
int Stack::stackReadId(void)
{
	return stackIdNumber;
}
/*-----------sets the id of the stuck-----------*/
void Stack::stackSetId(int id)
{
	stackIdNumber = id;
}
/*-------checks if the stuck is empty-------*/
bool Stack::isStackEmpty()
{
	if (numOfElemsInStuck == 0)//stack is empty
		return true;
	else
		return false;
}
