#ifndef _Stack//checks for redeclaration
	#define _Stack
	#include "NodeStack.h"// definition NodeStack class
		class Stack
		{
			private :
				int numOfElemsInStuck;// number of items in the stuck
				NodeStack* top;//pointer to the first element of the stuck
				int stackIdNumber;// id of the stuck
			public : 
				Stack();//constructor
				~Stack();//destructor
				void stackCreate(int id);// create the stuck
				void stackPush(int number);// push a number in the stuck
				int stackReadTop(void);// reads the number , in the top of the stuck
				int stackPop();// // returns the the top of the stack
				void stackDelete(void);// deletes the stuck
				/*----auxiliary functions----*/
				bool isStackEmpty();// checks if the stuck is empty
				int stackReadId(void);// returns the id of the stuck
				void stackSetId(int id);// sets the is of the stuck

		};//end class Stack 
#endif