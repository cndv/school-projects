#include <iostream>
#include <fstream>
#include "Stack.h"//definition of the Stack class
#include "HeapMax.h"//definition of the HeapMax class
#include "HeapMin.h"//definition of the HeapMin class
#include "Hash.h"//definition of the Hash class
                
using namespace std;

void main()//main function
{
	Stack* stackTable;// a table of Stacks
	HeapMax* heapMaxTable;//a table of Heap Max 
	HeapMin* heapMinTable;//a table of Heap Min
	Hash* hashFirstTable;//a table of Hash
	//at least 20 of each data structure
	stackTable = new Stack[20];
	heapMaxTable = new HeapMax[20];
	heapMinTable = new HeapMin[20];
	hashFirstTable = new Hash[20];

	int stackAux[20],heapAux[20],hashAux[20];//Auxiliary tables holding the ids
	bool heapFlag[20];//a tableto determine whether it is min or max

	for(int i=0; i<20; i++)//initializeis all the tables to zero
	{
		stackAux[i]=0;
		heapAux[i]=0;
		hashAux[i]=0;
		heapFlag[i]=false;//true->max, false->min
	}
	ifstream infile;//definition of the input file
	ofstream outfile;//definition of the output file
	char myBuffer[100];  //each line at least 100 characters

	infile.open("input.txt", ios::in);
	outfile.open("output.txt", ios::out);

	/*reads the input.txt file*/
	int i=0;
	while(infile.getline(myBuffer,100))
	{
		char * str;
		str = strtok (myBuffer," ;");//strtok function breaks the string into smaller stirngs , according to its parameters
		while (str != NULL)
		{
			if((strcmp ("STACK_CREATE",str) == 0)){//STACK_CREATE
					str = strtok(NULL, ";");
					int id;
					id = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(stackAux[i]==0){
							stackAux[i]=id;//sets the id in the auxiliary table , for future checks
							stackTable[i].stackCreate(id);
							break;
						}
					}
					outfile << "STACK_CREATE SUCCESSFUL , WITH ID "<<id<<endl;//writes to the output.txt

			}
			else if (strcmp ("STACK_PUSH",str) == 0){//STACK_PUSH
					str = strtok(NULL,",");
					int id = atoi(str);//convert to integer , gets the number
					str = strtok(NULL,";");
					int num = atoi(str);//convert to integer , gets the number

					for(i=0; i<20; i++){
						if(stackAux[i]==id){
							stackTable[i].stackPush(num);
							break;
						}
					}
					outfile << "STACK_PUSH SUCCESSFUL , WITH ID : "<<id<<" and number : "<<num<<endl;//writes to the output.txt
			}
			else if (strcmp ("STACK_POP",str) == 0){//STACK_POP
					str = strtok(NULL,";");
					int id = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(stackAux[i]==id){
							stackTable[i].stackPop();
							break;
						}
					}
					outfile << "STACK_POP SUCCESSFUL , WITH ID : "<<id<<endl;//writes to the output.txt
			}
			else if (strcmp ("STACK_READ_TOP",str) == 0){//STACK_READ_TOP
					int top;
					str = strtok(NULL,";");
					int id = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(stackAux[i]==id){
							top = stackTable[i].stackReadTop();
							break;
						}
					}
					outfile << "STACK_READ_TOP SUCCESSFUL , WITH ID : "<<id<<" WITH TOP : "<<top<<endl;//writes to the output.txt
			}
			else if (strcmp ("STACK_DESTROY",str) == 0){//STACK_DESTROY
					str = strtok(NULL,";");
					int id = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(stackAux[i]==id){
							stackTable[i].stackDelete();
							stackAux[i]=0;// sets again the auxiliary table to zero
							break;
						}
					}
					outfile << "STACK_DESTROY SUCCESSFUL , WITH ID : "<<id<<endl;//writes to the output.txt
			}
			else if (strcmp ("HEAP_CREATE",str) == 0){//HEAP_CREATE
					str = strtok(NULL,",");
					int id = atoi(str);//convert to integer , gets the number
					str = strtok(NULL,";");
					int num = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(heapAux[i]==0){
							if(num == 0){//min heap
								heapFlag[i] = false;//it is min
								heapMinTable[i].heapMinCreate(id);
								heapAux[i]=id;//sets the id in the auxiliary table , for future checks
								break;
							}		
							else{//max heap
								heapFlag[i] = true;//it is max
								heapMaxTable[i].heapMaxCreate(id);
								heapAux[i]=id;//sets the id in the auxiliary table , for future checks
								break;
							}
						}
					}
					outfile << "HEAP_CREATE SUCCESSFUL , WITH ID : "<<id<<endl;//writes to the output.txt
			}
			else if (strcmp ("HEAP_INSERT",str) == 0){//HEAP_INSERT
					str = strtok(NULL,",");
					int id = atoi(str);//convert to integer , gets the number
					str = strtok(NULL,";");
					int num = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(heapAux[i]==id){
							if(heapFlag[i] == false){// is a heap min
								heapMinTable[i].heapMinInsert(num);
								break;
							}		
							else{//is a heap max
								heapMaxTable[i].heapMaxInsert(num);
								break;
							}
						}
					}
					outfile << "HEAP_INSERT SUCCESSFUL , WITH ID : "<<id<<" and number : "<<num<<endl;//writes to the output.txt
			}
			else if (strcmp ("HEAP_READ_TOP",str) == 0){//HEAP_READ_TOP
					str = strtok(NULL,";");
					int num;
					int id = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(heapAux[i]==id){
							if(heapFlag[i] == false){// is a heap min
								num = heapMinTable[i].heapMinReadTop();
								break;
							}		
							else{//is a heap max
								num = heapMaxTable[i].heapMaxReadTop();
								break;
							}
						}
					}
					outfile << "HEAP_READ_TOP SUCCESSFUL , WITH ID : "<<id<<" and the top is  : "<<num<<endl;//writes to the output.txt
			}
			else if (strcmp ("HEAP_DELETE_TOP",str) == 0)//HEAP_DELETE_TOP
			{
					str = strtok(NULL,";");
					int num;
					int id = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(heapAux[i]==id){
							if(heapFlag[i] == false){// is a heap min
								num = heapMinTable[i].heapMinDeleteTop();
								break;
							}		
							else{//is a heap max
								num = heapMaxTable[i].heapMaxDeleteTop();
								break;
							}
						}
					}
					outfile << "HEAP_DELETE_TOP SUCCESSFUL , WITH ID : "<<id<<endl;//writes to the output.txt
			}
			else if (strcmp ("HEAP_DESTROY",str) == 0){//HEAP_DESTROY
					str = strtok(NULL,";");
					int id = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(heapAux[i]==id){
							if(heapFlag[i] == false){// is a heap min
								heapMinTable[i].heapMinDestroy();
								heapAux[i]=0;//sets again the auxiliary table to zero
								break;
							}		
							else{//is a heap max
								heapMaxTable[i].heapMaxDestroy();
								heapAux[i]=0;//sets again the auxiliary table to zero
								heapFlag[i]=false;//sets the flag to false , to be in the initial mode again
								break;
							}
						}
					}
					outfile << "HEAP_DESTROY SUCCESSFUL , WITH ID : "<<id<<endl;//writes to the output.txt
			}
			else if (strcmp ("HASH_CREATE",str) == 0)//HASH_CREATE
			{
					str = strtok(NULL,",");
					int id = atoi(str);//convert to integer , gets the number
					str = strtok(NULL,";");
					int num = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(hashAux[i]==0){
							hashAux[i]=id;//sets the id in the auxiliary table , for future checks
							hashFirstTable[i].hashTableCreate(id,num);
							break;
						}
					}
					outfile << "HASH_CREATE SUCCESSFUL , WITH ID : "<<id<<endl;//writes to the output.txt
			}
			else if (strcmp("HASH_INSERT",str) == 0)//HASH_INSERT
			{
					str = strtok(NULL,",");
					int id = atoi(str);//convert to integer , gets the number
					str = strtok(NULL,";");
					int num = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(hashAux[i]==id){
							hashFirstTable[i].hashTableInsert(num);
							break;
						}
					}
					outfile << "HASH_INSERT SUCCESSFUL , WITH ID : "<<id<<" and number : "<<num<<endl;//writes to the output.txt
			}
			else if(strcmp("HASH_DELETE",str) == 0)//HASH_DELETE
			{
					str = strtok(NULL,",");
					int id = atoi(str);//convert to integer , gets the number
					str = strtok(NULL,";");
					int num = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(hashAux[i]==id){
							if(hashFirstTable[i].hashTableDelete(num))//there is the number in the hash table
								outfile << "HASH_DELETE SUCCESSFUL , WITH ID : "<<id<<" the number : "<<num<<" was deleted ."<<endl;//writes to the output.txt
							else//the number doesn't exist
								outfile<< "ERROR , didn't find the number."<<endl;//writes to the output.txt
							break;
						}
					}
			}
			else if (strcmp("HASH_SEARCH",str) == 0){//HASH_SEARCH
					str = strtok(NULL,",");
					int id = atoi(str);//convert to integer , gets the number
					str = strtok(NULL,";");
					int num = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(hashAux[i]==id){
							if(hashFirstTable[i].hashTableSearch(num))//there is the number in the hash table
								outfile << "HASH_SEARCH SUCCESSFUL , WITH ID : "<<id<<" the number : "<<num<<" was found ."<<endl;//writes to the output.txt
							else//the number doesn't exist
								outfile<< "ERROR , didn't find the number."<<endl;//writes to the output.txt
							break;
						}
					}
			}
			else if (strcmp("HASH_DESTROY",str) == 0){//HASH_DESTROY
					str = strtok(NULL,";");
					int id = atoi(str);//convert to integer , gets the number
					for(i=0; i<20; i++){
						if(hashAux[i]==id){
							if(hashFirstTable[i].hashTableDestroy())
								outfile << "HASH_DESTROY SUCCESSFUL , WITH ID : "<<id<<endl;//writes to the output.txt
							else//hash has no elements
								outfile<< "ERROR , the hash is empty."<<endl;//writes to the output.txt

							hashAux[i]=0;//sets again the auxiliary table to zero
							break;
						}
					}
			}//end if

			str = strtok (NULL, " ,;");

		}//end while(str != NULL)

	}//end while(infile.getline(myBuffer,100))

	infile.close();
	outfile.close();

}//end of main