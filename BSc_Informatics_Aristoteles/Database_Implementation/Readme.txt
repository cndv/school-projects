Database Implementation System: 
o	REM_RecordFileManager: Create, open, close of file records
o	REM_RecordFileHandle: Read, Insert, Delete, Edit file records
o	REM_RecordHandle: Data and ID management of record
o	REM_RecordID: Record id
o	REM_RecordFileScan: Search in the file system
o	REM_RecordFileSubheader: Helper class stored at the front of each file. Stores info for records
o	REM_PageSubheader: Hepler Class stored at the start of each page. Stores info for each records per page, using a bitmap
