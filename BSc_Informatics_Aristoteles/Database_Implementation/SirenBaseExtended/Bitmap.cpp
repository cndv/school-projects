//
//  Bitmap.cpp
//  
//
//  Created by Kostas Hart on 8/24/12.
//
//

#include "REM.h"

#include <cmath>
#include <cstring>
#include <cassert>

Bitmap::Bitmap(int numBits): size(numBits)
{
    buffer = new char[this->numChars()];
    // zero out to avoid valgrind warnings.
    memset((void*)buffer, 0, this->numChars());
    this->reset();
}

Bitmap::Bitmap(char * buf, int numBits): size(numBits)
{
    buffer = new char[this->numChars()];
    memcpy(buffer, buf, this->numChars());
}

int Bitmap::to_char_buf(char * b, int len) const //copy content to char buffer -
{
    assert(b != NULL && len == this->numChars());
    memcpy((void*)b, buffer, len);
    return 0;
}

Bitmap::~Bitmap()
{
    delete [] buffer;
}

int Bitmap::numChars() const
{
    int numChars = (size / 8);
    if((size % 8) != 0)
        numChars++;
    return numChars;
}

void Bitmap::reset()
{
    for( unsigned int i = 0; i < size; i++) {
        Bitmap::reset(i);
    }
}

void Bitmap::reset(unsigned int bitNumber)
{
    assert(bitNumber <= (size - 1));
    int byte = bitNumber/8;
    int offset = bitNumber%8;
    
    buffer[byte] &= ~(1 << offset);
}

void Bitmap::set(unsigned int bitNumber)
{
    assert(bitNumber <= size - 1);
    int byte = bitNumber/8;
    int offset = bitNumber%8;
    
    buffer[byte] |= (1 << offset);
}

void Bitmap::set()
{
    for( unsigned int i = 0; i < size; i++) {
        Bitmap::set(i);
    }
}

// void Bitmap::flip(unsigned int bitNumber)
// {
//   assert(bitNumber <= size - 1);
//   int byte = bitNumber/8;
//   int offset = bitNumber%8;

//   buffer[byte] ^= (1 << offset);
// }

bool Bitmap::test(unsigned int bitNumber) const
{
    assert(bitNumber <= size - 1);
    int byte = bitNumber/8;
    int offset = bitNumber%8;
    
    return buffer[byte] & (1 << offset);
}


ostream& operator <<(ostream & os, const Bitmap& b)
{
    os << "[";
    for(int i=0; i < b.getSize(); i++)
    {
        if( i % 8 == 0 && i != 0 )
            os << ".";
        os << (b.test(i) ? 1 : 0);
    }
    os << "]";
    return os;
}
