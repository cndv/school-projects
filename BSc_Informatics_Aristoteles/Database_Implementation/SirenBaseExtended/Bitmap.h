//
//  Bitmap.h
//  
//
//  Created by Kostas Hart on 8/24/12.
//
//

#ifndef ____Bitmap__
#define ____Bitmap__

#include <iostream>

class Bitmap {
public:
    Bitmap(int numBits);
    Bitmap(char * buf, int numBits); //deserialize from buf
    ~Bitmap();
    
    void set(unsigned int bitNumber);
    void set(); // set all bits to 1
    void reset(unsigned int bitNumber);
    void reset(); // set all bits to 0
    bool test(unsigned int bitNumber) const;
    
    int numChars() const; // return size of char buffer to hold bitmap
    int to_char_buf(char *, int len) const; //serialize content to char buffer
    int getSize() const { return size; }
private:
    unsigned int size;
    char * buffer;
};


#endif /* defined(____Bitmap__) */
