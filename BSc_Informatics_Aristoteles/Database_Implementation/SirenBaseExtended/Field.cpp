//
//  Field.cpp
//  
//
//  Created by Kostas Chart on 8/29/12.
//
//
#include "REM.h"

Field::Field()
{    
}
Field::~Field(){}

Field::Field(t_attrType attrType,
             int attrLength,
             int attrOffset,
             t_compOp compOp,
             void *value)
{
    this->attrType = attrType;
    this->attrLength = attrLength;
    this->attrOffset = attrOffset;
    this->compOp = compOp;
    
    if(compOp != NO_OP)
		memcpy(&this->value,&value,sizeof(value));
    
}