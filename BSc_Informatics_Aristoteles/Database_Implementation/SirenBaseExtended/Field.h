//
//  Field.h
//  
//
//  Created by Kostas Chart on 8/29/12.
//
//

#ifndef ____Field__
#define ____Field__

#include <iostream>
class Field
{
friend class REM_RecordFileScan;
public:
    Field();
    Field(t_attrType attrType,
          int attrLength,
          int attrOffset,
          t_compOp compOp,
          void *value);
    ~Field();
	t_rc CheckRecord();
private:
	//search variables
	t_attrType attrType;
	int attrLength;
	int attrOffset;
	t_compOp compOp;
	void *value;

};
#endif /* defined(____Field__) */
