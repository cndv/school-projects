#ifndef _REM_h
#define _REM_h

#include "REM_Parameters.h"
#include "REM_RecordFileHandle.h"
#include "REM_RecordFileManager.h"
#include "REM_RecordFileScan.h"
#include "REM_RecordID.h"
#include "REM_RecordHandle.h"
#include "REM_PageSubheader.h"
#include "REM_RecordFileSubheader.h"
#include "Field.h"
#include "Bitmap.h"

#include "UTILS_misc.h"

#endif
