//
//  REM_PageSubheader.cpp
//  
//
//  Created by Kostas Hart on 8/14/12.
//
//

#include "REM_PageSubheader.h"
#include "Bitmap.h"


REM_PageSubheader::REM_PageSubheader(int numOfFreeSlots)
{
    numberOfFreeSlots = numOfFreeSlots;
    numberOfSlotsPerPage = numOfFreeSlots;
    slotBitmap = new char(this->mapsize());
}

REM_PageSubheader::~REM_PageSubheader()
{
    delete [] slotBitmap;
}

int REM_PageSubheader::size() const
{
    return sizeof(nextFreePageID) + sizeof(numberOfSlotsPerPage) + sizeof(numberOfFreeSlots)
    + Bitmap(numberOfSlotsPerPage).numChars()*sizeof(char);
}

int REM_PageSubheader::mapsize() const
{
    return this->size() - sizeof(nextFreePageID) - sizeof(numberOfSlotsPerPage) - sizeof(numberOfFreeSlots);
}

int REM_PageSubheader::to_buf(char *& buf) const
{
    memcpy(buf, &nextFreePageID, sizeof(nextFreePageID));
    memcpy(buf + sizeof(nextFreePageID), &numberOfSlotsPerPage, sizeof(numberOfSlotsPerPage));
    memcpy(buf + sizeof(nextFreePageID) + sizeof(numberOfSlotsPerPage),
           &numberOfFreeSlots, sizeof(numberOfFreeSlots));
    memcpy(buf + sizeof(nextFreePageID) + sizeof(numberOfSlotsPerPage) + sizeof(numberOfFreeSlots),
           slotBitmap, this->mapsize()*sizeof(char));
    return 0;
}

int REM_PageSubheader::from_buf(const char * buf)
{
    memcpy(&nextFreePageID, buf, sizeof(nextFreePageID));
    memcpy(&numberOfSlotsPerPage, buf + sizeof(nextFreePageID), sizeof(numberOfSlotsPerPage));
    memcpy(&numberOfFreeSlots, buf + sizeof(nextFreePageID) + sizeof(numberOfSlotsPerPage),
           sizeof(numberOfFreeSlots));
    memcpy(slotBitmap,
           buf + sizeof(nextFreePageID) + sizeof(numberOfSlotsPerPage) + sizeof(numberOfFreeSlots),
           this->mapsize()*sizeof(char));
    return 0;
}