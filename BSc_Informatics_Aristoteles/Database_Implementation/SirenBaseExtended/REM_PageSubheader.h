//
//  REM_PageSubheader.h
//  
//
//  Created by Kostas Hart on 8/14/12.
//
//

#ifndef ____REM_PageSubheader__
#define ____REM_PageSubheader__

#include <iostream>

class REM_PageSubheader{
    public:
        int nextFreePageID;
        int numberOfSlotsPerPage;
        int numberOfFreeSlots;
        char * slotBitmap;

        REM_PageSubheader(int numOfFreeSlots);
        ~REM_PageSubheader();
        int size() const;
        int mapsize() const;
        int to_buf(char *& buf) const;
        int from_buf(const char * buf);
};

#endif /* defined(____REM_PageSubheader__) */
