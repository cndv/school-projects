#ifndef _REM_Parameters_h
#define _REM_Parameters_h
#define MAX_NAME_ATTRIBUTE 50

typedef enum {
    EQ_OP ,// equal
    LT_OP ,// less than
    GT_OP ,// greater than 
    NE_OP ,// not equal
    LE_OP ,// less than or equal
    GE_OP ,// greater than or equal
    NO_OP  // No comparison. Sould be used when file scan value is NULL. 
} t_compOp;

typedef enum {
    TYPE_INT ,  // integer
    TYPE_STRING, // up to 255 characters
    TYPE_FLOAT,
    TYPE_DOUBLE
} t_attrType;

#endif