#include "REM.h"
#include "math.h"
#include "STORM.h"

REM_RecordFileHandle::REM_RecordFileHandle():fileIsOpen(false),fileHeaderIsChanged(false),stormFileHandle(NULL) {}
REM_RecordFileHandle::~REM_RecordFileHandle() {}

t_rc REM_RecordFileHandle::ReadRecord (const REM_RecordID &rid, REM_RecordHandle &rh) const
{
   
    int pageNum;
    int slotNum;
    rid.GetPageID(pageNum);
    rid.GetSlot(slotNum);
    t_rc rc = OK;
    STORM_PageHandle ph;
    REM_PageSubheader pHdr(remRecordFileSubheader.recordsPerPage);
    if((rc = stormFileHandle->GetPage(pageNum, ph)) ||
       (rc = stormFileHandle->UnpinPage(pageNum)) ||
       (rc = this->getPageHeader(ph, pHdr))
       )
        return rc;
    Bitmap b(pHdr.slotBitmap,remRecordFileSubheader.recordsPerPage);
    
    if(b.test(slotNum)) // already free
        return REM_NO_REC_WITH_THIS_RID;
    
    char * pData = NULL;
    if(t_rc rc = this->GetSlotPointer(ph, slotNum, pData))
        return rc;
    
    rh.SetData(*pData, rid);

    return OK;
}


t_rc REM_RecordFileHandle::InsertRecord (const char *pData, REM_RecordID &rid)
{
    STORM_PageHandle stormPageHandle;
    stormFileHandle->GetFirstPage(stormPageHandle);
    this->set_fileSubheader(stormPageHandle);
    REM_PageSubheader remPageSubHeader(remRecordFileSubheader.recordsPerPage);
    
    int pageNum ;
    int slotNum ;
    t_rc rc;
    char * pSlot;
    
    if((rc = this->getFreeSlot(stormPageHandle, pageNum, slotNum)))
        return rc;
    if((rc = this->getPageHeader(stormPageHandle, remPageSubHeader)))
        return rc;
    Bitmap b(remPageSubHeader.slotBitmap,remRecordFileSubheader.recordsPerPage );

    // TODO GetSlotPtr is trashing the pHdr
    if((rc = this->GetSlotPointer(stormPageHandle, slotNum, pSlot)))
        return rc;
    rid = REM_RecordID(pageNum, slotNum);
    memcpy(pSlot, pData, remRecordFileSubheader.recordSize);
    
    b.reset(slotNum); // slot slotNum is no longer free
    remPageSubHeader.numberOfFreeSlots--;
    if(    remPageSubHeader.numberOfFreeSlots == 0) {
        // remove from free list
        remRecordFileSubheader.firstFreePageID = remPageSubHeader.nextFreePageID;
        remPageSubHeader.nextFreePageID = REM_PAGE_IS_FULL;
    }
        b.to_char_buf(remPageSubHeader.slotBitmap, b.numChars());
    rc = this->setPageHeader(stormPageHandle, remPageSubHeader);
    return OK;
}

t_rc REM_RecordFileHandle::DeleteRecord (const REM_RecordID &rid)
{
    int pageNum;
    int slotNum;
    rid.GetPageID(pageNum);
    rid.GetSlot(slotNum);
    t_rc rc;
    STORM_PageHandle ph;
    REM_PageSubheader pHdr(remRecordFileSubheader.recordsPerPage);
    if((rc = stormFileHandle->GetPage(pageNum, ph)) ||
       (rc = stormFileHandle->MarkPageDirty(pageNum)) ||
       // Needs to be called everytime GetThisPage is called.
       (rc = stormFileHandle->UnpinPage(pageNum)) ||
       (rc = this->getPageHeader(ph, pHdr))
       )
        return rc;
    
    Bitmap b(pHdr.slotBitmap, remRecordFileSubheader.recordsPerPage);
    
    if(b.test(slotNum)) // already free
        return REM_NO_REC_WITH_THIS_RID;
    
    // TODO considering zero-ing record - IOs though
    b.set(slotNum); // slotNum is now free
    if(pHdr.numberOfFreeSlots == 0)
    {
        // this page used to be full and used to not be on the free list
        // add it to the free list now.
        pHdr.nextFreePageID = remRecordFileSubheader.firstFreePageID;
        remRecordFileSubheader.firstFreePageID = pageNum;
    }
    pHdr.numberOfFreeSlots++;
    // std::cerr << "RM_FileHandle::DeleteRec numFreeSlots in page "
    //           << pHdr.numFreeSlots
    //           << std::endl;
    b.to_char_buf(pHdr.slotBitmap, b.numChars());
    rc = this->setPageHeader(ph, pHdr);
    return rc;

    return OK;
}

t_rc REM_RecordFileHandle::UpdateRecord (const REM_RecordHandle &rh)
{
    REM_RecordID rid;
    rh.GetRecordID(rid);
    int pageNum;
    int slotNum;
    rid.GetPageID(pageNum);
    rid.GetSlot(slotNum);
    
    STORM_PageHandle ph;
    char * pSlot;
    t_rc rc;
    REM_PageSubheader pHdr(remRecordFileSubheader.recordsPerPage);
    if((rc = stormFileHandle->GetPage(pageNum, ph)) ||
       (rc = stormFileHandle->MarkPageDirty(pageNum)) ||
       // Needs to be called everytime GetThisPage is called.
       (rc = stormFileHandle->UnpinPage(pageNum)) ||
       (rc = this->getPageHeader(ph, pHdr))
       )
        return rc;
    
    Bitmap b(pHdr.slotBitmap, remRecordFileSubheader.recordsPerPage);
    
    if(b.test(slotNum)) // free - cannot update
        return REM_NO_REC_WITH_THIS_RID;
    
    char * pData = NULL;
    rh.GetData(pData);
    
    if(t_rc rc = this->GetSlotPointer(ph, slotNum, pSlot))
        return rc;
    memcpy(pSlot, pData, remRecordFileSubheader.recordSize);

    return OK;
}

t_rc REM_RecordFileHandle::FlushPages () const
{
    if (!fileIsOpen)
    {
        return STORM_FILEALREADYCLOSED;
    }
    return stormFileHandle->FlushAllPages();
    
}

t_rc REM_RecordFileHandle::get_fileSubheader(STORM_PageHandle stormPageHandle)
{
    char * pData;
    
    stormPageHandle.GetDataPtr(&pData);
    memcpy(&remRecordFileSubheader, pData, sizeof(remRecordFileSubheader));
    return OK;
}

t_rc REM_RecordFileHandle::set_fileSubheader(STORM_PageHandle stormPageHandle)
{
    char * pData;
    stormPageHandle.GetDataPtr(&pData);
    memcpy(&remRecordFileSubheader, pData, sizeof(remRecordFileSubheader));
    return OK;
}

void REM_RecordFileHandle::setOpenFileFlag(bool fileIsOpenn)
{
    fileIsOpen = fileIsOpen;
}

bool REM_RecordFileHandle::getOpenFileFlag(void)
{
    return fileIsOpen;
}
bool REM_RecordFileHandle::isFileHeaderChanged(void)
{
    return fileHeaderIsChanged;
}

void REM_RecordFileHandle::setFileHeaderIsChangedFlag(bool fileHeaderIsChangedFlag)
{
    fileHeaderIsChanged = fileHeaderIsChangedFlag;
}

t_rc REM_RecordFileHandle::getPageHeader(STORM_PageHandle ph, REM_PageSubheader& pHdr) const
{
    char * buf;
    t_rc rc = ph.GetDataPtr(&buf);
    pHdr.from_buf(buf);
    return rc;
}

t_rc REM_RecordFileHandle::setPageHeader(STORM_PageHandle ph, const REM_PageSubheader& pHdr)
{
    char * buf;
    t_rc rc;
    if((rc = ph.GetDataPtr(&buf)))
        return rc;
    pHdr.to_buf(buf);
    return OK;
}

t_rc REM_RecordFileHandle::getFreePage(int &pageId)
{
    STORM_PageHandle ph;
    REM_PageSubheader pHdr(remRecordFileSubheader.recordsPerPage);
    int pageNum;
    
    if(remRecordFileSubheader.firstFreePageID != REM_END_OF_LIST_PAGES)//// only the fist time remRecordFileSubheader.firstFree isEqualTo RM_PAGE_LIST_END
    {
        // this last page on the free list might actually be full
        t_rc rc;
        if ((rc = stormFileHandle->GetPage(remRecordFileSubheader.firstFreePageID, ph))
            || (rc = ph.GetPageID(pageNum))
            || (rc = stormFileHandle->MarkPageDirty(pageNum))
            // Needs to be called everytime GetThisPage is called.
            || (rc = stormFileHandle->UnpinPage(remRecordFileSubheader.firstFreePageID))
            || (rc = this->getPageHeader(ph, pHdr)))
            return rc;
        
    }
    if( //we need to allocate a new page
       // because this is the firs time
       remRecordFileSubheader.numberOfPages == 1 ||
       remRecordFileSubheader.firstFreePageID == REM_END_OF_LIST_PAGES ||
       // or due to a full page
       (pHdr.numberOfFreeSlots == 0)
       )
    {
        char *pData;
        t_rc rc;
        
        if ((rc = stormFileHandle->ReservePage(ph)) ||
            (rc = ph.GetDataPtr(&pData)) ||
            (rc = ph.GetPageID(pageNum)))
            return(rc);
        
        // Add page header
        REM_PageSubheader phdr(remRecordFileSubheader.recordsPerPage);
        phdr.nextFreePageID = REM_END_OF_LIST_PAGES;
        Bitmap b(remRecordFileSubheader.recordsPerPage);
        b.set(); // Initially all slots are free
        b.to_char_buf(phdr.slotBitmap, b.numChars());
        // std::cerr << "RM_FileHandle::GetNextFreePage new!!" << b << endl;
        phdr.to_buf(pData);
        
        // the default behavior of the buffer pool is to pin pages
        // let us make sure that we unpin explicitly after setting
        // things up
        if ((rc = stormFileHandle->UnpinPage(pageNum)))
            return rc;
        
        // add page to the free list
        remRecordFileSubheader.firstFreePageID = pageNum;
        remRecordFileSubheader.numberOfPages++;
        fileHeaderIsChanged = true;
        return OK; // pageNum is set correctly
    }
    // return existing free page
    pageNum = remRecordFileSubheader.firstFreePageID;
    return OK;
}

t_rc REM_RecordFileHandle::getFreeSlot(STORM_PageHandle & ph, int & pageNum, int & slotNum)
{
    REM_PageSubheader pHdr(this->remRecordFileSubheader.recordsPerPage);
    t_rc rc;
    
    if ((rc= this->getFreePage(pageNum))
        || (rc = stormFileHandle->GetPage(pageNum, ph))
        // Needs to be called everytime GetThisPage is called.
        || (rc = stormFileHandle->UnpinPage(pageNum))
        || (rc = this->getPageHeader(ph, pHdr)))
        return rc;
    Bitmap b(pHdr.slotBitmap, this->remRecordFileSubheader.recordsPerPage);
    for (int i = 0; i < this->remRecordFileSubheader.recordsPerPage; i++)
    {
        if (b.test(i)) {
            slotNum = i;
            return OK;
        }
    }
    // This page is full
    return UNKNOWN_ERROR; // unexpected error
    
}

t_rc REM_RecordFileHandle::GetSlotPointer(STORM_PageHandle ph, int slot, char *& pData) const
{
    t_rc rc = ph.GetDataPtr(&pData);
    if (rc >= 0 ) {
        Bitmap b(remRecordFileSubheader.numberOfRecords);
        pData = pData + (REM_PageSubheader(remRecordFileSubheader.numberOfRecords).size());
        pData = pData + slot * remRecordFileSubheader.recordSize;
    }
    
    return rc;
}

t_rc REM_RecordFileHandle::setStormFileHandle(STORM_FileHandle &sfh)
{
    stormFileHandle = &sfh;
    return OK;
}
