#ifndef _REM_RecordFileHandle_h
#define _REM_RecordFileHandle_h

#include "REM_RecordID.h"
#include "REM_RecordHandle.h"

#include "retcodes.h"
#include "STORM.h"
#include "REM_RecordFileSubheader.h"
#include "REM_PageSubheader.h"
#define REM_END_OF_LIST_PAGES -1
#define REM_PAGE_IS_FULL      -2       // page is fully used with no free slots


class REM_RecordFileHandle
{
    friend class REM_RecordFileManager;
    friend class REM_RecordFileScan;

    public:
        REM_RecordFileHandle(); 
        ~REM_RecordFileHandle(); 
        
        t_rc ReadRecord (const REM_RecordID &rid, REM_RecordHandle &rh) const;
        t_rc InsertRecord (const char *pData, REM_RecordID &rid);
        t_rc DeleteRecord (const REM_RecordID &rid);
        t_rc UpdateRecord (const REM_RecordHandle &rh);
        t_rc FlushPages () const;
        
        
    private:
        bool fileIsOpen;
        bool fileHeaderIsChanged;
        STORM_FileHandle * stormFileHandle;
        REM_RecordFileSubheader remRecordFileSubheader;
    
        bool isFileHeaderChanged(void);
        void setFileHeaderIsChangedFlag(bool fileHeaderIsChangedFlag);
        t_rc set_fileSubheader(STORM_PageHandle stormPageHandle);
        t_rc get_fileSubheader(STORM_PageHandle stormPageHandle);
        void setOpenFileFlag(bool fileIsOpenn);
        bool getOpenFileFlag(void);
        
        t_rc setStormFileHandle(STORM_FileHandle &sfh);
        t_rc getPageHeader(STORM_PageHandle ph, REM_PageSubheader & pHdr) const;
        t_rc setPageHeader(STORM_PageHandle ph, const REM_PageSubheader& pHdr);
        t_rc getFreePage(int &pageId);
        t_rc getFreeSlot(STORM_PageHandle & ph, int & pageNum, int & slotNum);

        t_rc GetSlotPointer(STORM_PageHandle ph, int slot, char *& pData) const;
};

#endif