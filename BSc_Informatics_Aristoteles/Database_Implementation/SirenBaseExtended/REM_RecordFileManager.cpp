#include "STORM.h"
#include "retcodes.h"
#include <iostream>
#include <cstdlib>
#include <cerrno>
#include "REM.h"
#include "math.h"
#include "REM_RecordFileHandle.h"

#ifdef _WIN32
#include <io.h>
#endif


REM_RecordFileManager::REM_RecordFileManager(STORM_StorageManager &sm):stormStorageManager(sm)
{
}

t_rc REM_RecordFileManager::CreateRecordFile (const char *fname, int rs)
{
	int recsPerPage = this->getRecordsPerPage(rs);
    t_rc rc;
    if (recsPerPage < 1) {
        return REM_RECORDSIZETOOBIG;
    }
    
    rc = stormStorageManager.CreateFile(fname);
	if(rc != OK)
		return rc;
    
    STORM_FileHandle stormFileHandle;
	rc = stormStorageManager.OpenFile(fname,stormFileHandle);
	if(rc != OK)
		return rc;
    
    STORM_PageHandle stormPageHandle;
	rc = stormFileHandle.ReservePage(stormPageHandle);
	if(rc != OK)
		return rc;
    
    char *pData;
	int pageId;
    
	//Get the pointer of the data
	rc = stormPageHandle.GetDataPtr(&pData);
	if(rc != OK)
		return rc;
    
	//Get the pageId
	rc = stormPageHandle.GetPageID(pageId);
	if(rc != OK)
		return rc;
    
    REM_RecordFileSubheader remRecordFileSubheader(REM_END_OF_LIST_PAGES, 1, rs, 0, recsPerPage);
    memcpy(pData, &remRecordFileSubheader, sizeof(remRecordFileSubheader));
    
    //Mark the page as dirty
	rc = stormFileHandle.MarkPageDirty(pageId);
	if(rc != OK)
		return rc;
    
    //Unpin the page
	rc = stormFileHandle.UnpinPage(pageId);
	if(rc != OK)
		return rc;
    
    //Close the file
	rc = stormStorageManager.CloseFile(stormFileHandle);
	if(rc != OK)
		return rc;
    
    return OK;
}

t_rc REM_RecordFileManager::OpenRecordFile (const char *fname, REM_RecordFileHandle &rfh)
{
    //rfh.openFile(stormStorageManager, fname);
    
    //Open the file
	t_rc rc;
    STORM_FileHandle stormFileHandle;
	rc = this->stormStorageManager.OpenFile(fname,stormFileHandle);
	if(rc != OK)
		return rc;
    
//TODO put rc checks
    STORM_PageHandle stormPageHandle;

    stormFileHandle.GetFirstPage(stormPageHandle);
    
    rfh.get_fileSubheader(stormPageHandle);
    rfh.setOpenFileFlag(true);
    rfh.setStormFileHandle(stormFileHandle);
    return OK;
    
}

t_rc REM_RecordFileManager::CloseRecordFile (REM_RecordFileHandle &rfh)
{
    //Check if the file is already closed
	if(!rfh.getOpenFileFlag())
		return STORM_FILEALREADYCLOSED;
    
    if(rfh.isFileHeaderChanged())
    {
        STORM_PageHandle stormPageHandle;
        rfh.stormFileHandle->GetFirstPage(stormPageHandle);
        int pageId;
        stormPageHandle.GetPageID(pageId);
        rfh.stormFileHandle->MarkPageDirty(pageId);
        rfh.stormFileHandle->UnpinPage(pageId);
        rfh.FlushPages();
    }

    
    stormStorageManager.CloseFile(*rfh.stormFileHandle);
    rfh.setOpenFileFlag(false);
    rfh.setFileHeaderIsChangedFlag(false);
    return OK;
    
    
}

t_rc REM_RecordFileManager::DestroyRecordFile(const char *fname)
{
    return stormStorageManager.DestroyFile(fname);

}
int REM_RecordFileManager::getRecordsPerPage(int rs)
{
    int recordsPerPage;
    recordsPerPage = (PAGE_DATA_SIZE - floor(sizeof(REM_PageSubheader))/rs);
    return recordsPerPage;
    
}
