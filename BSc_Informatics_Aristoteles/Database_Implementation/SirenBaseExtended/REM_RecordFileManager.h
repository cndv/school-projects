#ifndef _REM_RecordFileManager_h
#define _REM_RecordFileManager_h

#include "retcodes.h"
#include "STORM.h"
#include "REM_RecordFileHandle.h"

class REM_RecordFileManager
{
public:
    REM_RecordFileManager (STORM_StorageManager &sm);
	~REM_RecordFileManager() {}
    
    t_rc CreateRecordFile (const char *fname, int rs);	// rs = recordSize in bytes
    t_rc DestroyRecordFile (const char *fname);
    t_rc OpenRecordFile (const char *fname, REM_RecordFileHandle &rfh);
    t_rc CloseRecordFile (REM_RecordFileHandle &rfh);
    
private:
    int getRecordsPerPage(int rs);
    STORM_StorageManager &stormStorageManager;
    friend class REM_RecordFileHandle;
};

#endif