#include "REM_RecordFileScan.h"
REM_RecordFileScan::REM_RecordFileScan():fileIsOpen(false),pageId(2),slot(0)
{
}
REM_RecordFileScan::~REM_RecordFileScan(){}

t_rc REM_RecordFileScan::OpenRecordScan (const REM_RecordFileHandle &rfh, 
                     t_attrType attrType,
                     int        attrLength,
                     int        attrOffset,
                     t_compOp   compOp,
                     void       *value)
{
    this->rfh = &rfh;
    testField = Field(attrType,attrLength,attrOffset,compOp,value);
    fileIsOpen = true;
    return OK;
}

t_rc REM_RecordFileScan::GetNextRecord (REM_RecordHandle &rh)
{
    if(!fileIsOpen)
		return STORM_FILEALREADYCLOSED;
    
	t_rc rc;
    
	REM_RecordID rid;
	    
	if( pageId > rfh->stormFileHandle->GetNumReservedPages())
		return REM_PAGEID_OUT_OF_SCOPE;
    
    
	for( ;pageId<=rfh->stormFileHandle->GetNumReservedPages();pageId++){
		rc = rid.SetPageID(pageId);
		if(rc != OK)
			return rc;
        
		if( slot > rfh->remRecordFileSubheader.recordsPerPage )
			slot=0;
        
		for( ;slot<=rfh->remRecordFileSubheader.recordsPerPage;slot++){
			rc = rid.SetSlot(slot);
			if(rc != OK)
				return rc;
            
			rc = rfh->ReadRecord(rid,rh);
			if(rc != OK)
				return rc;
            
			rc = rh.GetData(pData);
			if(rc != OK)
				return rc;
            
			rc = findRecord();
			if(rc == OK){
				slot++;
				return OK;
			}
		}
	}
    
	return REM_NO_RECORD_FOUND;
    return OK;
}

t_rc REM_RecordFileScan::CloseRecordScan()
{
    if(!fileIsOpen)
		return STORM_FILEALREADYCLOSED;
    
	fileIsOpen = false;
	return OK;
}

t_rc REM_RecordFileScan::findRecord()
{
    char *val;
	val = (char *) malloc (testField.attrLength);
	memset (val, 0, testField.attrLength);
	memcpy(val,&pData[testField.attrOffset],testField.attrLength);
    
	int intConvertion;
	int intComparison;
	float floatConvertion,floatComparison;
	double doubleConvertion,doubleComparison;
	char *charConvertion,*charComparison;
	
    //Check attribute Type
	if(testField.attrType == TYPE_INT){
		memcpy(&intConvertion, &pData+testField.attrOffset, testField.attrLength);
        
		intComparison = *static_cast<int*>( testField.value);
	}
    else if(testField.attrType == TYPE_FLOAT){
     floatConvertion = atof(val);
     
     floatComparison = *static_cast<float*>( testField.value);
     }
     else if(testField.attrType == TYPE_DOUBLE){
          
         doubleComparison = *static_cast<double*>( testField.value);

     }
	else if(testField.attrType == TYPE_STRING){
		charConvertion = (char *) malloc (MAX_NAME_ATTRIBUTE);
                
		memcpy(charConvertion, &val+testField.attrOffset,MAX_NAME_ATTRIBUTE);
        
		charComparison = (char*) testField.value;
	}
	
	//Check Comparison operator
	if(testField.compOp == EQ_OP){
        
		if(testField.attrType == TYPE_INT){
            
			if(intConvertion == intComparison)
				return OK;
		}/*else if(attrType == TYPE_DOUBLE){
          
          if(doubleConvertion == doubleComparison)
          return OK;
          
          }else if(attrType == TYPE_FLOAT){
          
          if(floatConvertion == floatComparison)
          return OK;
          
          }
          */
        else if(testField.attrType == TYPE_STRING){
            
            if(strncmp(charConvertion,charComparison,testField.attrLength)==0)
                return OK;
        }
	}
	else if(testField.compOp == LT_OP){
		if(testField.attrType == TYPE_INT){
			if(intConvertion < intComparison)
				return OK;
		}/*else if(attrType == TYPE_DOUBLE){
          if(doubleConvertion < doubleComparison)
          return OK;
          }else if(attrType == TYPE_FLOAT){
          if(floatConvertion < floatComparison)
          return OK;
          }*/
		else if(testField.attrType == TYPE_STRING){
			if(strncmp(charConvertion,charComparison,testField.attrLength)<0)
				return OK;
		}
	}
	else if(testField.compOp == GT_OP){
        
		if(testField.attrType == TYPE_INT){
            
			if(intConvertion > intComparison)
				return OK;
            
		}/*else if(attrType == TYPE_DOUBLE){
          
          if(doubleConvertion > doubleComparison)
          return OK;
          
          }else if(attrType == TYPE_FLOAT){
          
          if(floatConvertion > floatComparison)
          return OK;
          }*/
		else if(testField.attrType == TYPE_STRING){
			if(strncmp(charConvertion,charComparison,testField.attrLength)>0)
				return OK;
		}
	}
	else if(testField.compOp == NE_OP){
		if(testField.attrType == TYPE_INT){
			if(intConvertion != intComparison)
				return OK;
		}/*else if(attrType == TYPE_DOUBLE){
          if(doubleConvertion != doubleComparison)
          return OK;
          }else if(attrType == TYPE_FLOAT){
          if(floatConvertion != floatComparison)
          return OK;
          }*/
		else if(testField.attrType == TYPE_STRING){
			if(strncmp(charConvertion,charComparison,testField.attrLength)!=0)
				return OK;
		}
	}
	else if(testField.compOp == LE_OP){
		if(testField.attrType == TYPE_INT){
			if(intConvertion <= intComparison)
				return OK;
		}/*else if(attrType == TYPE_DOUBLE){
          if(doubleConvertion <= doubleComparison)
          return OK;
          }else if(attrType == TYPE_FLOAT){
          if(floatConvertion <= floatComparison)
          return OK;
          }*/
		else if(testField.attrType == TYPE_STRING){
			if(strncmp(charConvertion,charComparison,testField.attrLength)<=0)
				return OK;
		}
	}
	else if(testField.compOp == GE_OP){
		if(testField.attrType == TYPE_INT){
			if(intConvertion >= intComparison)
				return OK;
		}/*else if(attrType == TYPE_DOUBLE){
          if(doubleConvertion >= doubleComparison)
          return OK;
          }else if(attrType == TYPE_FLOAT){
          if(floatConvertion >= floatComparison)
          return OK;
          }*/
		else if(testField.attrType == TYPE_STRING){
			if(strncmp(charConvertion,charComparison,testField.attrLength)>=0)
				return OK;
		}
	}
	else if(testField.compOp == NO_OP){
		return OK;
	}
}