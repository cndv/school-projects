#ifndef _REM_RecordFileScan_h
#define _REM_RecordFileScan_h

#import "REM_Parameters.h"
#include "retcodes.h"
#include "REM_RecordFileHandle.h"
#include "Field.h"

class REM_RecordFileScan
{
public:
    REM_RecordFileScan();
    ~REM_RecordFileScan();
    
    t_rc OpenRecordScan (const REM_RecordFileHandle &rfh, 
                         t_attrType attrType,
                         int        attrLength,
                         int        attrOffset,
                         t_compOp   compOp,
                         void       *value);
    t_rc GetNextRecord (REM_RecordHandle &rh);
    t_rc CloseRecordScan();
private:
    Field testField;
    int pageId;
	int slot;
    bool fileIsOpen;
    char *pData;
    const REM_RecordFileHandle *rfh;
    t_rc findRecord();
};
#endif