//
//  REM_RecordFileSubheader.cpp
//  
//
//  Created by Kostas Hart on 8/14/12.
//
//
#include "REM.h"

REM_RecordFileSubheader::REM_RecordFileSubheader(){}

REM_RecordFileSubheader::~REM_RecordFileSubheader(){}

REM_RecordFileSubheader::REM_RecordFileSubheader(int firstFreePageID,
                                                 int numberOfPages,
                                                 int recordSize,
                                                 int numberOfRecords,
                                                 int recordsPerPage
                                                 ): firstFreePageID(firstFreePageID),
                                                numberOfPages(numberOfPages),
                                                recordSize(recordSize),
                                                numberOfRecords(numberOfRecords),
                                                recordsPerPage(recordsPerPage)
{
    
}