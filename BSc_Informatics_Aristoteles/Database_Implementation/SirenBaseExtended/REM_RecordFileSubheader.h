//
//  REM_RecordFileSubheader.h
//  
//
//  Created by Kostas Hart on 8/14/12.
//
//

#ifndef ____REM_RecordFileSubheader__
#define ____REM_RecordFileSubheader__

#include <iostream>

class REM_RecordFileSubheader{
    public:
    REM_RecordFileSubheader();
    ~REM_RecordFileSubheader();
    REM_RecordFileSubheader(int firstFreePageID,
                            int numberOfPages,
                            int recordSize,
                            int numberOfRecords,
                            int recordsPerPage);
        int firstFreePageID;
        int numberOfPages;
        int recordSize;
        int numberOfRecords;
        int recordsPerPage;
    
    friend class REM_RecordFileHandle;
    friend class REM_RecordFileManager;
};


#endif /* defined(____REM_RecordFileSubheader__) */
