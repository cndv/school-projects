//
//  REM_RecordHandle.cpp
//  SdbMac25_3
//
//  Created by Con Ch on 4/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "REM_RecordHandle.h"

REM_RecordHandle::REM_RecordHandle()
{
}

REM_RecordHandle::~REM_RecordHandle()
{
}

t_rc REM_RecordHandle::GetData (char *&pDataq) const
{
    pDataq = this->rpData;
    return OK;
}

t_rc REM_RecordHandle::GetRecordID (REM_RecordID &rid) const
{
    rid = this->rid;
    return OK;
}

t_rc REM_RecordHandle::SetData(char &pData,const REM_RecordID &rid)
{
    this->rid = rid;
    this->rpData = &pData;
    return OK;
}
