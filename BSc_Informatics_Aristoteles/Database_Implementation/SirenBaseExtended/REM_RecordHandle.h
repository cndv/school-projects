//
//  REM_RecordHandle.h
//  SdbMac25_3
//
//  Created by Con Ch on 4/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//
#ifndef _REM_RecordHandle_h
#define _REM_RecordHandle_h

#include "retcodes.h"
#include "REM_RecordID.h"

class REM_RecordHandle
{
    public:
        REM_RecordHandle();
        ~REM_RecordHandle();
        
        t_rc GetData (char *&pDataq) const;
        t_rc GetRecordID (REM_RecordID &rid) const;
    private:
        char* rpData;
        REM_RecordID rid;
        t_rc SetData(char &pData,const REM_RecordID &rid);

    friend class REM_RecordFileHandle;
};

#endif