//
//  REM_RecordID.cpp
//  SdbMac25_3
//
//  Created by Con Ch on 4/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#include "REM_RecordID.h"


REM_RecordID::REM_RecordID()
{
    this->pageID = 0;
    this->slot = 0;
}

REM_RecordID::REM_RecordID(int pageID, int slot)
{
    this->pageID = pageID;
    this->slot = slot;
}

REM_RecordID::~REM_RecordID()
{
}

t_rc REM_RecordID::GetPageID (int &pageID) const
{
    pageID  = this->pageID;
    return OK;
}
t_rc REM_RecordID::GetSlot (int &slot) const
{
    slot = this->slot;
    return OK;
}
t_rc REM_RecordID::SetPageID (int pageID)
{
    this->pageID = pageID;
    return OK;
}
t_rc REM_RecordID::SetSlot (int slot)
{
    this->slot = slot;
    return OK;
}