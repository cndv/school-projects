//
//  REM_RecordID.h
//  SdbMac25_3
//
//  Created by Con Ch on 4/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef _REM_RecordID_h
#define _REM_RecordID_h

#include "retcodes.h"


class REM_RecordID{
public:
    REM_RecordID();
    REM_RecordID(int pageID, int slot);
    ~REM_RecordID();
    
    t_rc GetPageID (int &pageID) const;
    t_rc GetSlot (int &slot) const;
    t_rc SetPageID (int pageID);
    t_rc SetSlot (int slot);
private:
    int pageID;
    int slot;
};


#endif