#include "SSQLM.h"
#include "REM.h"
#include <string>

SSQLM::SSQLM(STORM_StorageManager &sm){
	//Arxikopoihsh ton modules pou tha xreiastoume
	STORM_sm=&sm;
	REM_rfm=new REM_RecordFileManager(sm);
	REM_rfh=new REM_RecordFileHandle();
	REM_rh=new REM_RecordHandle();
	rid=new REM_RecordID();
	scan=new REM_RecordFileScan();
}

//STHN EPOMENH ERGASIA...
t_rc SSQLM::CreateIndex(){
	return OK;
}

//STHN EPOMENH ERGASIA...
t_rc SSQLM::DropIndex(){
	return OK;
}

t_rc SSQLM::CreateTable(char *TableName,int no_attr,char *attrName[],int attrTypes[]){
	int rs=0,offset=0,length;
	char *tempTableName=new char();
	char *tempData=new char[42];
	char *tempDataAttr=new char[93];
	
	//upologizoume to record size gia kathe eggrafh
	for(int i=0;i<no_attr;i++) 
	{
		rs+=attrTypes[i];
		if(attrTypes[i]==0)
		rs+=4;
	}

	strcpy(tempTableName,TableName);
	strcat(tempTableName,".meta");
	rc=REM_rfm->CreateRecordFile(tempTableName,rs);
	if(rc!=OK)
	return rc;

	rc=REM_rfm->OpenRecordFile("rel.met",*REM_rfh);
	if(rc!=OK) 
	return rc;

	memcpy(tempData,TableName,64);
	memcpy(tempData+64,&rs,sizeof(int));
	memcpy(tempData+64+sizeof(int),&no_attr,sizeof(int));
	length=0;
	memcpy(tempData+64+2*sizeof(int),&length,sizeof(int));

	
	rc=REM_rfh->InsertRecord(tempData,*rid);
	if(rc!=OK) return rc;

	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;

	rc=REM_rfm->OpenRecordFile("attr.met",*REM_rfh);
	if(rc!=OK) return rc;

	for(int i=0;i<no_attr;i++)
	{
		memcpy(tempDataAttr,TableName,64);
		memcpy(tempDataAttr+64,attrName[i],64);
		memcpy(tempDataAttr+70,&offset,sizeof(int));
		if(attrTypes[i]==0)//0 --> exoume int
		//auto sumvainei giati o xrhsths vazei int h char(N)
		{
			length=4;
			memcpy(tempDataAttr+70+sizeof(int),"TYPE_INT",11);
			memcpy(tempDataAttr+81+sizeof(int),&length,sizeof(int));
		}
		else
		{
			length=attrTypes[i];
			memcpy(tempDataAttr+70+sizeof(int),"TYPE_STRING",11);
			memcpy(tempDataAttr+81+sizeof(int),&length,sizeof(int));
		}
		offset+=length;
		length=0;
		memcpy(tempDataAttr+81+2*sizeof(int),&length,sizeof(int));	
	}
	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;
	
	return OK;
}

t_rc SSQLM::DropTable(char *TableName){
	char *tempTableName=new char();

	strcpy(tempTableName,TableName);
	strcat(tempTableName,".meta");
	rc=REM_rfm->DestroyRecordFile(tempTableName);
	if(rc!=OK) return rc;

	rc=REM_rfm->OpenRecordFile("rel.met",*REM_rfh);
	if(rc!=OK) return rc;

	rc=scan->OpenRecordScan(*REM_rfh,TYPE_STRING,64,0,EQ_OP,TableName);
	if(rc!=OK) return rc;

	rc=scan->GetNextRecord(*REM_rh);
	if(rc==REM_SCANEOF) return SSQLM_TABLEDONTEXIST;
	else if(rc!=OK) return rc;

	rc=REM_rh->GetRecordID(*rid);
	if(rc!=OK) return rc;

	rc=REM_rfh->DeleteRecord(*rid);
	if(rc!=OK) return rc;

	rc=scan->CloseRecordScan();
	if(rc!=OK) return rc;

	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;

	rc=REM_rfm->OpenRecordFile("attr.met",*REM_rfh);
	if(rc!=OK) return rc;

	rc=scan->OpenRecordScan(*REM_rfh,TYPE_STRING,64,0,EQ_OP,TableName);
	if(rc!=OK) return rc;

	rc=scan->GetNextRecord(*REM_rh);

	while(rc!=REM_SCANEOF)
	{
		if(rc!=OK) return rc;

		rc=REM_rh->GetRecordID(*rid);
		if(rc!=OK) return rc;

		rc=REM_rfh->DeleteRecord(*rid);
		if(rc!=OK) return rc;

		rc=scan->GetNextRecord(*REM_rh);
	}

	rc=scan->CloseRecordScan();
	if(rc!=OK) return rc;

	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;

	return OK;
}

t_rc SSQLM::Insert(char *TableName,void *values[]){
	char* tempData=new char();
	char* insertData=new char();
	char* tempTableName=new char();
	int offset=0,length=0;
	rc=REM_rfm->OpenRecordFile("attr.met",*REM_rfh);
	if(rc!=OK) return rc;
	
	rc=scan->OpenRecordScan(*REM_rfh,TYPE_STRING,64,0,EQ_OP,TableName);
	if(rc!=OK) return rc;
	
	rc=scan->GetNextRecord(*REM_rh);

	int i=0;
	while(rc!=REM_SCANEOF){

		if(rc!=OK) return rc;

		rc=REM_rh->GetData(tempData);
		if(rc!=OK) return rc;

		memcpy(&offset,tempData+70,sizeof(int));
		memcpy(&length,tempData+81+sizeof(int),sizeof(int));

		memcpy(insertData+offset,values[i],length);

		rc=scan->GetNextRecord(*REM_rh);
		i++;
	}

	rc=scan->CloseRecordScan();
	if(rc!=OK) return rc;

	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;

	strcpy(tempTableName,TableName);
	strcat(tempTableName,".meta");
	rc=REM_rfm->OpenRecordFile(tempTableName,*REM_rfh);
	if(rc!=OK) return rc;

	rc=REM_rfh->InsertRecord(insertData,*rid);
	if(rc!=OK) return rc;

	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;
	
	return OK;
}

t_rc SSQLM::Delete(char* TableName)
{
	char* tempTableName=new char();

	strcpy(tempTableName,TableName);
	strcat(tempTableName,".meta");
	rc=REM_rfm->OpenRecordFile(tempTableName,*REM_rfh);
	if(rc!=OK) return rc;
	
	rc=scan->OpenRecordScan(*REM_rfh,TYPE_STRING,0,0,NO_OP,NULL);
	if(rc!=OK) return rc;
	
	rc=scan->GetNextRecord(*REM_rh);
	
	while(rc!=REM_SCANEOF)
	{
		if(rc!=OK) return rc;

		rc=REM_rh->GetRecordID(*rid);
		if(rc!=OK) return rc;

		rc=REM_rfh->DeleteRecord(*rid);
		if(rc!=OK) return rc;

		rc=scan->GetNextRecord(*REM_rh);
	}

	rc=scan->CloseRecordScan();
	if(rc!=OK) return rc;

	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;

	return OK;
}

t_rc SSQLM::Delete(char* TableName,char *attr1,t_compOp comp,void *attr2){
	char* tempData=new char();
	char* insertData=new char();
	char* tempTableName=new char();
	char* attrType = new char();
	int offset=0,length=0;
	//anoigoume to attr.met kai pairnoume to length k to offset ths sthlhs attr1 apo ton pinaka tablename
	rc=REM_rfm->OpenRecordFile("attr.met",*REM_rfh);
	if(rc!=OK) return rc;
	
	rc=scan->OpenRecordScan(*REM_rfh,TYPE_STRING,64,0,EQ_OP,TableName);
	if(rc!=OK) return rc;
	
	rc=scan->GetNextRecord(*REM_rh);
	if(rc!=OK) return rc;
	while(rc!=REM_SCANEOF){
		
		rc=REM_rh->GetData(tempData);
		if(rc!=OK) return rc;

		if(strcmp(tempData+64,attr1)==0){
			memcpy(attrType,tempData+70+sizeof(int),11);
			memcpy(&offset,tempData+70,sizeof(int));
			memcpy(&length,tempData+81+sizeof(int),sizeof(int));
			break;
		}
		rc=scan->GetNextRecord(*REM_rh);
	}


	rc=scan->CloseRecordScan();
	if(rc!=OK) return rc;
	
	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;
	
	//Scannaroume tis eggrafes, opoies ikanopoihhoun th syntihiki diagrafontai
	strcpy(tempTableName,TableName);
	strcat(tempTableName,".meta");
	rc=REM_rfm->OpenRecordFile(tempTableName,*REM_rfh);
	if(rc!=OK) return rc;
	
	if(strcmp(attrType,"TYPE_STRING")==0)
	rc=scan->OpenRecordScan(*REM_rfh, TYPE_STRING,length,offset,comp,&attr2);
	else
	rc=scan->OpenRecordScan(*REM_rfh, TYPE_INT,length,offset,comp,&attr2);
	if(rc!=OK) return rc;
	
	rc=scan->GetNextRecord(*REM_rh);
	while(rc!=REM_SCANEOF){
		if(rc!=OK) return rc;

		rc=REM_rh->GetRecordID(*rid);
		if(rc!=OK) return rc;

		rc=REM_rfh->DeleteRecord(*rid);
		if(rc!=OK) return rc;

		rc=scan->GetNextRecord(*REM_rh);
	}
	rc=scan->CloseRecordScan();
	if(rc!=OK) return rc;

	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;

	return OK;
}

t_rc SSQLM::Update(char* TableName,char* attrName,void* value,char *attr1,t_compOp comp,void *attr2){
	char* tempData=new char();
	char* updateData=new char();
	char* tempTableName=new char();
	char* wattrType = new char();
	char* setattrType=new char();
	int setoffset=0,setlength=0,woffset=0,wlength=0;
	REM_RecordHandle *temp_rh=new REM_RecordHandle();
	
	//anoigoume to attr.met kai pairnoume to length k to offset ths sthlhs attr1 apo ton pinaka tablename
	rc=REM_rfm->OpenRecordFile("attr.met",*REM_rfh);
	if(rc!=OK) return rc;
	
	rc=scan->OpenRecordScan(*REM_rfh,TYPE_STRING,64,0,EQ_OP,TableName);
	if(rc!=OK) return rc;
	
	rc=scan->GetNextRecord(*REM_rh);
	if(rc!=OK) return rc;
	while(rc!=REM_SCANEOF){
		
		rc=REM_rh->GetData(tempData);
		if(rc!=OK) return rc;

		if(strcmp(tempData+64,attr1)==0){
			memcpy(wattrType,tempData+70+sizeof(int),11);
			memcpy(&woffset,tempData+70,sizeof(int));
			memcpy(&wlength,tempData+81+sizeof(int),sizeof(int));
		}

		if(strcmp(tempData+64,attrName)==0){
			memcpy(setattrType,tempData+70+sizeof(int),11);
			memcpy(&setoffset,tempData+70,sizeof(int));
			memcpy(&setlength,tempData+81+sizeof(int),sizeof(int));
		}	
		rc=scan->GetNextRecord(*REM_rh);
	}


	rc=scan->CloseRecordScan();
	if(rc!=OK) return rc;
	
	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;
	
	//vriskoume tis eggrafes pou ikanopoioun th sun8hkh kai tis ananewnoume
	strcpy(tempTableName,TableName);
	strcat(tempTableName,".meta");
	rc=REM_rfm->OpenRecordFile(tempTableName,*REM_rfh);
	if(rc!=OK) return rc;
	

	if(strcmp(wattrType,"TYPE_STRING")==0)
	rc=scan->OpenRecordScan(*REM_rfh, TYPE_STRING,wlength,woffset,comp,&attr2);
	else
	rc=scan->OpenRecordScan(*REM_rfh, TYPE_INT,wlength,woffset,comp,&attr2);
	if(rc!=OK) return rc;
	
	rc=scan->GetNextRecord(*REM_rh);

	while(rc!=REM_SCANEOF){

		if(rc!=OK) return rc;

		rc=REM_rh->GetRecordID(*rid);
		if(rc!=OK) return rc;

		rc=REM_rh->GetData(updateData);
		if(rc!=OK) return rc;

		
		memcpy(updateData+setoffset,value,setlength);
		rc=REM_rfh->UpdateRecord(*REM_rh);  //  (???) o rh den dinei prosvasei gia allagh tou .metaa
		if(rc!=OK) return rc;

		rc=scan->GetNextRecord(*REM_rh);
	}
	rc=scan->CloseRecordScan();
	if(rc!=OK) return rc;

	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;

	return OK;
}

//TODO!!
t_rc SSQLM::Select(int noAttr,char *attrNames[],char *tableName,int noWheres,char *attr1Names[],t_compOp oper[],void *GivenValues[]){
	char *tempstring=new char();
	char *tempData=new char();
	tempTableName=new char[34];
	seloffsets=new int[noAttr];
	sellengths=new int[noAttr];
	selTypeString=new bool[noAttr];
	woffsets=new int[noWheres];
	wlengths=new int[noWheres];
	wTypeString=new bool[noWheres];
	int tempint;

	rc=REM_rfm->OpenRecordFile("attr.met",*REM_rfh);
	if(rc!=OK) return rc;

	rc=scan->OpenRecordScan(*REM_rfh,TYPE_STRING,64,0,EQ_OP,tableName);
	if(rc!=OK) return rc;
	
	rc=scan->GetNextRecord(*REM_rh);

	while(rc!=REM_SCANEOF)
	{	
		if(rc!=OK) return rc;
		rc=REM_rh->GetData(tempData);
		if(rc!=OK) return rc;

		for(int j=0;j<noAttr;j++)
		{
			if(strcmp(attrNames[j],tempData+64)==0)
			{
				memcpy(&seloffsets[j],tempData+70,sizeof(int));
				memcpy(&sellengths[j],tempData+81+sizeof(int),sizeof(int));
				memcpy(tempstring,tempData+70+sizeof(int),11);
				if(strcmp(tempstring,"TYPE_STRING")==0)
				selTypeString[j]=true;
				else
				selTypeString[j]=false;
				break;
			}
		}

		for(int j=0;j<noWheres;j++)
		{
			if(strcmp(attr1Names[j],tempData+64)==0)
			{
				memcpy(&woffsets[j],tempData+70,sizeof(int));
				memcpy(&wlengths[j],tempData+81+sizeof(int),sizeof(int));
				memcpy(tempstring,tempData+70+sizeof(int),11);
				if(strcmp(tempstring,"TYPE_STRING")==0)
				wTypeString[j]=true;
				else
				wTypeString[j]=false;
				break;
			}
		}

		rc=scan->GetNextRecord(*REM_rh);
	}

	rc=scan->CloseRecordScan();
	if(rc!=OK) return rc;

	rc=REM_rfm->CloseRecordFile(*REM_rfh);
	if(rc!=OK) return rc;


	no_attr=noAttr;
	no_wheres=noWheres;
	attr_names=attrNames;
	table_name=tableName;
	attr1_names=attr1Names;
	op=oper;
	values=GivenValues;
	*rid_checker=new REM_RecordID[no_wheres];
	scan_rfh=new REM_RecordFileHandle();
	maxRid=new REM_RecordID(0,0);

	for(int i=0;i<no_wheres;i++){
		scans[i]=new REM_RecordFileScan();
		rid_checker[i]=new REM_RecordID(0,0);
		if(wTypeString[i]==true)
		scans[i]->OpenRecordScan(*scan_rfh,TYPE_STRING,wlengths[i],woffsets[i],op[i],values[i]);
		else
		scans[i]->OpenRecordScan(*scan_rfh,TYPE_INT,wlengths[i],woffsets[i],op[i],values[i]);
	}

	strcpy(tempTableName,tableName);
	strcat(tempTableName,".meta");
	rc=REM_rfm->OpenRecordFile(tempTableName,*REM_rfh);
	if(rc!=OK) return rc;

	return OK;
}

SSQLM::~SSQLM(){
	//Destructor, tpt
}