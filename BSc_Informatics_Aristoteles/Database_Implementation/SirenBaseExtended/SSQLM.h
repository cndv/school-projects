#ifndef _SSQLM_h
#define _SSQLM_h

#include "REM.h"
#include "rcs.h"

class SSQLM{
private:
	STORM_StorageManager *STORM_sm;
	REM_RecordFileManager *REM_rfm;
	REM_RecordFileHandle *REM_rfh;
	REM_RecordHandle *REM_rh;
	REM_RecordID *rid;
	REM_RecordFileScan *scan;
	t_rc rc;
	
	//edw ksekinane oi metablites gia tin select
	int no_attr,no_wheres;
	char **attr_names,*table_name,**attr1_names;
	t_compOp *op;
	void **values;
	//ta arxikopoioume stin Select() //TODO //apo giorgo!
	char *tempTableName;
	int *seloffsets;
	int *sellengths;
	bool *selTypeString;
	int *woffsets;
	int *wlengths;
	bool *wTypeString;

	REM_RecordFileScan **scans;
	REM_RecordID **rid_checker;
	REM_RecordID *maxRid;
	REM_RecordFileHandle *scan_rfh;

public:
	SSQLM(STORM_StorageManager &sm);
	~SSQLM();
	t_rc CreateTable(char *TableName,int no_attr,char *attrName[],int attrTypes[]);
	t_rc DropTable(char *TableName);
	t_rc CreateIndex();
	t_rc DropIndex();
	t_rc Select(int noAttr,char *attrNames[],char *tableName,int noWheres,char *attr1Names[],t_compOp oper[],void *GivenValues[]);
	t_rc Insert(char *TableName,void *values[]);
	t_rc Delete(char* TableName);
	t_rc Delete(char* TableName,char *attr1,t_compOp comp,void *attr2);
	t_rc Update(char* TableName,char* attrName,void* value,char *attr1,t_compOp comp,void *attr2);
	
};
#endif