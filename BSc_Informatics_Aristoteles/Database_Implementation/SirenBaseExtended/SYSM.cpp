#include <stdlib.h>
#include <iostream>
#include <fstream>
//gcc -dM dummy.cpp --> vgazei ta macros pou einai currently defined
#if defined (__APPLE__) || defined (__linux__)
#include <sys/stat.h>
#include <sys/param.h> //gia to MAXPATHLEN
#include <unistd.h>
#else //Windows
#include <direct.h>
//#include <Windows.h> //xreiazetai?
#define MAXPATHLEN 512
#endif
#include "rcs.h"
#include "SYSM.h"

SYSM::SYSM(STORM_StorageManager &sm){
	CurrDB = new char[128];
	CurrDB = NULL;
	STORM_sm = &sm;
	REM_fm = new REM_RecordFileManager(*STORM_sm); //Ftiakse kai neo REM_RecordFileManager
	CurrDir = new char[MAXPATHLEN];
	getcwd(CurrDir,MAXPATHLEN)==NULL;
	strcat(CurrDir,"\\");
	strcat(CurrDir,"sirenbase\\data"); //To vasiko path ths efarmoghs
	if(chdir(CurrDir) != 0) //An den mporei na kanei change dir, den yparxei ara ftiaksto
	{
		mkdir("sirenbase\\data");
		chdir(CurrDir);
	}
	isOpened = true;
}

//Entolh: create database dbName
t_rc SYSM::CreateDatabase(char *dbName){
	//Apothikeyse to working directory
	getcwd(CurrDir,MAXPATHLEN);
	
	//Ftiakse to fakelo gia th vash
	if(mkdir(dbName)!=0) 
	return SYSM_CANNOTCREATEDBFOLDER;
	strcat(CurrDir,"\\");
	strcat(CurrDir,dbName);
	if(chdir(CurrDir)!=0) 
	return SYSM_CANNOTACCESSDBFOLDER;
	
	//OK, ftiaxnoume ta arxeia rel.met kai attr.met
	rc = REM_fm->CreateRecordFile("rel.met",42);
	if(rc != OK) return rc;
	rc = REM_fm->CreateRecordFile("attr.met",93);
	if(rc != OK) return rc;
	
	//Pame piso ston kentriko fakelo -- prepei na to kano h oxi telika???
	if(chdir("..") != 0) 
	return SYSM_CANNOTCHANGEDIR;
	
	return OK;
}

//Entolh: open database dbName
t_rc SYSM::OpenDatabase(char *dbName){
	if(isOpened)
	return SYSM_DBISOPENED;
	
	CurrDB=dbName;
	if(getcwd(CurrDir,MAXPATHLEN)==NULL) 
	return SYSM_CANNOTACCESSDBFOLDER;
	
	t_rc rc;
	//Anoikse to rel.met kai arrt.met
	rc = rfm.OpenRecordFile("rel.met", relFileHandle);
	if(rc != OK)
	return rc;
	rc = rfm.OpenRecordFile("attr.met", attrFileHandle);
	if(rc != OK)
	return rc;
	
	//OK, anoikse
	isOpened = TRUE;
	return OK;
}

//Entolh: close database dbName
t_rc SYSM::CloseDatabase(char *dbName){
	if(isOpened)
	return SYSM_DBALREADYCLOSED;
	CurrDB=NULL;
	
	t_rc rc;
	//Flush tis selides tou rel ston disko
	rc = relFileHandle.FlushPages();
	if(rc != OK)
	return rc;
	//Flush tis selides tou attr
	rc = attrFileHandle.FlushPages();
	if(rc != OK)
	return rc;

	//Kleise rel.met kai attr.met
	rc = rfm.CloseRecordFile(relFileHandle);
	if(rc != OK)
	return rc;
	rc = rfm.CloseRecordFile(attrFileHandle);
	if(rc != OK)
	return rc;
	
	isOpened = FALSE;
	return OK;
}

//Entolh: drop database dbName
t_rc SYSM::DropDatabase(char *dbName){
	
	if(isOpened && CurrDB!=dbName) 
	return SYSM_OTHERDBISOPENED;
	if(getcwd(CurrDir,MAXPATHLEN)==NULL) 
	return SYSM_CANNOTGETDIR;
	strcat(CurrDir,"\\");
	strcat(CurrDir,dbName);
	if(chdir(CurrDir)!=0) 
	return SYSM_CANNOTCHANGEDIR;
	
	REM_RecordFileHandle *REM_fh=new REM_RecordFileHandle();
	REM_RecordFileScan *scan=new REM_RecordFileScan();
	REM_RecordHandle *REM_rh=new REM_RecordHandle();
	
	rc=REM_fm->OpenRecordFile("rel.met",*REM_fh);
	if(rc!=OK) return rc;

	rc=REM_fm->DestroyRecordFile("rel.met");
	if(rc!=OK) return rc;
	rc=REM_fm->DestroyRecordFile("attr.met");
	if(rc!=OK) return rc;

	if(chdir("..")!=0) return SYSM_CANNOTCHANGEDIR; //pame piso ston kentriko fakelo
	
	#if defined (__APPLE__) || (__linux__)
	//delete, the unix way!
	char* delete_cmd[50];
	sprintf(delete_cmd, "rm -rf %s", dbName);
	if(system(delete_cmd) == -1)
	return SYSM_CANNOTDELETEDATABASE;
	#endif
	
	//Gia Windows: ??? -- prepei na einai prota adeio???
	//if(rmdir(dbName)!=0) return SYSM_CANNOTDELETEDATABASE;
	
	return OK;
}

SYSM::~SYSM(){} //Destructor, tipota

