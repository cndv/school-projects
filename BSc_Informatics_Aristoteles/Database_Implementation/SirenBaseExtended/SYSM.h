#ifndef _SYSM_h
#define _SYSM_h

#include "REM.h"
#include "SYSM_Parameters.h"

class SYSM
{
private:
	STORM_StorageManager *STORM_sm;
	
	REM_RecordFileManager *REM_fm;
	REM_RecordFileHandle relFileHandle;
	REM_RecordFileHandle attrFileHandle;
	
	char *CurrDB; //Anoikth database, kathe fora mono mia einai energh
	char *CurrDir; //Trexon directory
	bool isOpened;
	t_rc rc;
public:
	SYSM(STORM_StorageManager &STORM_sm);
	//Create: Ftiaxnei ta arxeia rel.met (gia tous pinakes) kai attr.met (gia ta attributes) kai tou fakelou gia kathe vash
	t_rc CreateDatabase(char *dbName);
	t_rc OpenDatabase(char *dbName);
	t_rc CloseDatabase(char *dbName);
	//DROP (SQL): Diagrafei olo to fakelo kai ta periexomena
	t_rc DropDatabase(char *dbName);
	~SYSM();
};
#endif