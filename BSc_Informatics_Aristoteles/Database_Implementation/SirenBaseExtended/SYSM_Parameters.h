#ifndef _SYSM_Parameters_h
#define _SYSM_Parameters_h

#define TABLE_NAME_SIZE (64*sizeof(char))
#define RECORD_SIZE (sizeof(int)) //in bytes
#define ATTRIBUTE_NAME_SIZE (64*sizeof(char))
#define TYPE_NAME_SIZE (11*sizeof(char)) //max("TYPE_STRING", "TYPE_INT") = 11

#endif