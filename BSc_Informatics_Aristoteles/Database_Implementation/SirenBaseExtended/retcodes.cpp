#include "retcodes.h"
#include <cstdio>

void DisplayReturnCode(t_rc rc)
{
	char *msg;

	switch(rc)
	{
	case OK: msg = "OK"; break;
	case UNKNOWN_ERROR: msg = "ERROR";break;
	case STORM_FILENOTFOUND: msg = "Specified file is not found."; break;
	case STORM_FILEEXISTS: msg = "File already exists."; break;
	case STORM_FILEDOESNOTEXIST: msg="File does not exist."; break;
	case STORM_FILEALREADYOPENED: msg = "File already opened."; break;
	case STORM_EOF: msg = "End Of File (EOF) has been reached."; break;
	case STORM_FILEFULL: msg = "File has reached its maximum capacity."; break;
	case STORM_FILEOPENERROR: msg = "File open error."; break;
	case STORM_FILECLOSEERROR: msg = "File close error."; break;
	case STORM_FILENOTOPENED: msg = "File is not opened."; break;
	case STORM_OPENEDFILELIMITREACHED: msg = "Limit of opened files reached."; break;
	case STORM_INVALIDFILEHANDLE: msg = "Invalid File Handle."; break;
	case STORM_INVALIDPAGE: msg = "Page is not valid."; break;
	case STORM_CANNOTCREATEFILE: msg = "Cannot create file."; break;
	case STORM_CANNOTDESTROYFILE: msg = "Cannot destroy file."; break;
	case STORM_PAGENOTINBUFFER: msg = "Page is not in buffer."; break;
	case STORM_PAGEISPINNED: msg = "Page is pinned."; break;
	case STORM_ALLPAGESPINNED: msg = "All pages are pinned."; break;
	case STORM_IOERROR: msg = "Input/Output error."; break;
	case STORM_MEMALLOCERROR: msg = "Memory allocation error."; break;
		
	//REM return messages for case()
	case REM_RECORDSIZETOOBIG: msg = "The record size is too big"; break;
	case REM_NO_REC_WITH_THIS_RID: msg = "Didn't find record with this RID"; break;
	case REM_NO_RECORD_FOUND: msg = "No record found"; break;
	case REM_PAGEID_OUT_OF_SCOPE: msg = "Page ID is out of scope."; break;	
	case REM_SCANEOF: msg = "Record scan: No more records"; break;
	//SYSM return messages for case()
	case SYSM_CANNOTCREATEDBFOLDER: msg = "Cannot create DB folder"; break;
	case SYSM_CANNOTACCESSDBFOLDER: msg = "Cannot access DB folder"; break;
	case SYSM_CANNOTCHANGEDIR: msg = "Cannot change directory"; break;
	case SYSM_CANNOTGETDIR: msg = "Cannot get current working dir"; break;
	case SYSM_DBISOPENED: msg = "Database is already opened"; break;
	case SYSM_OTHERDBISOPENED: msg = "Another database is already opened"; break;
	case SYSM_DBALREADYCLOSED: msg = "Database is already closed"; break;
	case SYSM_CANNOTDELETEDATABASE: msg = "Cannot delete database folder"; break;
	//SSQLM return messages for case()
	case SSQLM_TABLEDONTEXIST: msg = "Database table does not exist"; break;
		
	default: msg = "Unknown return code."; break;
		
	}

	fprintf (stderr, "ERROR: %s\n", msg);
}

void DisplayMessage(char *msg)
{
	fprintf (stderr, "ERROR: %s\n", msg); 
}
