Concept: A site dedicated to the Greeks that live abroad, helping them deal with public transactions and bureaucracy. 
Currently are implemented the experience application, and european health card application.

Technologies: HTML, CSS, jQuery/ jQueryUI, PHP/ MySQL, MVC Pattern with Codeigniter.
