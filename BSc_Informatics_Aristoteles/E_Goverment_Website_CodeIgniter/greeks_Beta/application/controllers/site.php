<?php
class Site extends CI_Controller//extends codeigniter's framework
{
	function __construct()
	{
		parent::__construct();
		$this->is_logged_in();
		$this->load->model('site_model');
	}

	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		if(!isset($is_logged_in) || $is_logged_in != true){
			redirect();		
		}	
	}	
	
	function index()
	{
		$data = array();
		if($query = $this->site_model->get_aitiseis())
		{
			$data['applications'] = $query;
		}
		$user_data = array();
		if ($query_user_data = $this->site_model->get_user_data())
		{
			$data['usr_data'] = $query_user_data;
		}
		$this->load->view('personal_page_copy', $data);
	}

	function create_application()
	{
		$eidos_aitisis = $this->uri->segment(3);
		if ($eidos_aitisis == 2){
			$this->site_model->create_health_card();
		}
		else{
			$this->site_model->create_cv();
		}
		redirect('site');
		

	}
	
	function foitites()
	{
		$this->load->view('view_foitites');
	}
	
	function view_aitisi_gia_karta_ygeias(){
		$final_data = array();
		$final_data['usr_data'] = $this->site_model->get_user_data();
		$final_data['health'] = $this->site_model->get_health_data();
		$this->load->view('view_ygeia', $final_data);
	}

	function view_aitisi_gia_proupiresia(){
		$final_data = array();
		$final_data['usr_data'] = $this->site_model->get_user_data();
		$final_data['ergasia'] = $this->site_model->get_ergasia_data();
		$this->load->view('view_ergasia', $final_data);
	}
	function view_aitisi_gia_karta_ygeias_final(){
		$final_data = array();
		$final_data['usr_data'] = $this->site_model->get_user_data();
		$final_data['health'] = $this->site_model->get_health_data();
		$this->load->view('view_ygeia_final', $final_data);
	}

	function view_aitisi_gia_proupiresia_final(){
		$final_data = array();
		$final_data['usr_data'] = $this->site_model->get_user_data();
		$final_data['ergasia'] = $this->site_model->get_ergasia_data();
		$this->load->view('view_ergasia_final', $final_data);
	}

	
	function aitisi_view()
	{
		$eidos_aitisis = $this->site_model->get_eidos_aitisis();
		
		
		$final_data = array();
		$final_data['usr_data'] = $this->site_model->get_user_data();
		if ($eidos_aitisis == 2){
			//fetch ygeia data
			$final_data['health'] = $this->site_model->get_health_data();
			$this->load->view('view_ygeia', $final_data);
		}
		else{
			//fetch ergasia data
			$final_data['ergasia'] = $this->site_model->get_ergasia_data();
			$this->load->view('view_ergasia', $final_data);
		}		
	}
	
	function delete()
	{
		$this->site_model->delete_row();
		redirect('site');
	}

	
}
