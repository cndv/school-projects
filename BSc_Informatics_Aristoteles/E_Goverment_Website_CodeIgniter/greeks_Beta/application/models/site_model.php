<?php
class Site_model extends CI_Model//extends codeigniters model
{
	function has_health_card(){
		$userName = $this->get_userName();
		$userId = $this->get_userId($userName);
		
		$this->db->select('*');
		$this->db->from('aitiseis');
		$this->db->where('user_id', $userId);
		$this->db->where('eidosAitisis_id', 2);
		$query = $this->db->get();
		return $query->result();
	}
	function has_cv(){
		$userName = $this->get_userName();
		$userId = $this->get_userId($userName);
		
		$this->db->select('*');
		$this->db->from('aitiseis');
		$this->db->where('user_id', $userId);
		$this->db->where('eidosAitisis_id', 1);
		$query = $this->db->get();
		return $query->result();
	}
	function create_health_card() {
		if (!$this->has_health_card()){		
			$data = array(
				'eidosAitisis_id' => 2,
				'user_id' => $this->get_userId($this->get_userName())
			);
			$this->db->insert('aitiseis',$data);
		}
		return;
	}
	//create curriculum vitae
	function create_cv() {
		if (!$this->has_cv()){
			$this->db->set('id',null);
			$this->db->set('eidosAitisis_id',1);
			$this->db->set('user_id',$this->get_userId($this->get_userName()));
			$this->db->insert('aitiseis');
		}
		return;
	}
	
	function get_health_data(){
		$userName = $this->get_userName();
		$userId = $this->get_userId($userName);
		
		$this->db->select('*');
		$this->db->from('healthCard');
		$this->db->where('user_id', $userId);
		$query = $this->db->get();			
		
		return $query->result();	
	}
	
	function get_ergasia_data(){
		$userName = $this->get_userName();
		$userId = $this->get_userId($userName);
		
		$this->db->select('*');
		$this->db->from('ergasia');
		$this->db->where('user_id', $userId);
		$query = $this->db->get();			
		
		return $query->result();	
	}
	
	function get_eidos_aitisis(){
		$this->db->where('id', $this->uri->segment(3));
		$query = $this->db->get('aitiseis');
		foreach ($query->result() as $row):
			if ($row->eidosAitisis_id == 2){
				return $e=2;
			}
			else{
				return $e=1;
			}
		endforeach;
	}
	
	function get_aitiseis(){
		$userName = $this->get_userName();
		$userId = $this->get_userId($userName);
		
		$this->db->select('*');
		$this->db->from('aitiseis');
		$this->db->where('user_id', $userId);
		$query = $this->db->get();			
		
		return $query->result();			
	}
	
	function get_userId($userName){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('username', $userName);
		$query = $this->db->get();
		foreach ($query->result() as $row):
			$user_id = $row->id;
		endforeach;
		return $user_id;
	}
	
	function get_user_data(){
		$userName = $this->get_userName();
		$userId = $this->get_userId($userName);
		
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('username', $userName);
		$query = $this->db->get();
		
		return $query->result();
	}
	
	function get_userName(){
		return $this->session->userdata('username');
	}

	function delete_row(){
		$this->db->where('id', $this->uri->segment(3));
		$this->db->delete('aitiseis');
	}
}
?>