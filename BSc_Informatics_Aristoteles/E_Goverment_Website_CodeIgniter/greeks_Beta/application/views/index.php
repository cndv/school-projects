<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js" type="text/javascript"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js" type="text/javascript"></script>
	<title>Greeks Living Abroad</title>
	<link rel="stylesheet" href="<?php echo base_url();?>css/style.css" type="text/css" media="screen" />
</head>
<body>
<!--header start -->
<div id="header">
	<a href="<?php echo base_url();?>"><img src="images/logo.jpg" alt="Greeks Living Abroad" width="137" height="50" border="0" class="logo" title="Greeks Living Abroad" /></a>
	<p class="topTxt"><span>Greeks Living Abroad</span>&nbsp;&nbsp; Ένα e-gov site για τους Έλληνες στο εξωτερικό. </p>
</div>
<!--header end -->
<!--body start -->
<div id="body">
	<p class="navLeft"></p>
	<ul class="nav">
		<li><a href="<?php echo base_url();?>">Αρχική</a></li>
		<li><a href="#">Σχετικά</a></li>
		<li><a href="#">Online Υπηρεσίες</a></li>
		<li><a href="#">Νέα</a></li>
		<li><a href="#">Βοήθεια</a></li>
		<li class="noImg">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<?php if($this->session->userdata('username')){
				echo "Καλώς ήλθες,";?>
				<li>"<?php echo anchor("site",$this->session->userdata('username'));?>"></li>
				<?php echo "!"; ?>			
		</li>
		
		<li>"<?php echo anchor('login/logout', 'Logout');?>"></li>
		<?php } else { ?>
				<li class="noImg">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Δεν έχετε συνδεθεί!
				</li>
		<?php } ?>
		
	</ul>
	<p class="navRight"></p>
	<!--left start -->
	<div id="left">
		<h2 class="welcome">Καλώς Ήρθατε!</h2>
		<p class="lftTxt"> Το site Greeks Living Abroad δημιουργήθηκε για να προσφέρει ηλεκτρονικές
		υπηρεσίες από το ελληνικό δημόσιο προς τους Έλληνες πολίτες, που είτε διαμένουν μόνιμα στο εξωτερικό
		είτε πρόκειται να λείψουν από τη χώρα για μικρό ή μεγάλο χρονικό διάστημα.
		<br /><br />
		<span>Διασύνδεση με δημόσιες υπηρεσίες</span>
		<br/>To site διασυνδέεται με πληθώρα δημόσιων υπηρεσιών όπως: Αστυνομία, Υπ.Συγκοινωνιών,
		 Ασφαλιστικά Ταμεία, Υπ.Παιδείας, Εφορία, Υπ.Υγείας, Ληξειαρχεία και Δημοτολόγια.  
		<br/>
		</p>
		<h2 class="le">Τελευταία Νέα</h2>
		<p class="lftTxt2">Εδώ θα βρείτε τα τελευταία νέα που αφορούν τις e-υπηρεσίες του site.</p>
		<p class="lftTxt3"><span>20.05.12</span> Προστέθηκε νέα e-υπηρεσία για την έκδοση της Ευρωπαϊκής Κάρτας
		Υγείας.</p>
		<a href="#" class="rm"></a>
		<p class="lftTxt3"><span>02.05.12</span> Προστέθηκε η υπηρεσία ηλεκτρονικής έκδοσης Πιστοποιητικού
		Προϋπηρεσίας από το ασφαλιστικό σας ταμείο.</p>
		<a href="#" class="rm"></a>
		<br class="spacer" />
	</div>
	<!--left end -->
	<!--right start -->
	
	<div id="right">
	<!--service start -->
		<div id="service">
			<h2>Κατηγορίες</h2>
			<h3>Επιλέξτε κατηγορία</h3>
			<img src="images/service_pic.gif" alt="" width="81" height="98" class="pic" />
			<ul>
				<li><a href="#">Πριν το ταξίδι</a></li>
				<li><?php echo anchor("public_site/ergazomenoi",'Εργασία'); ?></li>
				<li><a href="#">Υγεία</a></li>
				<li><a href="#">Παιδεία &amp; Νεολαία</a></li>
				<li><a href="#">Εισόδημα &amp; Φόροι</a></li>
				<li><a href="#">e-Ληξιαρχείο &amp; Δικαιωμάτα</a></li>
				<!--<li><a href="#" class="rm2"></a></li> -->
			</ul>
			<p class="serBot"></p>
			<br class="spacer" />
		</div>
		<!--service end -->
		<!--question start -->
		<div id="question">
			<h2>Πολίτες</h2>
			<h3>Επιλέξτε κατηγορία</h3>
			<img src="images/question_pic.gif" alt="" width="79" height="83" class="pic2" />
			<ul>
				<li><a href="#">Ταξιδιώτες </a></li>
				<li><?php echo anchor("public_site/foitites",'Φοιτητές'); ?></li>
				<li><?php echo anchor("public_site/ergazomenoi",'Εργαζόμενοι'); ?></li>
				<li><a href="#">Συνταξιούχοι</a></li>
				<li><a href="#">Γονείς (οικογένεια &amp; παιδιά)</a></li>
				<li><a href="#">Α.Μ.Ε.Α. &amp; Συνοδοί</a></li>
				<!--<li><a href="#" class="rm3"></a></li> -->
			</ul>
			<p class="questionBot"></p>
			<br class="spacer" />
		</div>
		<!--question end -->
		<!--mem start -->
		<div id="mem" style="height: 160px">
			<h2>Σύνδεση Μελών</h2>
		    <?php 
				echo form_open('login/validate_credentials'); // this is where it will go
				echo "<label>Username</label>";
				echo form_input('username', 'Όνομα χρήστη ');
				echo "<label>Password</label>";
				echo form_password('password', 'Password');
				echo form_submit('submit', 'Login');
				echo form_close();
				?>
			<br class="spacer" />
			<br class="spacer" />
		</div>
	
		<div id="mem2">
			<h2>Σύνδεση Μελών</h2>
		    <?php 
				echo form_open('login/validate_credentials'); // this is where it will go
				echo "<label>Username</label>";
				echo form_input('username', 'Όνομα χρήστη ');
				echo "<label>Password</label>";
				echo form_password('password', 'Password');
				echo form_submit('submit', 'Login');
				echo form_close();
			?>
		</div>	
			<?php if ($this->session->userdata('username')) { ?>	
				<script language="javascript" type="text/javascript">					
					$("#mem").hide();
				</script>			
			<?php }else {?>	
				<script language="javascript" type="text/javascript">					
					$("#mem").show();
					$("#mem2").hide();
				</script>			
			<?php }?>
		<!--mem end -->
		
					
			<script type="text/javascript">
			       $(document).ready(function(){
			           $('#login_ygeia').click(function(){
			               $('#mem').dialog({open:function() { $("#mem2").hide(); },
			               					close:function(){ $("#mem2").show(); },
			               					height: 200,
			               					width: 300,
			               					show: "bind",
			               					hide: "explode"
			               					});
			           });
			           $("#mem2").hide();
			       });
			</script>
			<!--direction start -->
			<div id="direction">
			<h2>e-Υπηρεσίες</h2>
			<h3>Επιλέξτε ηλεκτρονική υπηρεσία</h3>
			<img src="images/transparent.gif" alt="" width="15" height="83" class="pic3" />
			<ul>
				<?php if ($this->session->userdata('username')) { ?>	
				<li> href="<?php echo anchor('site/view_aitisi_gia_karta_ygeias', 'Ευρωπαϊκή Κάρτα Υγείας');?>"></li>
				<li>"<?php echo anchor('site/view_aitisi_gia_proupiresia', 'Πιστοποιητικό Προϋπηρεσίας');?>"></li>
				<?php }else{ ?>
				<div id="login_ygeia">
					<li><a href="#">Ευρωπαϊκή Κάρτα Υγείας</a></li>
					<li><a href="#">Πιστοποιητικό Προϋπηρεσίας</a></li>
				</div>
				<?php } ?>
				<li><a href="#">Πιστοποιητικά Ληξειαρχείου</a></li>
				<li><a href="#">Πιστοποιητικά Εκπαίδευσης</a></li>
				<li><a href="#">Έκδοση Διαβατηρίου/Βίζας</a></li>
				<li><a href="#" class="rm4"></a></li>
			</ul>
			<br class="spacer" />
		</div>
		<!--direction end -->
		<br class="spacer" />
		<h3 class="rightBot">&nbsp; &nbsp; </h3>
		<p class="rightTxt">Για οποιοδήποτε πρόβλημα αντιμετωπίζετε με τη σύνδεση σας ή με κάποια από
		 τις ηλεκτρονικές υπηρεσίες απευθυνθείτε στην εξής διέυθυνση ηλ.ταχυδρομείου
		<a href="#">help
		@greekslivingabroad.gov.gr</a></p>
		<br class="spacer" />
	</div>
	<!--right end -->
<br class="spacer" />
</div>
<!--body end -->
<!--footer start -->
<div id="footer"> 
  <p class="copyright">Copyright © G.L.A 2012. All Rights Reserved.</p>
  <p class="design">Designed by : <a href="http://www.templateworld.com/" target="_blank" class="link">
  Csd&amp;Us</a></p>
  </div>
<!--footer end -->
</body>
</html>
