<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script>
	<title>Φοιτητές</title>
<link rel="stylesheet" href="<?php echo base_url();?>css/innerstyle.css" type="text/css" media="screen" /></head>
<body>
<!--header start -->
<div id="header">
<a href="<?php echo base_url();?>"><img src="../../images/logo.jpg" alt="Greeks Living Abroad" width="137" height="50" border="0" class="logo" title="Greeks Living Abroad" /></a>
<p class="topTxt"><span>Greeks Living Abroad</span>&nbsp;&nbsp; Ένα e-gov site 
για τους Έλληνες στο εξωτερικό. </p>
</div>
<!--header end -->
<!--body start -->
<div id="body">
	<p class="navLeft"></p>
		<ul class="nav">
				<li><a href="<?php echo base_url();?>">Αρχική</a></li>
				<li><a href="#">Σχετικά</a></li>
				<li><a href="#">Online Υπηρεσίες</a></li>
				<li><a href="#">Νέα</a></li>
				<li><a href="#">Βοήθεια</a></li>
				<li class="noImg"><a href="#">Επικοινωνία</a></li>
				<li class="noImg">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


				<?php if($this->session->userdata('username')){
						echo "Καλώς ήλθες,";?>
						<li>"<?php echo anchor("site",$this->session->userdata('username'));?>"></li>
						<?php echo "!"; ?>			
				</li>
				
				<li>"<?php echo anchor('login/logout', 'Logout');?>"></li>
				<?php } else { ?>
						<li class="noImg">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;
							Δεν έχετε συνδεθεί!
						</li>
				<?php } ?>
	
						
			</ul>		
		
		
	<p class="navRight"></p>
	<!--left start -->
		<div id="left">
		<h2 class="welcome">Φοιτητές</h2>
		<hr/>
		</br>
		<h2 class="le">Πριν το ταξίδι</h2>
		<p class="lftTxt">Ενημερωθείτε για όλα όσα πρέπει να γνωρίζετε πριν ταξιδέψετε στη χώρα προορισμού σας.
		 <a class="rm" href="#">Περισσότερα..</a> </p>
			<br/>
		<p class="lftTxt"> <span>Online Υπηρεσίες:</span></p>
			<ul>
			   <li><a href="#">Ηλεκτρονική Αίτηση Έκδοσης Διαβατηρίου </a></li>
			   <li><a href="#">Ηλεκτρονική Αίτηση Έκδοσης Βίζας </a></li>
			</ul>
			<br/><br/>
		</br>
		</br>		
		<h2 class="le">Υγεία στο εξωτερικό</h2>
		<p class="lftTxt">Διαβάστε για τις ισχύουσες διατάξεις σχετικά με την υγειονομική κάλυψη των Ελλήνων φοιτητών
		στις χώρες της Ευρωπαϊκής Ένωσης και αλλού. Μάθετε τι προσφέρει η Ευρωπαϊκή Κάρτα Υγείας και ποιες περιπτώσεις
		 περίθαλψης καλύπτει.  
		<a class="rm" href="#">Περισσότερα..</a> </p>
		<br/>
		<p class="lftTxt">
		 <span>Online Υπηρεσίες:</span>
		</p>
		<ul>
			<li>"<?php echo anchor('site/view_aitisi_gia_karta_ygeias', 'Έκδοση Ηλεκτρονικής Κάρτας Υγείας');?>"></li>
		</ul>
		<br/><br/></br>
		<h2 class="le">Εκπαίδευση στο εξωτερικό</h2>
		<p class="lftTxt">Εδώ θα βρείτε πληροφορίες και χρήσιμους συνδέσμους σχετικά με προπτυχιακές και μεταπτυχιακές σπουδές
		στο εξωτερικό σε διάφορους κλάδους.
		<a class="rm" href="#">Περισσότερα..</a>  </p>
		<br/>
		<p class="lftTxt">
		 <span>Online Υπηρεσίες:</span>
		</p>
		<ul>
		   <li><a href="#">Ηλεκτρονική Έκδοση Πιστοποιητικών Εκπαίδευσης</a></li>
		   <li><a href="#">Αναζήτηση υποτροφιών για φοίτηση στο εξωτερικό</a></li>
		</ul>
		
		<br class="spacer" />
		</div>
		<!--left end -->
	<!--right start -->
	<div id="right">
	<!--service start -->
	<div id="service">
	<h2>Κατηγορίες</h2>
	<h3>Επιλέξτε κατηγορία</h3>
	<img src="../../images/service_pic.gif" alt="" width="81" height="98" class="pic" />
	<ul>
		<li><a href="#">Πριν το ταξίδι</a></li>
		<li><?php echo anchor("public_site/ergazomenoi",'Εργασία'); ?></li>
		<li><a href="#">Υγεία</a></li>
		<li><a href="#">Παιδεία &amp; Νεολαία</a></li>
		<li><a href="#">Εισόδημα &amp; Φόροι</a></li>
		<li><a href="#">e-Ληξιαρχείο &amp; Δικαιωμάτα</a></li>
	<!--<li><a href="#" class="rm2"></a></li> -->
	</ul>
	<p class="serBot"></p>
	<br class="spacer" />
	</div>
	<!--service end -->
	<!--question start -->
	<div id="question">
	<h2>Πολίτες</h2>
	<h3>Επιλέξτε κατηγορία</h3>
	<img src="../../images/question_pic.gif" alt="" width="79" height="83" class="pic2" />
	<ul>
		<li><a href="#">Ταξιδιώτες </a></li>
		<li><?php echo anchor("public_site/foitites",'Φοιτητές'); ?></li>
		<li><?php echo anchor("public_site/ergazomenoi",'Εργαζόμενοι'); ?></li>
		<li><a href="#">Συνταξιούχοι</a></li>
		<li><a href="#">Γονείς (οικογένεια &amp; παιδιά)</a></li>
		<li><a href="#">Α.Μ.Ε.Α. &amp; Συνοδοί</a></li>
	<!--<li><a href="#" class="rm3"></a></li> -->
	</ul>
	<p class="questionBot"></p>
	<br class="spacer" />
	</div>
	<!--question end -->
	<!--mem start -->
	<div id="mem" style="height: 160px">
			<h2>Σύνδεση Μελών</h2>
		    <?php 
			echo form_open('login/validate_credentials'); // this is where it will go
			echo "<label>Username</label>";
			echo form_input('username', 'Όνομα χρήστη ');
			echo "<label>Password</label>";
			echo form_password('password', 'Password');
			echo form_submit('submit', 'Login');
			echo form_close();
			?>
			<br class="spacer" />
			<br class="spacer" />
		</div>

		<?php if ($this->session->userdata('username')) { ?>
				
				<script language="javascript" type="text/javascript">					
					$("#mem").hide();
				</script>			
			
		<?php }else {?>
				<script language="javascript" type="text/javascript">					
					$("#mem").show();
				</script>			
		<?php }?>
	<!--mem end -->
	<!--direction start -->
	<div id="direction">
	<h2>e-Υπηρεσίες</h2>
	<h3>Επιλέξτε ηλεκτρονική υπηρεσία</h3>
	<img src="../../images/transparent.gif" alt="" width="15" height="83" class="pic3" />
	<ul>
		<li>"<?php echo anchor('site/view_aitisi_gia_karta_ygeias', 'Ευρωπαϊκή Κάρτα Υγείας');?>"></li>
		<li>"<?php echo anchor('site/view_aitisi_gia_proupiresia', 'Πιστοποιητικό Προϋπηρεσίας');?>"></li>
		<li><a href="#">Πιστοποιητικά Ληξειαρχείου</a></li>
		<li><a href="#">Πιστοποιητικά Εκπαίδευσης</a></li>
		<li><a href="#">Έκδοση Διαβατηρίου/Βίζας</a></li>
		<li><a href="#" class="rm4"></a></li>
	</ul>
	<br class="spacer" />
	</div>
	<!--direction end -->
	<h3 class="rightBot"><br/></h3>
	<br class="spacer" />
	</div>
	<!--right end -->
	<br class="spacer" />
</div>
<!--body end -->
<!--footer start -->
<div id="footer"> 
  <p class="copyright">Copyright © Circle 20XX. All Rights Reserved.</p>
  <p class="design">Designed by : <a href="http://www.templateworld.com/" target="_blank" class="link">
  Template World</a></p>
 </div>
<!--footer end -->
</body>
</html>
