<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ευρωπαϊκή Κάρτα Υγείας</title>
<link rel="stylesheet" href="<?php echo base_url();?>css/innerstyle.css" type="text/css" media="screen" /></head>
<body>
<!--header start -->
<div id="header">
<a href="<?php echo base_url();?>"><img src="../../images/logo.jpg" alt="Greeks Living Abroad" width="137" height="50" border="0" class="logo" title="Greeks Living Abroad" /></a>
<p class="topTxt"><span>Greeks Living Abroad</span>&nbsp;&nbsp; Ένα e-gov site 
για τους Έλληνες στο εξωτερικό. </p>
</div>
<!--header end -->
<!--body start -->
<div id="body">
<p class="navLeft"></p>
<ul class="nav">
		<li><a href="<?php echo base_url();?>">Αρχική</a></li>
		<li><a href="#">Σχετικά</a></li>
		<li><a href="#">Online Υπηρεσίες</a></li>
		<li><a href="#">Νέα</a></li>
		<li><a href="#">Βοήθεια</a></li>
		<li class="noImg">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<?php if($this->session->userdata('username')){
				echo "Καλώς ήλθες,";?>
				<li>"<?php echo anchor("site",$this->session->userdata('username'));?>"></li>
				<?php echo "!"; ?>			
		</li>
		
		<li>"<?php echo anchor('login/logout', 'Logout');?>"></li>
		<?php } else { ?>
				<li class="noImg">
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Δεν έχετε συνδεθεί!
				</li>
		<?php } ?>
		
	</ul>
<p class="navRight"></p>
<!--left start -->
	<div id="left">
		<div id="lftTxtPersonalData">
			<h2 class="card">Ευρωπαϊκή Κάρτα Υγείας</h2>
			</br>
			<?php if(isset($usr_data)) : foreach($usr_data as $u_d) : ?>
				<p class = "lftTxt2">
				<?php 
					echo "Όνομα : ";					
					echo $u_d->name;
									echo " <br>";
					echo "Επίθετο : ";
					echo $u_d->surname;
									echo " <br>";
					echo "email : ";
					echo $u_d->email;
									echo " <br>";
					echo "Όνομα Πατρός : ";
					echo $u_d->fatherName;
									echo " <br>";
					echo "Hμε/νία γέννησης : ";
					echo $u_d->birthdate;
									echo " <br>";
					echo "ΑΜΚΑ : ";
					echo $u_d->amka;
									echo " <br>";
					echo "Αρ. Ταυτότητας : ";
					echo $u_d->arithmos_tautotitas;	
					endforeach;
					endif;
					?>
				</p>	
				
				<?php if(isset($health)) : foreach($health as $h) : ?>
				<p class = "lftTxt2">
					<?php 
							echo "Φορέας : ";
							echo $h->foreas;
							echo " <br>";
							echo "Ημερ/νία λήξης : ";
							echo $h->expirationDate;
							echo " <br>";	
							echo "Αρ. Κάρτας : ";
							echo $h->card_num;	
						endforeach;
						else :
					?>
				</p>	
				<h2 class = "le">Δεν ανήκετε σε κάποιο φορέα!</h2>
				<?php endif; ?>	
		</div>
	</div>
<!--left end -->
<!--right start -->
	<div id="right">
	<!--service start -->
		<div id="service">
			<h2>Κατηγορίες</h2>
			<h3>Επιλέξτε κατηγορία</h3>
			<img src="../../images/service_pic.gif" alt="" width="81" height="98" class="pic" />
			<ul>
				<li><a href="#">Πριν το ταξίδι</a></li>
				<li><?php echo anchor("public_site/ergazomenoi",'Εργασία'); ?></li>
				<li><a href="#">Υγεία</a></li>
				<li><a href="#">Παιδεία &amp; Νεολαία</a></li>
				<li><a href="#">Εισόδημα &amp; Φόροι</a></li>
				<li><a href="#">e-Ληξιαρχείο &amp; Δικαιωμάτα</a></li>
			</ul>
			<p class="serBot"></p>
			<br class="spacer" />
		</div>
		<!--service end -->
		<!--question start -->
		<div id="question">
			<h2>Πολίτες</h2>
			<h3>Επιλέξτε κατηγορία</h3>
			<img src="../../images/question_pic.gif" alt="" width="79" height="83" class="pic2" />
			<ul>
				<li><a href="#">Ταξιδιώτες </a></li>
				<li><?php echo anchor("public_site/foitites",'Φοιτητές'); ?></li>
				<li><?php echo anchor("public_site/ergazomenoi",'Εργαζόμενοι'); ?></li>
				<li><a href="#">Συνταξιούχοι</a></li>
				<li><a href="#">Γονείς (οικογένεια &amp; παιδιά)</a></li>
				<li><a href="#">Α.Μ.Ε.Α. &amp; Συνοδοί</a></li>
			</ul>
			<p class="questionBot"></p>
			<br class="spacer" />
		</div>
		<!--question end -->
		
		<!--direction start -->
		<div id="direction">
			<h2>e-Υπηρεσίες</h2>
			<h3>Επιλέξτε ηλεκτρονική υπηρεσία</h3>
			<img src="../../images/transparent.gif" alt="" width="15" height="83" class="pic3" />
			<ul>
				<li>"<?php echo anchor('site/view_aitisi_gia_karta_ygeias', 'Ευρωπαϊκή Κάρτα Υγείας');?>"></li>
				<li>"<?php echo anchor('site/view_aitisi_gia_proupiresia', 'Πιστοποιητικό Προϋπηρεσίας');?>"></li>
				<li><a href="#">Πιστοποιητικά Ληξειαρχείου</a></li>
				<li><a href="#">Πιστοποιητικά Εκπαίδευσης</a></li>
				<li><a href="#">Έκδοση Διαβατηρίου/Βίζας</a></li>
			</ul>
			<br class="spacer" />
		</div>
		<!--direction end -->
		<h3 class="rightBot"><br/></h3>
		<br class="spacer" />
	</div>
	<!--right end -->
<br class="spacer" />
</div>
<!--body end -->
<!--footer start -->
<div id="footer"> 
  <p class="copyright">Copyright © Circle 20XX. All Rights Reserved.</p>
  <p class="design">Designed by : <a href="http://www.templateworld.com/" target="_blank" class="link">
  Template World</a></p>
  </div>
<!--footer end -->
</body>
</html>
