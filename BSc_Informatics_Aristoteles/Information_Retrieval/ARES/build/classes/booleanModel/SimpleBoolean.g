grammar SimpleBoolean;

options {
  language = Java;
    output = AST;
}
@header {
  package com.booly;
}

rule: ;

LPAREN : '(' ;
RPAREN : ')' ;
AND : 'AND';
OR : 'OR';
NOT: 'NOT';
WS :  ( ' ' | '\t' | '\r' | '\n') {$channel=HIDDEN;}  ;  
WORD :  (~( ' ' | '\t' | '\r' | '\n' | '(' | ')' ))*;  


expr : orexpr;

orexpr : andexpr (OR^ andexpr)*;
andexpr: notexpr (AND^ notexpr)*;
notexpr :  NOT* atom;

atom : WORD | (LPAREN! expr RPAREN!);