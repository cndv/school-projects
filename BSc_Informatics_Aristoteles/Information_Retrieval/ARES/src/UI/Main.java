/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package UI;

import booleanModel.BooleanModel;
import invertedIndex.IndexManager;
import vectorModel.VectorModel;
import collectionManager.CollectionManager;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import vectorModel.IDF_Enum;
import vectorModel.SimilarityFunction_Enum;
import vectorModel.TF_Enum;

/**
 *
 * @author root
 */
public class Main {

    public static void main(String[] args) throws Throwable {
        //Arxikopoihsh olon ton apaitoumenon modules
        IndexManager imgr = new IndexManager();
        VectorModel vm = new VectorModel(imgr);
        BooleanModel bm = new BooleanModel(imgr);
        CollectionManager cm = new CollectionManager();
        Menu menu = new Menu();

        Scanner scan = new Scanner(System.in);
        //h epanaxrhsimopoihsh tou scan exei provlima se nested switches
        //(an parapano exei diavasei kati allo) ara ftiaxnoume ena akoma:
        Scanner insideScan = new Scanner(System.in);
        String userInput;
        int choice;
        while (true) {
            menu.showMainMenu();
            System.out.print(">> ");
            choice = scan.nextInt();
            switch (choice) {
                case 1: //Dimiourgia sylloghs
                    System.out.println("Dwse to onoma tis sillogis (px \"collection1\"):");
                    System.out.print(">> ");
//                    userInput = scan.nextLine();
                    userInput = scan.next();
                    imgr.createEmptyCollection(userInput);
                    break;
                case 2: //Anoigma sylloghs
                    System.out.println("Dwse to onoma tis sillogis (px \"collection1\"):");
                    System.out.print(">> ");
//                    userInput = scan.nextLine();
                    userInput = scan.next();
                    imgr.openCollection(userInput);
                    menu.openCollectionMenu();
                    System.out.print(">> ");
                    choice = scan.nextInt();
                    switch (choice) {
                        case 1: //Eisagogh arxeiou/arxeion
                            System.out.println("Dwse to path tou arxeiou i tou katalogou arxeiwn (px C:\\data):");
                            System.out.print(">> ");
//                            userInput = scan.nextLine();
                            userInput = scan.next();
                            File file = new File(userInput);
                            imgr.insert(file);
                            break;
                        case 2: //Diagrafh arxeiou/arxeion
                            System.out.println("Dwse ton titlo tou eggrafou (px \"doc1\"):");
                            System.out.print(">> ");
//                            userInput = scan.nextLine();
                            userInput = scan.next();
                            imgr.delete(userInput);
                            //FIXME diagrafh pollon eggrafon
                            break;
                        case 3: //Ektelesh erothmatos
                            menu.queryMenu();
                            System.out.print(">> ");
                            choice = scan.nextInt();
                            switch (choice) {
                                case 1: //Boolean query
                                    System.out.println("Dwse to erwtima me kefailaious telestes (px \"term1 AND term2 AND (term3 OR NOT term4)\":");
                                    System.out.print(">> ");
//                                    userInput = insideScan.nextLine();
                                    userInput = insideScan.next();
                                    System.out.println(bm.query(userInput));
                                    break;
                                case 2: //Vector query
                                    System.out.println("Dwse to erwtima (px term1 term2):");
                                    System.out.print(">> ");
                                    userInput = insideScan.nextLine();
                                    Scanner tokenizer = new Scanner(userInput);
                                    ArrayList<String> terms = new ArrayList<String>();
                                    while (tokenizer.hasNext()) {
                                        terms.add(tokenizer.next());
                                    }
                                    //System.out.println(terms);
                                    menu.vectorMenu();
                                    System.out.print(">> ");
                                    choice = scan.nextInt();
                                    switch (choice) {
                                        case 1:
                                            menu.showTfMenu();
                                            System.out.print(">> ");
                                            int tf = insideScan.nextInt();
                                            switch (tf) {
                                                case 1:
                                                    vm.setTf(TF_Enum.SIMPLE);
                                                    break;
                                                case 2:
                                                    vm.setTf(TF_Enum.LOGARITHMIC);
                                                    break;
                                                case 3:
                                                    vm.setTf(TF_Enum.BINARY);
                                                    break;
                                                case 4: //Epistrofh
                                                    break;
                                                default:
                                                    vm.setTf(TF_Enum.BINARY);
                                                    break;
                                            }
                                        case 2:
                                            menu.showIdfMenu();
                                            System.out.print(">> ");
                                            int idf = insideScan.nextInt();
                                            switch (idf) {
                                                case 1:
                                                    vm.setIdf(IDF_Enum.LOGARITHMIC_1);
                                                    break;
                                                case 2:
                                                    vm.setIdf(IDF_Enum.LOGARITHMIC_2);
                                                    break;
                                                case 3:
                                                    vm.setIdf(IDF_Enum.LOGARITHMIC_3);
                                                    break;
                                                case 4:
                                                    vm.setIdf(IDF_Enum.BINARY);
                                                    break;
                                                case 5: //Epistrofh
                                                    break;
                                                default:
                                                    vm.setIdf(IDF_Enum.BINARY);
                                                    break;
                                            }
                                        case 3:
                                            menu.showSimilarityMenu();
                                            System.out.print(">> ");
                                            int sim = insideScan.nextInt();
                                            switch (sim) {
                                                case 1:
                                                    vm.setSim(SimilarityFunction_Enum.COSINE);
                                                    break;
                                                case 2:
                                                    vm.setSim(SimilarityFunction_Enum.DICE);
                                                    break;
                                                case 3:
                                                    vm.setSim(SimilarityFunction_Enum.JACCARD);
                                                    break;
                                                case 4:
                                                    vm.setSim(SimilarityFunction_Enum.OVERLAPPING);
                                                    break;
                                                case 5:
                                                    vm.setSim(SimilarityFunction_Enum.INNER_PRODUCT);
                                                    break;
                                                case 6: //Epistrofh
                                                    break;
                                                default:
                                                    vm.setSim(SimilarityFunction_Enum.INNER_PRODUCT);
                                                    break;
                                            }
                                        case 4:
                                            System.out.println("Dwse tin timi tou katwfliou omoiotitas (px 0,3): ");
                                            System.out.print(">> ");
                                            double threshold = insideScan.nextDouble();
                                            vm.setThreshold(threshold);
                                            vm.setTop_k(Boolean.FALSE);
                                            break;
                                        case 5:
                                            System.out.println("Dwse tin timi twn K kaliterwn apotelesmatwn (px 20)");
                                            System.out.print(">> ");
                                            int k = insideScan.nextInt();
                                            vm.setK(k);
                                            vm.setTop_k(Boolean.TRUE);
                                            break;
                                        case 6: //Synexeia - use defaults
                                            break;
                                        default:
                                            break;
                                    }
                                    System.out.println(vm.runQuery(terms));
                                    break;
                                case 3: //Epistrofh
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case 4: //Epistrofh
                            break;
                        default:
                            break;
                    }
                    break;
                case 3: //Kleisimo sylloghs
                    imgr.closeCollection();
                    break;
                case 4: //Dimiourgia sylloghs anaforas
                    System.out.println("Dwse to path tou katalogou pou vrisketai to arxeio med_docs.txt (px \"C:\\docs\"):");
                    System.out.print(">> ");
//                    userInput = scan.nextLine();
                    userInput = scan.next();
                    cm.splitMED(userInput);
                    System.out.println("Dimiourgithikan ta ksexwrista arxeia ston katalogo: " + userInput + "\\MED_DOCS");
                    System.out.println("Dimiourgia tis sillogis eggrafwn MED... (mporei na parei ligo xrono)");
                    imgr.createEmptyCollection("MEDcollection");
                    imgr.openCollection("MEDcollection");
                    File MEDdocs = new File(userInput + "\\MED_DOCS\\");
                    imgr.insert(MEDdocs);
                    break;
                case 5: //Anoigma sylloghs anaforas
                    imgr.openCollection("MEDcollection");
                    menu.openRefColletionMenu();
                    System.out.print(">> ");
                    choice = scan.nextInt();
                    switch (choice) {
                        case 1: //Ektelesh erothmaton anaforas
                            System.out.println("Dwse to path tou arxeiou med_queries.txt (px C:\\Docs\\med_queries.txt):");
                            System.out.print(">> ");           
                            userInput = scan.next();
//                            userInput = scan.next();
                            HashMap<String, String> MEDqueries = new HashMap<String, String>();
                            menu.stopWordsMenu();
                            System.out.print(">> ");
                            choice = scan.nextInt();
                            switch (choice) {
                                case 1:
                                    cm.proccessQueries(userInput, MEDqueries, Boolean.TRUE);
                                    break;
                                case 2:
                                    cm.proccessQueries(userInput, MEDqueries, Boolean.FALSE);
                                    break;
                                case 3: //Epistrofh: oxi break, pame sto default
                                default:
                                    cm.proccessQueries(userInput, MEDqueries, Boolean.TRUE);
                                    break;
                            }
                            System.out.println("Dwse to path tou arxeiou med_relevant.txt (px C:\\Docs\\med_relevant.txt):");
                            System.out.print(">> ");
//                            userInput = scan.nextLine();
                            userInput = scan.next();
                            
                            HashMap<String, ArrayList<String>> MEDrelevant = new HashMap<String, ArrayList<String>>();
                            cm.proccessRelevant(userInput, MEDrelevant);
                            menu.vectorMenu();
                            System.out.print(">> ");
                            choice = scan.nextInt();
                            switch (choice) {
                                case 1:
                                    menu.showTfMenu();
                                    System.out.print(">> ");
                                    int tf = insideScan.nextInt();
                                    switch (tf) {
                                        case 1:
                                            vm.setTf(TF_Enum.SIMPLE);
                                            break;
                                        case 2:
                                            vm.setTf(TF_Enum.LOGARITHMIC);
                                            break;
                                        case 3:
                                            vm.setTf(TF_Enum.BINARY);
                                            break;
                                        case 4: //Epistrofh
                                            break;
                                        default:
                                            vm.setTf(TF_Enum.BINARY);
                                            break;
                                    }
                                case 2:
                                    menu.showIdfMenu();
                                    System.out.print(">> ");
                                    int idf = insideScan.nextInt();
                                    switch (idf) {
                                        case 1:
                                            vm.setIdf(IDF_Enum.LOGARITHMIC_1);
                                            break;
                                        case 2:
                                            vm.setIdf(IDF_Enum.LOGARITHMIC_2);
                                            break;
                                        case 3:
                                            vm.setIdf(IDF_Enum.LOGARITHMIC_3);
                                            break;
                                        case 4:
                                            vm.setIdf(IDF_Enum.BINARY);
                                            break;
                                        case 5: //Epistrofh
                                            break;
                                        default:
                                            vm.setIdf(IDF_Enum.BINARY);
                                            break;
                                    }
                                case 3:
                                    menu.showSimilarityMenu();
                                    System.out.print(">> ");
                                    int sim = insideScan.nextInt();
                                    switch (sim) {
                                        case 1:
                                            vm.setSim(SimilarityFunction_Enum.COSINE);
                                            break;
                                        case 2:
                                            vm.setSim(SimilarityFunction_Enum.DICE);
                                            break;
                                        case 3:
                                            vm.setSim(SimilarityFunction_Enum.JACCARD);
                                            break;
                                        case 4:
                                            vm.setSim(SimilarityFunction_Enum.OVERLAPPING);
                                            break;
                                        case 5:
                                            vm.setSim(SimilarityFunction_Enum.INNER_PRODUCT);
                                            break;
                                        case 6: //Epistrofh
                                            break;
                                        default:
                                            vm.setSim(SimilarityFunction_Enum.INNER_PRODUCT);
                                            break;
                                    }
                                case 4:
                                    System.out.println("Dwse tin timi tou katwfliou omoiotitas (px 0,3): ");
                                    System.out.print(">> ");
                                    double threshold = insideScan.nextDouble();
                                    vm.setThreshold(threshold);
                                    vm.setTop_k(Boolean.FALSE);
                                    break;
                                case 5:
                                    System.out.println("Dwse tin timi twn K kaliterwn apotelesmatwn (px 20)");
                                    System.out.print(">> ");
                                    int k = insideScan.nextInt();
                                    vm.setK(k);
                                    vm.setTop_k(Boolean.TRUE);
                                    break;
                                case 6: //Synexeia - use defaults
                                    break;
                                default:
                                    break;
                            }
                            cm.runQueries(vm, MEDqueries, MEDrelevant);
                            break;
                        case 2: //Epistrofh
                            break;
                        default:
                            break;
                    }
                    break;
                case 6: //Kleisimo sylloghs anaforas
                    imgr.closeCollection();
                    break;
                case 7: //Exodos
                    System.exit(0);
                default:
                    break;
            }

        }

        //MENU
        //Dimiourgia sylloghs
        //Anoigma sylloghs
        //Eisagogh eggrafou/eggrafon
        //Diagrafh eggrafou/eggrafon
        //Ektelesh erothmatos
        //Boolean
        //Vector
        //top-k,threshold,tf,idf,similarity
        //Kleisimo sylloghs
        //Dimiourgia sylloghs anaforas
        //Anoigma sylloghs anaforas
        //Ektelesh erothmaton sylloghs anaforas
        //Kleisimo sylloghs anaforas
        //Exodos

    }
}
