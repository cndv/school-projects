/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package UI;

/**
 *
 * @author Workstation
 */
public class Menu {

    public Menu() { }

    public void showMainMenu() {
        System.out.println("\nARES - informAtion REtrieval System");
        System.out.println("\tMenu epilogwn:");
        System.out.println("1 - Dimiourgia sillogis eggrafwn");
        System.out.println("2 - Anoigma sillogis eggrafwn");
        System.out.println("3 - Apothikeusi kai kleisimos sillogis eggrafwn");
        System.out.println("4 - Dimiourgia sillogis anaforas");
        System.out.println("5 - Anoigma sillogis anaforas");
        System.out.println("6 - Apothikeusi kai kleisimo sillogis anaforas");
        System.out.println("7 - Eksodos");
    }

    public void openCollectionMenu() {
        System.out.println("1 - Eisagowgi eggrafou/wn");
        System.out.println("2 - Diagrafi eggrafou/wn");
        System.out.println("3 - Ektelesi erwtimatos");
        System.out.println("4 - Epistrofi");
    }

    public void openRefColletionMenu() {
        System.out.println("1 - Ektelesi erwtimatwn anaforas");
        System.out.println("2 - Epistrofi");
    }

    public void queryMenu() {
        System.out.println("1 - Boolean model");
        System.out.println("2 - Vector model");
        System.out.println("3 - Epistrofi");
    }

    public void stopWordsMenu() {
        System.out.println("1 - Afairesi stopwords apo ta erwtimata");
        System.out.println("2 - Diatirisi stopwords sta erwtimata");
        System.out.println("3 - Epistrofi");
    }

    public void vectorMenu() {
        System.out.println("1 - Epilogi sinartisis TF");
        System.out.println("2 - Epilogi sinartisis IDF");
        System.out.println("3 - Epilogi sinartisis omoiotitas");
        System.out.println("4 - Orismos katwfliou omoiotitas");
        System.out.println("5 - Epilogi Top-K apotelesmatwn");
        System.out.println("6 - Sinexeia (efarmogi default rithmisewn)");
    }

    public void showTfMenu() {
        System.out.println("1 - Simple");
        System.out.println("2 - Logarithmic");
        System.out.println("3 - Binary (default)");
        System.out.println("4 - Epistrofi");
    }

    public void showIdfMenu() {
        System.out.println("1 - Logarithmic 1");
        System.out.println("2 - Logarithmic 2");
        System.out.println("3 - Logarithmic 3");
        System.out.println("4 - Binary (default)");
        System.out.println("5 - Epistrofi");
    }

    public void showSimilarityMenu() {
        System.out.println("1 - Cosine");
        System.out.println("2 - Dice");
        System.out.println("3 - Jaccard");
        System.out.println("4 - Overlapping");
        System.out.println("5 - Inner Product (default)");
        System.out.println("6 - Epistrofi");
    }
}
