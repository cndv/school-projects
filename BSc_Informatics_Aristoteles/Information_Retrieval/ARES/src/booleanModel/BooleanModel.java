/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package booleanModel;

import invertedIndex.IndexManager;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.*;

import org.antlr.runtime.ANTLRStringStream;
import org.antlr.runtime.CharStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.TokenStream;
import org.antlr.runtime.tree.CommonTree;
/*
 * Η κλάση BooleanModel, υλοποιεί το Boolean Μοντέλο
 */
public class BooleanModel 
{
	private IndexManager imgr; 
	
	/**********************************
	  Constructor: initializes an object of an index manager
	 **********************************/
	public BooleanModel(IndexManager imgr) 
	{
		this.imgr=imgr;
	}
	
	/**********************************
	  Create an ASTTree from a file
	 **********************************/
	public void createASTTree(CommonTree t, int indent) throws IOException 
	{				
		PrintStream stdout = System.out;//save the original value of System.out
		if ( t != null ) 
		{
			//change the console output to output.txt
			PrintStream out = new PrintStream(new FileOutputStream("output.txt",true));
			System.setOut(out); //set out to output.txt
			StringBuffer sb = new StringBuffer(indent);
			if (t.getParent() == null)
			{
				System.out.println(sb.toString() + t.getText().toString());
			}
			for ( int i = 0; i < indent; i++ )
				sb = sb.append("   ");
			for ( int i = 0; i < t.getChildCount(); i++ ) 
			{
				System.out.println(sb.toString() + t.getChild(i).toString());
				createASTTree((CommonTree)t.getChild(i), indent+1);				
			}
		}
		System.setOut(stdout); //restore the original value of System.out it through System.setOut
	}
	
	/**********************************
	  Puts the query from the file into stack
	 **********************************/
	public  Stack<String> createQueryStack(String fileName) throws Throwable 
	{
		FileInputStream fstream = new FileInputStream(fileName);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		Stack<String> s = new Stack<String>();
		//Read File Line By Line
		while ((strLine = br.readLine()) != null)   
		{
			// clean white space
			s.push(strLine.replaceAll("\\s",""));
		}
		// Close the input stream
		in.close();
		File file =new File(fileName);
		// delete file
		file.delete();
		return s;
	}
	
	/**********************************
	  Gets the stack that contains the query and
	  returns an array with the documnt ids that
	  satisfy the boolean model  
	 **********************************/
	public ArrayList<String> findDocumentsWithBooleanModel (Stack <String> queryStack)
	{
		Stack<ArrayList<String>> tempStack = new Stack<ArrayList<String>>();
		while (!queryStack.isEmpty()) 
		{
			String term = queryStack.pop();
			if (term.equals("NOT")) 
			{
				Set <String> tempSetTerm1 = new HashSet<String>(tempStack.pop());
				Set <String> tempSetAllDocIds = new HashSet <String>(this.imgr.getDocumentListCopy());
				tempSetAllDocIds.removeAll(tempSetTerm1);
				ArrayList <String> tempAr = new ArrayList <String>(tempSetAllDocIds);
				tempStack.push(tempAr);
			}
			else if (term.equals("AND")) 
			{
				ArrayList <String> t1 = tempStack.pop();
				ArrayList <String> t2 = tempStack.pop();
				if (t1!=null && t2!=null) 
				{
					Set <String> tempSetTerm1 = new HashSet<String>(t1);
					Set <String> tempSetTerm2 = new HashSet<String>(t2);
					tempSetTerm1.retainAll(tempSetTerm2);
					ArrayList <String> tempAr = new ArrayList <String>(tempSetTerm1);
					tempStack.push(tempAr);
				}
				else //δεν ικανοποιεί το AND, οπότε καταχωρούμε την κενή λίστα
				{
					ArrayList <String> tempAr = new ArrayList <String>();
					tempStack.push(tempAr);
				}
			}
			else if (term.equals("OR")) 
			{
				ArrayList <String> t1 = tempStack.pop();
				ArrayList <String> t2 = tempStack.pop();				
				if (t1!=null || t2!=null) 
				{
					Set <String> tempSetTerm1 = new HashSet<String>(t1);
					Set <String> tempSetTerm2 = new HashSet<String>(t2);
					tempSetTerm1.addAll(tempSetTerm2);
					ArrayList <String> tempAr = new ArrayList <String>(tempSetTerm1);
					tempStack.push(tempAr);	
				}
				else //δεν ικανοποιεί το OR, οπότε καταχωρούμε την κενή λίστα
				{
					ArrayList <String> tempAr = new ArrayList <String>();
					tempStack.push(tempAr);	
				}
			}
			else 
			{
				ArrayList<String>tempAr = null;
				tempAr = this.imgr.getDocumentListCopy(term);                                
				if(tempAr != null) 
					tempStack.push(tempAr);
				else 
				{
					tempAr = new ArrayList<String>();
					tempStack.push(tempAr);
				}
			}	
		}
		//finalSetOfDocIds: the return list with the ids
		ArrayList<String> finalSetOfDocIds = null;
		if(!tempStack.isEmpty()) 
			finalSetOfDocIds = tempStack.pop();
		return finalSetOfDocIds ;
	}
	
	/**********************************
	  prints a list with Documents Ids that satisfy the boolean model
	 **********************************/
	public ArrayList<String> query(String str) throws Throwable  {	
		//initializing an expression
		CharStream expression = new ANTLRStringStream(str);
		//initializing a lexer
		SimpleBooleanLexer lexer = new SimpleBooleanLexer (expression);
		TokenStream tokenStream = new CommonTokenStream(lexer);
		//initializing a parser
		SimpleBooleanParser parser = new SimpleBooleanParser (tokenStream);
		SimpleBooleanParser.expr_return ret = parser.expr();
		//initializing an abstract syntact tree
		CommonTree ast = (CommonTree)ret.tree;
		//creating an ast tree and saving it to an output.txt file
		createASTTree(ast,1);
		//put the contents of the output.txt file to a stack
		Stack <String> s = createQueryStack("output.txt");	
		//find the final documents using the boolean model
		ArrayList<String> finalDocIdsList = findDocumentsWithBooleanModel (s);
        return finalDocIdsList;
	}	
}