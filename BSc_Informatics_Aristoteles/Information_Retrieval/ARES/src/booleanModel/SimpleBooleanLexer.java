/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package booleanModel;

// $ANTLR 3.4 /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g 2012-06-15 21:32:03

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class SimpleBooleanLexer extends Lexer {
    public static final int EOF=-1;
    public static final int AND=4;
    public static final int LPAREN=5;
    public static final int NOT=6;
    public static final int OR=7;
    public static final int RPAREN=8;
    public static final int WORD=9;
    public static final int WS=10;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public SimpleBooleanLexer() {} 
    public SimpleBooleanLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public SimpleBooleanLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "/Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g"; }

    // $ANTLR start "LPAREN"
    public final void mLPAREN() throws RecognitionException {
        try {
            int _type = LPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:13:8: ( '(' )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:13:10: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LPAREN"

    // $ANTLR start "RPAREN"
    public final void mRPAREN() throws RecognitionException {
        try {
            int _type = RPAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:14:8: ( ')' )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:14:10: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "RPAREN"

    // $ANTLR start "AND"
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:15:5: ( 'AND' )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:15:7: 'AND'
            {
            match("AND"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "OR"
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:16:4: ( 'OR' )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:16:6: 'OR'
            {
            match("OR"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "NOT"
    public final void mNOT() throws RecognitionException {
        try {
            int _type = NOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:17:4: ( 'NOT' )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:17:6: 'NOT'
            {
            match("NOT"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOT"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:18:4: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:18:7: ( ' ' | '\\t' | '\\r' | '\\n' )
            {
            if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "WORD"
    public final void mWORD() throws RecognitionException {
        try {
            int _type = WORD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:19:6: ( (~ ( ' ' | '\\t' | '\\r' | '\\n' | '(' | ')' ) )* )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:19:9: (~ ( ' ' | '\\t' | '\\r' | '\\n' | '(' | ')' ) )*
            {
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:19:9: (~ ( ' ' | '\\t' | '\\r' | '\\n' | '(' | ')' ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= '\u0000' && LA1_0 <= '\b')||(LA1_0 >= '\u000B' && LA1_0 <= '\f')||(LA1_0 >= '\u000E' && LA1_0 <= '\u001F')||(LA1_0 >= '!' && LA1_0 <= '\'')||(LA1_0 >= '*' && LA1_0 <= '\uFFFF')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\b')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\u001F')||(input.LA(1) >= '!' && input.LA(1) <= '\'')||(input.LA(1) >= '*' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WORD"

    public void mTokens() throws RecognitionException {
        // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:1:8: ( LPAREN | RPAREN | AND | OR | NOT | WS | WORD )
        int alt2=7;
        switch ( input.LA(1) ) {
        case '(':
            {
            alt2=1;
            }
            break;
        case ')':
            {
            alt2=2;
            }
            break;
        case 'A':
            {
            int LA2_3 = input.LA(2);

            if ( (LA2_3=='N') ) {
                int LA2_8 = input.LA(3);

                if ( (LA2_8=='D') ) {
                    int LA2_11 = input.LA(4);

                    if ( ((LA2_11 >= '\u0000' && LA2_11 <= '\b')||(LA2_11 >= '\u000B' && LA2_11 <= '\f')||(LA2_11 >= '\u000E' && LA2_11 <= '\u001F')||(LA2_11 >= '!' && LA2_11 <= '\'')||(LA2_11 >= '*' && LA2_11 <= '\uFFFF')) ) {
                        alt2=7;
                    }
                    else {
                        alt2=3;
                    }
                }
                else {
                    alt2=7;
                }
            }
            else {
                alt2=7;
            }
            }
            break;
        case 'O':
            {
            int LA2_4 = input.LA(2);

            if ( (LA2_4=='R') ) {
                int LA2_9 = input.LA(3);

                if ( ((LA2_9 >= '\u0000' && LA2_9 <= '\b')||(LA2_9 >= '\u000B' && LA2_9 <= '\f')||(LA2_9 >= '\u000E' && LA2_9 <= '\u001F')||(LA2_9 >= '!' && LA2_9 <= '\'')||(LA2_9 >= '*' && LA2_9 <= '\uFFFF')) ) {
                    alt2=7;
                }
                else {
                    alt2=4;
                }
            }
            else {
                alt2=7;
            }
            }
            break;
        case 'N':
            {
            int LA2_5 = input.LA(2);

            if ( (LA2_5=='O') ) {
                int LA2_10 = input.LA(3);

                if ( (LA2_10=='T') ) {
                    int LA2_13 = input.LA(4);

                    if ( ((LA2_13 >= '\u0000' && LA2_13 <= '\b')||(LA2_13 >= '\u000B' && LA2_13 <= '\f')||(LA2_13 >= '\u000E' && LA2_13 <= '\u001F')||(LA2_13 >= '!' && LA2_13 <= '\'')||(LA2_13 >= '*' && LA2_13 <= '\uFFFF')) ) {
                        alt2=7;
                    }
                    else {
                        alt2=5;
                    }
                }
                else {
                    alt2=7;
                }
            }
            else {
                alt2=7;
            }
            }
            break;
        case '\t':
        case '\n':
        case '\r':
        case ' ':
            {
            alt2=6;
            }
            break;
        default:
            alt2=7;
        }

        switch (alt2) {
            case 1 :
                // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:1:10: LPAREN
                {
                mLPAREN(); 


                }
                break;
            case 2 :
                // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:1:17: RPAREN
                {
                mRPAREN(); 


                }
                break;
            case 3 :
                // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:1:24: AND
                {
                mAND(); 


                }
                break;
            case 4 :
                // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:1:28: OR
                {
                mOR(); 


                }
                break;
            case 5 :
                // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:1:31: NOT
                {
                mNOT(); 


                }
                break;
            case 6 :
                // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:1:35: WS
                {
                mWS(); 


                }
                break;
            case 7 :
                // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:1:38: WORD
                {
                mWORD(); 


                }
                break;

        }

    }


 

}