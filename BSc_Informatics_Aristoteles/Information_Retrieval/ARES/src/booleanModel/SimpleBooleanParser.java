/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */

// $ANTLR 3.4 /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g 2012-06-15 21:32:02

  package booleanModel;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class SimpleBooleanParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "LPAREN", "NOT", "OR", "RPAREN", "WORD", "WS"
    };

    public static final int EOF=-1;
    public static final int AND=4;
    public static final int LPAREN=5;
    public static final int NOT=6;
    public static final int OR=7;
    public static final int RPAREN=8;
    public static final int WORD=9;
    public static final int WS=10;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public SimpleBooleanParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public SimpleBooleanParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return SimpleBooleanParser.tokenNames; }
    public String getGrammarFileName() { return "/Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g"; }


    public static class rule_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "rule"
    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:11:1: rule :;
    public final SimpleBooleanParser.rule_return rule() throws RecognitionException {
        SimpleBooleanParser.rule_return retval = new SimpleBooleanParser.rule_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        try {
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:11:5: ()
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:11:7: 
            {
            root_0 = (Object)adaptor.nil();


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "rule"


    public static class expr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "expr"
    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:22:1: expr : orexpr ;
    public final SimpleBooleanParser.expr_return expr() throws RecognitionException {
        SimpleBooleanParser.expr_return retval = new SimpleBooleanParser.expr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        SimpleBooleanParser.orexpr_return orexpr1 =null;



        try {
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:22:6: ( orexpr )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:22:8: orexpr
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_orexpr_in_expr151);
            orexpr1=orexpr();

            state._fsp--;

            adaptor.addChild(root_0, orexpr1.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "expr"


    public static class orexpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "orexpr"
    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:24:1: orexpr : andexpr ( OR ^ andexpr )* ;
    public final SimpleBooleanParser.orexpr_return orexpr() throws RecognitionException {
        SimpleBooleanParser.orexpr_return retval = new SimpleBooleanParser.orexpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token OR3=null;
        SimpleBooleanParser.andexpr_return andexpr2 =null;

        SimpleBooleanParser.andexpr_return andexpr4 =null;


        Object OR3_tree=null;

        try {
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:24:8: ( andexpr ( OR ^ andexpr )* )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:24:10: andexpr ( OR ^ andexpr )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_andexpr_in_orexpr159);
            andexpr2=andexpr();

            state._fsp--;

            adaptor.addChild(root_0, andexpr2.getTree());

            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:24:18: ( OR ^ andexpr )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==OR) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:24:19: OR ^ andexpr
            	    {
            	    OR3=(Token)match(input,OR,FOLLOW_OR_in_orexpr162); 
            	    OR3_tree = 
            	    (Object)adaptor.create(OR3)
            	    ;
            	    root_0 = (Object)adaptor.becomeRoot(OR3_tree, root_0);


            	    pushFollow(FOLLOW_andexpr_in_orexpr165);
            	    andexpr4=andexpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, andexpr4.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "orexpr"


    public static class andexpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "andexpr"
    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:25:1: andexpr : notexpr ( AND ^ notexpr )* ;
    public final SimpleBooleanParser.andexpr_return andexpr() throws RecognitionException {
        SimpleBooleanParser.andexpr_return retval = new SimpleBooleanParser.andexpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token AND6=null;
        SimpleBooleanParser.notexpr_return notexpr5 =null;

        SimpleBooleanParser.notexpr_return notexpr7 =null;


        Object AND6_tree=null;

        try {
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:25:8: ( notexpr ( AND ^ notexpr )* )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:25:10: notexpr ( AND ^ notexpr )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_notexpr_in_andexpr173);
            notexpr5=notexpr();

            state._fsp--;

            adaptor.addChild(root_0, notexpr5.getTree());

            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:25:18: ( AND ^ notexpr )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==AND) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:25:19: AND ^ notexpr
            	    {
            	    AND6=(Token)match(input,AND,FOLLOW_AND_in_andexpr176); 
            	    AND6_tree = 
            	    (Object)adaptor.create(AND6)
            	    ;
            	    root_0 = (Object)adaptor.becomeRoot(AND6_tree, root_0);


            	    pushFollow(FOLLOW_notexpr_in_andexpr179);
            	    notexpr7=notexpr();

            	    state._fsp--;

            	    adaptor.addChild(root_0, notexpr7.getTree());

            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "andexpr"


    public static class notexpr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "notexpr"
    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:26:1: notexpr : ( NOT )* atom ;
    public final SimpleBooleanParser.notexpr_return notexpr() throws RecognitionException {
        SimpleBooleanParser.notexpr_return retval = new SimpleBooleanParser.notexpr_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token NOT8=null;
        SimpleBooleanParser.atom_return atom9 =null;


        Object NOT8_tree=null;

        try {
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:26:9: ( ( NOT )* atom )
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:26:12: ( NOT )* atom
            {
            root_0 = (Object)adaptor.nil();


            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:26:12: ( NOT )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==NOT) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:26:12: NOT
            	    {
            	    NOT8=(Token)match(input,NOT,FOLLOW_NOT_in_notexpr189); 
            	    NOT8_tree = 
            	    (Object)adaptor.create(NOT8)
            	    ;
            	    adaptor.addChild(root_0, NOT8_tree);


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            pushFollow(FOLLOW_atom_in_notexpr192);
            atom9=atom();

            state._fsp--;

            adaptor.addChild(root_0, atom9.getTree());

            }

            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "notexpr"


    public static class atom_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom"
    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:28:1: atom : ( WORD | ( LPAREN ! expr RPAREN !) );
    public final SimpleBooleanParser.atom_return atom() throws RecognitionException {
        SimpleBooleanParser.atom_return retval = new SimpleBooleanParser.atom_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token WORD10=null;
        Token LPAREN11=null;
        Token RPAREN13=null;
        SimpleBooleanParser.expr_return expr12 =null;


        Object WORD10_tree=null;
        Object LPAREN11_tree=null;
        Object RPAREN13_tree=null;

        try {
            // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:28:6: ( WORD | ( LPAREN ! expr RPAREN !) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==WORD) ) {
                alt4=1;
            }
            else if ( (LA4_0==LPAREN) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;

            }
            switch (alt4) {
                case 1 :
                    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:28:8: WORD
                    {
                    root_0 = (Object)adaptor.nil();


                    WORD10=(Token)match(input,WORD,FOLLOW_WORD_in_atom200); 
                    WORD10_tree = 
                    (Object)adaptor.create(WORD10)
                    ;
                    adaptor.addChild(root_0, WORD10_tree);


                    }
                    break;
                case 2 :
                    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:28:15: ( LPAREN ! expr RPAREN !)
                    {
                    root_0 = (Object)adaptor.nil();


                    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:28:15: ( LPAREN ! expr RPAREN !)
                    // /Users/conchar/Dropbox/12se/ir/irWorkspace/booleanParser/src/com/booly/SimpleBoolean.g:28:16: LPAREN ! expr RPAREN !
                    {
                    LPAREN11=(Token)match(input,LPAREN,FOLLOW_LPAREN_in_atom205); 

                    pushFollow(FOLLOW_expr_in_atom208);
                    expr12=expr();

                    state._fsp--;

                    adaptor.addChild(root_0, expr12.getTree());

                    RPAREN13=(Token)match(input,RPAREN,FOLLOW_RPAREN_in_atom210); 

                    }


                    }
                    break;

            }
            retval.stop = input.LT(-1);


            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom"

    // Delegated rules


 

    public static final BitSet FOLLOW_orexpr_in_expr151 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_andexpr_in_orexpr159 = new BitSet(new long[]{0x0000000000000082L});
    public static final BitSet FOLLOW_OR_in_orexpr162 = new BitSet(new long[]{0x0000000000000260L});
    public static final BitSet FOLLOW_andexpr_in_orexpr165 = new BitSet(new long[]{0x0000000000000082L});
    public static final BitSet FOLLOW_notexpr_in_andexpr173 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_AND_in_andexpr176 = new BitSet(new long[]{0x0000000000000260L});
    public static final BitSet FOLLOW_notexpr_in_andexpr179 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_NOT_in_notexpr189 = new BitSet(new long[]{0x0000000000000260L});
    public static final BitSet FOLLOW_atom_in_notexpr192 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WORD_in_atom200 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LPAREN_in_atom205 = new BitSet(new long[]{0x0000000000000260L});
    public static final BitSet FOLLOW_expr_in_atom208 = new BitSet(new long[]{0x0000000000000100L});
    public static final BitSet FOLLOW_RPAREN_in_atom210 = new BitSet(new long[]{0x0000000000000002L});

}