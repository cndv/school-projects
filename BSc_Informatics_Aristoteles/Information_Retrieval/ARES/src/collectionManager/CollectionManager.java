
/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package collectionManager;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import moj.lang.StopList;
import vectorModel.VectorModel;
import vectorModel.documentRank;

/**
 *
 * @author root
 */
public class CollectionManager {

    public CollectionManager() {
    }

    public void splitMED(String dir) throws FileNotFoundException, IOException {
        File documentCollection = new File(dir + "\\med_docs.txt");
        File outputFolder = new File(dir + "\\MED_DOCS");
        boolean mkdir = outputFolder.mkdir();

        //Input streams
        FileInputStream in = new FileInputStream(documentCollection);
        DataInputStream din = new DataInputStream(in);
        BufferedReader fin = new BufferedReader(new InputStreamReader(din));

        //Output streams
        FileWriter fw = null;
        BufferedWriter fout = null;

        //Diavasma tou arxeiou MED
        String line;
        while ((line = fin.readLine()) != null) {
            //An kapoia grammi arxizei me .I
            if ((line.indexOf(".I") == 0)) {
                //Kopse to .I kai krata to noumero pou akolouthei
                String fileNum = line.substring(3, line.length());
                //Kleise ta file streams pou einai anoikta apo prin (ektos ths 1hs foras)
                if (fout != null) {
                    fout.close();
                    fw.close();
                }
                //Ftiakse to neo arxeio kai dhmiourghse to
                fw = new FileWriter(dir + "\\MED_DOCS\\" + fileNum + ".txt");
                fout = new BufferedWriter(fw);
                //Prosperase kai to ".W" pou akolouthei tou arithmou
                fin.readLine();
            } //Allios exoume mia apli grammi keimenou, grapsthn sto trexon arxeio
            else {
                fout.write(line);
                fout.newLine();
            }
        }
        fout.close();
        fw.close();
    }

    public void splitCRAN(String dir) throws FileNotFoundException, IOException {
        File documentCollection = new File(dir + "\\cran_docs.txt");
        File outputFolder = new File(dir + "\\CRAN_DOCS");
        boolean mkdir = outputFolder.mkdir();

        //Input streams
        FileInputStream in = new FileInputStream(documentCollection);
        DataInputStream din = new DataInputStream(in);
        BufferedReader fin = new BufferedReader(new InputStreamReader(din));

        //Output streams
        FileWriter fw = null;
        BufferedWriter fout = null;

        //Diavasma tou arxeiou CRAN
        String line;
        while ((line = fin.readLine()) != null) {
            //An kapoia grammi arxizei me .I
            if ((line.indexOf(".I") == 0)) {
                //Kopse to .I kai krata to noumero pou akolouthei
                String fileNum = line.substring(3, line.length());
                //Kleise ta file streams pou einai anoikta apo prin (ektos ths 1hs foras)
                if (fout != null) {
                    fout.close();
                    fw.close();
                }
                //Ftiakse to neo arxeio kai dhmiourghse to
                fw = new FileWriter(dir + "\\CRAN_DOCS\\" + fileNum + ".txt");
                fout = new BufferedWriter(fw);
                //Prosperase kai to ".T" pou akolouthei tou arithmou
                fin.readLine();
                //Diavase tis ypoloipes grammes (ton titlo) kai grapstes
                //sto arxeio (mexri na vreis to ".A")
                while ((line = fin.readLine()).indexOf(".A") != 0) {
                    fout.write(line);
                    fout.newLine();
                }
                //Exoume vre to ".A", prospername ta panta mexri na vroume to ".W"
                while ((line = fin.readLine()).indexOf(".W") != 0);
            } //Allios exoume mia apli grammi keimenou, grapsthn sto trexon arxeio
            else {
                fout.write(line);
                fout.newLine();
            }
        }
        fout.close();
        fw.close();
    }

    public void proccessQueries(String dir, HashMap queries, Boolean stopwords) throws FileNotFoundException, IOException {
        StopList stoplist = new StopList();
        if (stopwords) {
            populateStopList(stoplist);
        }

        File queryCollection = new File(dir);
        //Input streams
        FileInputStream in = new FileInputStream(queryCollection);
        DataInputStream din = new DataInputStream(in);
        BufferedReader fin = new BufferedReader(new InputStreamReader(din));

        queries.clear();
        String line;
        String queryID = null;
        String query = "";
        while ((line = fin.readLine()) != null) {
            //An arxizei me I
            if (line.indexOf(".I") == 0) {
                if (queryID != null) {
                    if (stopwords) {
                        query = stoplist.removeStopWords(query);
                    }
                    queries.put(queryID, query);
                }
                query = "";
                queryID = line.substring(3, line.length());
                fin.readLine();
            } else {
                query += line;
            }
        }
        //Eisagogh tou teleytaiou query
        if (stopwords) {
            query = stoplist.removeStopWords(query);
        }
        queries.put(queryID, query);

        //System.out.println("Number of queries: " + queries.size());
        /*
        for(int i=1; i<=queries.size()+140; i++)
        {
        if(!queries.containsKey(Integer.toString(i))) continue;
        System.out.println("Query " + i + ": " + queries.containsKey(Integer.toString(i)));
        System.out.println(queries.get(Integer.toString(i)));
        }
         */
    }

    public void proccessRelevant(String dir, HashMap<String, ArrayList<String>> relevant) throws FileNotFoundException, IOException {
        File relevantCollection = new File(dir);
        //Input streams
        FileInputStream in = new FileInputStream(relevantCollection);
        DataInputStream din = new DataInputStream(in);
        BufferedReader fin = new BufferedReader(new InputStreamReader(din));

        relevant.clear();
        String line;
        String query_id = null;
        String doc_id = null;
        while ((line = fin.readLine()) != null) {
            int k = line.indexOf(" ");
            query_id = line.substring(0, k);
            int l = line.indexOf(" ", k + 1);
            doc_id = line.substring(k + 1, l);
            if (relevant.get(query_id) != null) {
                relevant.get(query_id).add(doc_id);
            } else {
                ArrayList<String> temp = new ArrayList<String>();
                temp.add(doc_id);
                relevant.put(query_id, temp);
            }
        }

        //System.out.println("Number of relevant query entries: " + relevant.size());
        /*
        String newLine = System.getProperty("line.separator");
        for(int i=1; i<=relevant.size()+140; i++)
        {
        if(!relevant.containsKey(Integer.toString(i))) continue;
        System.out.println(newLine + "Query: " + i + ", relevant docs:");
        for(int j=0; j<relevant.get(Integer.toString(i)).size(); j++)
        System.out.print(relevant.get(Integer.toString(i)).get(j) + " ");
        }
         */
    }

    public void runQueries(VectorModel vm, HashMap<String, String> queries, HashMap<String, ArrayList<String>> relevant) {
        ArrayList<Float> precisions = new ArrayList<Float>();
        ArrayList<Float> recalls = new ArrayList<Float>();
        long time = System.currentTimeMillis();
        for (int i = 1; i <= queries.size(); i++) {
            if (!queries.containsKey(Integer.toString(i))) {
                continue;
            }
            String resultString = queries.get(Integer.toString(i)).replaceAll("[.,]+", "");
            String[] query = resultString.trim().split("\\s+");
            ArrayList<String> terms = new ArrayList(Arrays.asList(query));
            //System.out.println(terms);
            ArrayList<documentRank> results;
            results = vm.runQuery(terms);
            //System.out.println(results);

            float recall = 0, precision = 0;
            int recallSum = 0, precisionSum = 0;
            for (int j = 0; j < results.size(); j++) {
                int end = results.get(j).getDocID().lastIndexOf(".txt");
                String docID = results.get(j).getDocID().substring(0, end);
                //System.out.println(docID);
                if (relevant.get(Integer.toString(i)).contains(docID)) {
                    recallSum++;
                    precisionSum++;
                }
            }
            recall = (float) recallSum / (float) relevant.get(Integer.toString(i)).size();
            recall = (float) (Math.round(recall * 1000.0) / 1000.0);
            precision = (float) precisionSum / (float) results.size();
            precision = (float) (Math.round(precision * 1000.0) / 1000.0);
            precisions.add(precision);
            recalls.add(recall);
            System.out.println("Query " + i + ":");
            System.out.println("\tRecall: " + recallSum + "/" + relevant.get(Integer.toString(i)).size() + " = " + recall * 100 + "%");
            System.out.println("\tPrecision: " + precisionSum + "/" + results.size() + " = " + precision * 100 + "%");
        }
        float avgRecall = 0, avgPrecision = 0;
        for (int i = 0; i < recalls.size(); i++) {
            avgRecall += recalls.get(i);
        }
        for (int i = 0; i < precisions.size(); i++) {
            avgPrecision += precisions.get(i);
        }
        avgRecall = (float) avgRecall / (float) recalls.size();
        avgRecall = (float) (Math.round(avgRecall * 1000.0) / 1000.0);
        avgPrecision = (float) avgPrecision / (float) precisions.size();
        avgPrecision = (float) (Math.round(avgPrecision * 1000.0) / 1000.0);
        time = System.currentTimeMillis() - time;
        System.out.println("Average Recall: " + avgRecall * 100 + "%");
        System.out.println("Average Precision: " + avgPrecision * 100 + "%");
        System.out.println("Time in milliseconds: " + time + "msec");
    }

    private void populateStopList(StopList stoplist) {
        //Stopwords list from: http://www.cs.cmu.edu/~mccallum/bow/rainbow/
        stoplist.addStopWord("in");
        stoplist.addStopWord("or");
        stoplist.addStopWord("of");
        stoplist.addStopWord("and");
        stoplist.addStopWord("the");
        stoplist.addStopWord("a");
        stoplist.addStopWord("to");
        stoplist.addStopWord("by");
        stoplist.addStopWord("a");
        stoplist.addStopWord("able");
        stoplist.addStopWord("about");
        stoplist.addStopWord("above");
        stoplist.addStopWord("according");
        stoplist.addStopWord("accordingly");
        stoplist.addStopWord("across");
        stoplist.addStopWord("actually");
        stoplist.addStopWord("after");
        stoplist.addStopWord("afterwards");
        stoplist.addStopWord("again");
        stoplist.addStopWord("against");
        stoplist.addStopWord("all");
        stoplist.addStopWord("allow");
        stoplist.addStopWord("allows");
        stoplist.addStopWord("almost");
        stoplist.addStopWord("alone");
        stoplist.addStopWord("along");
        stoplist.addStopWord("already");
        stoplist.addStopWord("also");
        stoplist.addStopWord("although");
        stoplist.addStopWord("always");
        stoplist.addStopWord("am");
        stoplist.addStopWord("among");
        stoplist.addStopWord("amongst");
        stoplist.addStopWord("an");
        stoplist.addStopWord("and");
        stoplist.addStopWord("another");
        stoplist.addStopWord("any");
        stoplist.addStopWord("anybody");
        stoplist.addStopWord("anyhow");
        stoplist.addStopWord("anyone");
        stoplist.addStopWord("anything");
        stoplist.addStopWord("anyway");
        stoplist.addStopWord("anyways");
        stoplist.addStopWord("anywhere");
        stoplist.addStopWord("apart");
        stoplist.addStopWord("appear");
        stoplist.addStopWord("appreciate");
        stoplist.addStopWord("appropriate");
        stoplist.addStopWord("are");
        stoplist.addStopWord("around");
        stoplist.addStopWord("as");
        stoplist.addStopWord("aside");
        stoplist.addStopWord("ask");
        stoplist.addStopWord("asking");
        stoplist.addStopWord("associated");
        stoplist.addStopWord("at");
        stoplist.addStopWord("available");
        stoplist.addStopWord("away");
        stoplist.addStopWord("awfully");
        stoplist.addStopWord("b");
        stoplist.addStopWord("be");
        stoplist.addStopWord("became");
        stoplist.addStopWord("because");
        stoplist.addStopWord("become");
        stoplist.addStopWord("becomes");
        stoplist.addStopWord("becoming");
        stoplist.addStopWord("been");
        stoplist.addStopWord("before");
        stoplist.addStopWord("beforehand");
        stoplist.addStopWord("behind");
        stoplist.addStopWord("being");
        stoplist.addStopWord("believe");
        stoplist.addStopWord("below");
        stoplist.addStopWord("beside");
        stoplist.addStopWord("besides");
        stoplist.addStopWord("best");
        stoplist.addStopWord("better");
        stoplist.addStopWord("between");
        stoplist.addStopWord("beyond");
        stoplist.addStopWord("both");
        stoplist.addStopWord("brief");
        stoplist.addStopWord("but");
        stoplist.addStopWord("by");
        stoplist.addStopWord("c");
        stoplist.addStopWord("came");
        stoplist.addStopWord("can");
        stoplist.addStopWord("cannot");
        stoplist.addStopWord("cant");
        stoplist.addStopWord("cause");
        stoplist.addStopWord("causes");
        stoplist.addStopWord("certain");
        stoplist.addStopWord("certainly");
        stoplist.addStopWord("changes");
        stoplist.addStopWord("clearly");
        stoplist.addStopWord("co");
        stoplist.addStopWord("com");
        stoplist.addStopWord("come");
        stoplist.addStopWord("comes");
        stoplist.addStopWord("concerning");
        stoplist.addStopWord("consequently");
        stoplist.addStopWord("consider");
        stoplist.addStopWord("considering");
        stoplist.addStopWord("contain");
        stoplist.addStopWord("containing");
        stoplist.addStopWord("contains");
        stoplist.addStopWord("corresponding");
        stoplist.addStopWord("could");
        stoplist.addStopWord("course");
        stoplist.addStopWord("currently");
        stoplist.addStopWord("d");
        stoplist.addStopWord("definitely");
        stoplist.addStopWord("described");
        stoplist.addStopWord("despite");
        stoplist.addStopWord("did");
        stoplist.addStopWord("different");
        stoplist.addStopWord("do");
        stoplist.addStopWord("does");
        stoplist.addStopWord("doing");
        stoplist.addStopWord("done");
        stoplist.addStopWord("down");
        stoplist.addStopWord("downwards");
        stoplist.addStopWord("during");
        stoplist.addStopWord("e");
        stoplist.addStopWord("each");
        stoplist.addStopWord("edu");
        stoplist.addStopWord("eg");
        stoplist.addStopWord("eight");
        stoplist.addStopWord("either");
        stoplist.addStopWord("else");
        stoplist.addStopWord("elsewhere");
        stoplist.addStopWord("enough");
        stoplist.addStopWord("entirely");
        stoplist.addStopWord("especially");
        stoplist.addStopWord("et");
        stoplist.addStopWord("etc");
        stoplist.addStopWord("even");
        stoplist.addStopWord("ever");
        stoplist.addStopWord("every");
        stoplist.addStopWord("everybody");
        stoplist.addStopWord("everyone");
        stoplist.addStopWord("everything");
        stoplist.addStopWord("everywhere");
        stoplist.addStopWord("ex");
        stoplist.addStopWord("exactly");
        stoplist.addStopWord("example");
        stoplist.addStopWord("except");
        stoplist.addStopWord("f");
        stoplist.addStopWord("far");
        stoplist.addStopWord("few");
        stoplist.addStopWord("fifth");
        stoplist.addStopWord("first");
        stoplist.addStopWord("five");
        stoplist.addStopWord("followed");
        stoplist.addStopWord("following");
        stoplist.addStopWord("follows");
        stoplist.addStopWord("for");
        stoplist.addStopWord("former");
        stoplist.addStopWord("formerly");
        stoplist.addStopWord("forth");
        stoplist.addStopWord("four");
        stoplist.addStopWord("from");
        stoplist.addStopWord("further");
        stoplist.addStopWord("furthermore");
        stoplist.addStopWord("g");
        stoplist.addStopWord("get");
        stoplist.addStopWord("gets");
        stoplist.addStopWord("getting");
        stoplist.addStopWord("given");
        stoplist.addStopWord("gives");
        stoplist.addStopWord("go");
        stoplist.addStopWord("goes");
        stoplist.addStopWord("going");
        stoplist.addStopWord("gone");
        stoplist.addStopWord("got");
        stoplist.addStopWord("gotten");
        stoplist.addStopWord("greetings");
        stoplist.addStopWord("h");
        stoplist.addStopWord("had");
        stoplist.addStopWord("happens");
        stoplist.addStopWord("hardly");
        stoplist.addStopWord("has");
        stoplist.addStopWord("have");
        stoplist.addStopWord("having");
        stoplist.addStopWord("he");
        stoplist.addStopWord("hello");
        stoplist.addStopWord("help");
        stoplist.addStopWord("hence");
        stoplist.addStopWord("her");
        stoplist.addStopWord("here");
        stoplist.addStopWord("hereafter");
        stoplist.addStopWord("hereby");
        stoplist.addStopWord("herein");
        stoplist.addStopWord("hereupon");
        stoplist.addStopWord("hers");
        stoplist.addStopWord("herself");
        stoplist.addStopWord("hi");
        stoplist.addStopWord("him");
        stoplist.addStopWord("himself");
        stoplist.addStopWord("his");
        stoplist.addStopWord("hither");
        stoplist.addStopWord("hopefully");
        stoplist.addStopWord("how");
        stoplist.addStopWord("howbeit");
        stoplist.addStopWord("however");
        stoplist.addStopWord("i");
        stoplist.addStopWord("ie");
        stoplist.addStopWord("if");
        stoplist.addStopWord("ignored");
        stoplist.addStopWord("immediate");
        stoplist.addStopWord("in");
        stoplist.addStopWord("inasmuch");
        stoplist.addStopWord("inc");
        stoplist.addStopWord("indeed");
        stoplist.addStopWord("indicate");
        stoplist.addStopWord("indicated");
        stoplist.addStopWord("indicates");
        stoplist.addStopWord("inner");
        stoplist.addStopWord("insofar");
        stoplist.addStopWord("instead");
        stoplist.addStopWord("into");
        stoplist.addStopWord("inward");
        stoplist.addStopWord("is");
        stoplist.addStopWord("it");
        stoplist.addStopWord("its");
        stoplist.addStopWord("itself");
        stoplist.addStopWord("j");
        stoplist.addStopWord("just");
        stoplist.addStopWord("k");
        stoplist.addStopWord("keep");
        stoplist.addStopWord("keeps");
        stoplist.addStopWord("kept");
        stoplist.addStopWord("know");
        stoplist.addStopWord("knows");
        stoplist.addStopWord("known");
        stoplist.addStopWord("l");
        stoplist.addStopWord("last");
        stoplist.addStopWord("lately");
        stoplist.addStopWord("later");
        stoplist.addStopWord("latter");
        stoplist.addStopWord("latterly");
        stoplist.addStopWord("least");
        stoplist.addStopWord("less");
        stoplist.addStopWord("lest");
        stoplist.addStopWord("let");
        stoplist.addStopWord("like");
        stoplist.addStopWord("liked");
        stoplist.addStopWord("likely");
        stoplist.addStopWord("little");
        stoplist.addStopWord("ll"); //stoplist.addStopWorded to avoid words like you'll,I'll etc.
        stoplist.addStopWord("look");
        stoplist.addStopWord("looking");
        stoplist.addStopWord("looks");
        stoplist.addStopWord("ltd");
        stoplist.addStopWord("m");
        stoplist.addStopWord("mainly");
        stoplist.addStopWord("many");
        stoplist.addStopWord("may");
        stoplist.addStopWord("maybe");
        stoplist.addStopWord("me");
        stoplist.addStopWord("mean");
        stoplist.addStopWord("meanwhile");
        stoplist.addStopWord("merely");
        stoplist.addStopWord("might");
        stoplist.addStopWord("more");
        stoplist.addStopWord("moreover");
        stoplist.addStopWord("most");
        stoplist.addStopWord("mostly");
        stoplist.addStopWord("much");
        stoplist.addStopWord("must");
        stoplist.addStopWord("my");
        stoplist.addStopWord("myself");
        stoplist.addStopWord("n");
        stoplist.addStopWord("name");
        stoplist.addStopWord("namely");
        stoplist.addStopWord("nd");
        stoplist.addStopWord("near");
        stoplist.addStopWord("nearly");
        stoplist.addStopWord("necessary");
        stoplist.addStopWord("need");
        stoplist.addStopWord("needs");
        stoplist.addStopWord("neither");
        stoplist.addStopWord("never");
        stoplist.addStopWord("nevertheless");
        stoplist.addStopWord("new");
        stoplist.addStopWord("next");
        stoplist.addStopWord("nine");
        stoplist.addStopWord("no");
        stoplist.addStopWord("nobody");
        stoplist.addStopWord("non");
        stoplist.addStopWord("none");
        stoplist.addStopWord("noone");
        stoplist.addStopWord("nor");
        stoplist.addStopWord("normally");
        stoplist.addStopWord("not");
        stoplist.addStopWord("nothing");
        stoplist.addStopWord("novel");
        stoplist.addStopWord("now");
        stoplist.addStopWord("nowhere");
        stoplist.addStopWord("o");
        stoplist.addStopWord("obviously");
        stoplist.addStopWord("of");
        stoplist.addStopWord("off");
        stoplist.addStopWord("often");
        stoplist.addStopWord("oh");
        stoplist.addStopWord("ok");
        stoplist.addStopWord("okay");
        stoplist.addStopWord("old");
        stoplist.addStopWord("on");
        stoplist.addStopWord("once");
        stoplist.addStopWord("one");
        stoplist.addStopWord("ones");
        stoplist.addStopWord("only");
        stoplist.addStopWord("onto");
        stoplist.addStopWord("or");
        stoplist.addStopWord("other");
        stoplist.addStopWord("others");
        stoplist.addStopWord("otherwise");
        stoplist.addStopWord("ought");
        stoplist.addStopWord("our");
        stoplist.addStopWord("ours");
        stoplist.addStopWord("ourselves");
        stoplist.addStopWord("out");
        stoplist.addStopWord("outside");
        stoplist.addStopWord("over");
        stoplist.addStopWord("overall");
        stoplist.addStopWord("own");
        stoplist.addStopWord("p");
        stoplist.addStopWord("particular");
        stoplist.addStopWord("particularly");
        stoplist.addStopWord("per");
        stoplist.addStopWord("perhaps");
        stoplist.addStopWord("placed");
        stoplist.addStopWord("please");
        stoplist.addStopWord("plus");
        stoplist.addStopWord("possible");
        stoplist.addStopWord("presumably");
        stoplist.addStopWord("probably");
        stoplist.addStopWord("provides");
        stoplist.addStopWord("q");
        stoplist.addStopWord("que");
        stoplist.addStopWord("quite");
        stoplist.addStopWord("qv");
        stoplist.addStopWord("r");
        stoplist.addStopWord("rather");
        stoplist.addStopWord("rd");
        stoplist.addStopWord("re");
        stoplist.addStopWord("really");
        stoplist.addStopWord("reasonably");
        stoplist.addStopWord("regarding");
        stoplist.addStopWord("regardless");
        stoplist.addStopWord("regards");
        stoplist.addStopWord("relatively");
        stoplist.addStopWord("respectively");
        stoplist.addStopWord("right");
        stoplist.addStopWord("s");
        stoplist.addStopWord("said");
        stoplist.addStopWord("same");
        stoplist.addStopWord("saw");
        stoplist.addStopWord("say");
        stoplist.addStopWord("saying");
        stoplist.addStopWord("says");
        stoplist.addStopWord("second");
        stoplist.addStopWord("secondly");
        stoplist.addStopWord("see");
        stoplist.addStopWord("seeing");
        stoplist.addStopWord("seem");
        stoplist.addStopWord("seemed");
        stoplist.addStopWord("seeming");
        stoplist.addStopWord("seems");
        stoplist.addStopWord("seen");
        stoplist.addStopWord("self");
        stoplist.addStopWord("selves");
        stoplist.addStopWord("sensible");
        stoplist.addStopWord("sent");
        stoplist.addStopWord("serious");
        stoplist.addStopWord("seriously");
        stoplist.addStopWord("seven");
        stoplist.addStopWord("several");
        stoplist.addStopWord("shall");
        stoplist.addStopWord("she");
        stoplist.addStopWord("should");
        stoplist.addStopWord("since");
        stoplist.addStopWord("six");
        stoplist.addStopWord("so");
        stoplist.addStopWord("some");
        stoplist.addStopWord("somebody");
        stoplist.addStopWord("somehow");
        stoplist.addStopWord("someone");
        stoplist.addStopWord("something");
        stoplist.addStopWord("sometime");
        stoplist.addStopWord("sometimes");
        stoplist.addStopWord("somewhat");
        stoplist.addStopWord("somewhere");
        stoplist.addStopWord("soon");
        stoplist.addStopWord("sorry");
        stoplist.addStopWord("specified");
        stoplist.addStopWord("specify");
        stoplist.addStopWord("specifying");
        stoplist.addStopWord("still");
        stoplist.addStopWord("sub");
        stoplist.addStopWord("such");
        stoplist.addStopWord("sup");
        stoplist.addStopWord("sure");
        stoplist.addStopWord("t");
        stoplist.addStopWord("take");
        stoplist.addStopWord("taken");
        stoplist.addStopWord("tell");
        stoplist.addStopWord("tends");
        stoplist.addStopWord("th");
        stoplist.addStopWord("than");
        stoplist.addStopWord("thank");
        stoplist.addStopWord("thanks");
        stoplist.addStopWord("thanx");
        stoplist.addStopWord("that");
        stoplist.addStopWord("thats");
        stoplist.addStopWord("the");
        stoplist.addStopWord("their");
        stoplist.addStopWord("theirs");
        stoplist.addStopWord("them");
        stoplist.addStopWord("themselves");
        stoplist.addStopWord("then");
        stoplist.addStopWord("thence");
        stoplist.addStopWord("there");
        stoplist.addStopWord("thereafter");
        stoplist.addStopWord("thereby");
        stoplist.addStopWord("therefore");
        stoplist.addStopWord("therein");
        stoplist.addStopWord("theres");
        stoplist.addStopWord("thereupon");
        stoplist.addStopWord("these");
        stoplist.addStopWord("they");
        stoplist.addStopWord("think");
        stoplist.addStopWord("third");
        stoplist.addStopWord("this");
        stoplist.addStopWord("thorough");
        stoplist.addStopWord("thoroughly");
        stoplist.addStopWord("those");
        stoplist.addStopWord("though");
        stoplist.addStopWord("three");
        stoplist.addStopWord("through");
        stoplist.addStopWord("throughout");
        stoplist.addStopWord("thru");
        stoplist.addStopWord("thus");
        stoplist.addStopWord("to");
        stoplist.addStopWord("together");
        stoplist.addStopWord("too");
        stoplist.addStopWord("took");
        stoplist.addStopWord("toward");
        stoplist.addStopWord("towards");
        stoplist.addStopWord("tried");
        stoplist.addStopWord("tries");
        stoplist.addStopWord("truly");
        stoplist.addStopWord("try");
        stoplist.addStopWord("trying");
        stoplist.addStopWord("twice");
        stoplist.addStopWord("two");
        stoplist.addStopWord("u");
        stoplist.addStopWord("un");
        stoplist.addStopWord("under");
        stoplist.addStopWord("unfortunately");
        stoplist.addStopWord("unless");
        stoplist.addStopWord("unlikely");
        stoplist.addStopWord("until");
        stoplist.addStopWord("unto");
        stoplist.addStopWord("up");
        stoplist.addStopWord("upon");
        stoplist.addStopWord("us");
        stoplist.addStopWord("use");
        stoplist.addStopWord("used");
        stoplist.addStopWord("useful");
        stoplist.addStopWord("uses");
        stoplist.addStopWord("using");
        stoplist.addStopWord("usually");
        stoplist.addStopWord("uucp");
        stoplist.addStopWord("v");
        stoplist.addStopWord("value");
        stoplist.addStopWord("various");
        stoplist.addStopWord("ve"); //stoplist.addStopWorded to avoid words like I've,you've etc.
        stoplist.addStopWord("very");
        stoplist.addStopWord("via");
        stoplist.addStopWord("viz");
        stoplist.addStopWord("vs");
        stoplist.addStopWord("w");
        stoplist.addStopWord("want");
        stoplist.addStopWord("wants");
        stoplist.addStopWord("was");
        stoplist.addStopWord("way");
        stoplist.addStopWord("we");
        stoplist.addStopWord("welcome");
        stoplist.addStopWord("well");
        stoplist.addStopWord("went");
        stoplist.addStopWord("were");
        stoplist.addStopWord("what");
        stoplist.addStopWord("whatever");
        stoplist.addStopWord("when");
        stoplist.addStopWord("whence");
        stoplist.addStopWord("whenever");
        stoplist.addStopWord("where");
        stoplist.addStopWord("whereafter");
        stoplist.addStopWord("whereas");
        stoplist.addStopWord("whereby");
        stoplist.addStopWord("wherein");
        stoplist.addStopWord("whereupon");
        stoplist.addStopWord("wherever");
        stoplist.addStopWord("whether");
        stoplist.addStopWord("which");
        stoplist.addStopWord("while");
        stoplist.addStopWord("whither");
        stoplist.addStopWord("who");
        stoplist.addStopWord("whoever");
        stoplist.addStopWord("whole");
        stoplist.addStopWord("whom");
        stoplist.addStopWord("whose");
        stoplist.addStopWord("why");
        stoplist.addStopWord("will");
        stoplist.addStopWord("willing");
        stoplist.addStopWord("wish");
        stoplist.addStopWord("with");
        stoplist.addStopWord("within");
        stoplist.addStopWord("without");
        stoplist.addStopWord("wonder");
        stoplist.addStopWord("would");
        stoplist.addStopWord("would");
        stoplist.addStopWord("x");
        stoplist.addStopWord("y");
        stoplist.addStopWord("yes");
        stoplist.addStopWord("yet");
        stoplist.addStopWord("you");
        stoplist.addStopWord("your");
        stoplist.addStopWord("yours");
        stoplist.addStopWord("yourself");
        stoplist.addStopWord("yourselves");
        stoplist.addStopWord("z");
        stoplist.addStopWord("zero");
    }
}
