/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package invertedIndex;

import java.io.Serializable;

public class DocumentHits implements Comparable<DocumentHits>, Serializable {

    private String docID;
    private int numberOfHits;

    public DocumentHits() {
        this.numberOfHits = 1;
    }

    public DocumentHits(String val) {
        this();
        this.docID = val;
    }

    public void increaseNumberOfHits() {
        this.numberOfHits++;
    }

    public int getNumberOfHits() {
        return numberOfHits;
    }

    public String getDocumentID() {
        return this.docID;
    }

    public int compareTo(DocumentHits t) {
        return this.docID.compareTo(t.docID);
    }

    //Ttesting
    @Override
    public String toString() {
        return "(" + this.docID + "," + this.numberOfHits + ")";
    }
}
