
/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package invertedIndex;

import java.io.Serializable;
import vectorModel.IDF_Enum;
import vectorModel.TF_Enum;

/**
 *
 * @author zisis
 */
public final class DocumentNorm implements Serializable {

    private String docId;
    private double Binary_Binary;
    private double Binary_Logarithmic1;
    private double Binary_Logarithmic2;
    private double Binary_Logarithmic3;
    private double Simple_Binary;
    private double Simple_Logarithmic1;
    private double Simple_Logarithmic2;
    private double Simple_Logarithmic3;
    private double Logarithmic_Binary;
    private double Logarithmic_Logarithmic1;
    private double Logarithmic_Logarithmic2;
    private double Logarithmic_Logarithmic3;

    public DocumentNorm(String docId) {
        this.docId = docId;
        this.initialize();
    }
    
    public void initialize(){
        this.Binary_Binary = 0;
        this.Binary_Logarithmic1 = 0;
        this.Binary_Logarithmic2 = 0;
        this.Binary_Logarithmic3 = 0;
        this.Simple_Binary = 0;
        this.Simple_Logarithmic1 = 0;
        this.Simple_Logarithmic2 = 0;
        this.Simple_Logarithmic3 = 0;
        this.Logarithmic_Binary = 0;
        this.Logarithmic_Logarithmic1 = 0;
        this.Logarithmic_Logarithmic2 = 0;
        this.Logarithmic_Logarithmic3 = 0;
    }

    public void add(int numberOfDocsContainingTerm, int termFrequence, int numberOfAllDocs) {
        double tempTfBinary = TF_Enum.BINARY.getTF(termFrequence);
        double tempTfSimple = TF_Enum.SIMPLE.getTF(termFrequence);
        double tempTfLog = TF_Enum.LOGARITHMIC.getTF(termFrequence);

        double tempIdf;

        tempIdf = IDF_Enum.BINARY.getIDF(numberOfAllDocs, numberOfDocsContainingTerm);
        this.Binary_Binary += Math.pow(tempTfBinary * tempIdf, 2);
        this.Simple_Binary += Math.pow(tempTfSimple * tempIdf, 2);
        this.Logarithmic_Binary += Math.pow(tempTfLog * tempIdf, 2);

        tempIdf = IDF_Enum.LOGARITHMIC_1.getIDF(numberOfAllDocs, numberOfDocsContainingTerm);
        this.Binary_Logarithmic1 += Math.pow(tempTfBinary * tempIdf, 2);
        this.Simple_Logarithmic1 += Math.pow(tempTfSimple * tempIdf, 2);
        this.Logarithmic_Logarithmic1 += Math.pow(tempTfLog * tempIdf, 2);

        tempIdf = IDF_Enum.LOGARITHMIC_2.getIDF(numberOfAllDocs, numberOfDocsContainingTerm);
        this.Binary_Logarithmic2 += Math.pow(tempTfBinary * tempIdf, 2);
        this.Simple_Logarithmic2 += Math.pow(tempTfSimple * tempIdf, 2);
        this.Logarithmic_Logarithmic2 += Math.pow(tempTfLog * tempIdf, 2);

        tempIdf = IDF_Enum.LOGARITHMIC_3.getIDF(numberOfAllDocs, numberOfDocsContainingTerm);
        this.Binary_Logarithmic3 += Math.pow(tempTfBinary * tempIdf, 2);
        this.Simple_Logarithmic3 += Math.pow(tempTfSimple * tempIdf, 2);
        this.Logarithmic_Logarithmic3 += Math.pow(tempTfLog * tempIdf, 2);
    }

    public void finish() {
        this.Binary_Binary = Math.sqrt(Binary_Binary);
        this.Binary_Logarithmic1 = Math.sqrt(Binary_Logarithmic1);
        this.Binary_Logarithmic2 = Math.sqrt(Binary_Logarithmic2);
        this.Binary_Logarithmic3 = Math.sqrt(Binary_Logarithmic3);
        this.Simple_Binary = Math.sqrt(Simple_Binary);
        this.Simple_Logarithmic1 = Math.sqrt(Simple_Logarithmic1);
        this.Simple_Logarithmic2 = Math.sqrt(Simple_Logarithmic2);
        this.Simple_Logarithmic3 = Math.sqrt(Simple_Logarithmic3);
        this.Logarithmic_Binary = Math.sqrt(Logarithmic_Binary);
        this.Logarithmic_Logarithmic1 = Math.sqrt(Logarithmic_Logarithmic1);
        this.Logarithmic_Logarithmic2 = Math.sqrt(Logarithmic_Logarithmic2);
        this.Logarithmic_Logarithmic3 = Math.sqrt(Logarithmic_Logarithmic3);
    }

    public double getNorm(TF_Enum tf, IDF_Enum idf) {
        switch (tf) {
            case BINARY:
                switch (idf) {
                    case BINARY:
                        return this.Binary_Binary;
                    case LOGARITHMIC_1:
                        return this.Binary_Logarithmic1;
                    case LOGARITHMIC_2:
                        return this.Binary_Logarithmic2;
                    case LOGARITHMIC_3:
                        return this.Binary_Logarithmic3;
                }
            case SIMPLE:
                switch (idf) {
                    case BINARY:
                        return this.Simple_Binary;
                    case LOGARITHMIC_1:
                        return this.Simple_Logarithmic1;
                    case LOGARITHMIC_2:
                        return this.Simple_Logarithmic2;
                    case LOGARITHMIC_3:
                        return this.Simple_Logarithmic3;
                }
            case LOGARITHMIC:
                switch (idf) {
                    case BINARY:
                        return this.Logarithmic_Binary;
                    case LOGARITHMIC_1:
                        return this.Logarithmic_Logarithmic1;
                    case LOGARITHMIC_2:
                        return this.Logarithmic_Logarithmic2;
                    case LOGARITHMIC_3:
                        return this.Logarithmic_Logarithmic3;
                }
            default:
                return 1;
        }
    }

    public String getDocId() {
        return docId;
    }

    //testing

    @Override
    public String toString() {
        return "DocumentNorm{" + "docId=" + docId + ", Binary_Binary=" + Binary_Binary + ", Binary_Logarithmic1=" + Binary_Logarithmic1 + ", Binary_Logarithmic2=" + Binary_Logarithmic2 + ", Binary_Logarithmic3=" + Binary_Logarithmic3 + ", Simple_Binary=" + Simple_Binary + ", Simple_Logarithmic1=" + Simple_Logarithmic1 + ", Simple_Logarithmic2=" + Simple_Logarithmic2 + ", Simple_Logarithmic3=" + Simple_Logarithmic3 + ", Logarithmic_Binary=" + Logarithmic_Binary + ", Logarithmic_Logarithmic1=" + Logarithmic_Logarithmic1 + ", Logarithmic_Logarithmic2=" + Logarithmic_Logarithmic2 + ", Logarithmic_Logarithmic3=" + Logarithmic_Logarithmic3 + '}';
    }
   
}
