/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package invertedIndex;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

import org.apache.commons.io.FileUtils;

public class FileManager {

    private File currentDir;
    private static String indexName = "index.data";
    private static File library = new File("data");

    public FileManager() {
        currentDir = null;
    }

    public Boolean createDir(String Name, InvertedIndex index){
        File tempDir = new File(library, Name);
        if (tempDir.exists()) {
            File[] files = tempDir.listFiles();
            for (int i = 0; i < files.length; i++) {
                files[i].delete();
            }
        } else {
            tempDir.mkdirs();
        }
        Boolean indexSaved = this.saveIndex(tempDir, index);
        return (indexSaved);
    }

    public void openDir(String Name) {
        currentDir = new File(library, Name);
    }

    public void closeDir() {
        currentDir = null;
    }

    private Boolean saveIndex(File file, InvertedIndex index) {
        File tempIndexFile = new File(file, indexName);
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(tempIndexFile)));
            out.writeObject(index);
            out.close();
        } catch (Exception ex) {
            System.err.println(ex);
            return false;
        }
        return true;
    }

    public Boolean saveIndex(InvertedIndex index) {
        return this.saveIndex(currentDir, index);
    }

    public InvertedIndex loadIndex() {
        File tempIndexFile = new File(currentDir, indexName);
        InvertedIndex index = null;
        ObjectInputStream in = null;
        try {
            in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(tempIndexFile)));
            index = (InvertedIndex) in.readObject();
            in.close();
        } catch (Exception ex) {
            System.err.println(ex);
            return null;
        }
        return index;
    }

    public Boolean fileExistsInCollection(String filename) {
        File file = new File(currentDir, filename);
        return file.exists();
    }

    public Boolean copyDocument(File file) {
        try {
            FileUtils.copyFileToDirectory(file, currentDir);
            return true;
        } catch (Exception ex) {
            System.err.println(ex);
            return false;
        }
    }

    public Boolean deleteDocument(String filename) {
        File tempFile = new File(currentDir, filename);
        return tempFile.delete();
    }

    public Scanner openfile(String docID) {
        Scanner in = null;
        File inputFile = new File(currentDir, docID);
        try {
            in = new Scanner(new BufferedReader(new FileReader(inputFile)));
            //FIXME check if is correct
            in.useDelimiter("[[^\\w\\d]\\r\\n ]+");
        } catch (FileNotFoundException e) {
            System.err.println(e);
        }
        return in;
    }
}