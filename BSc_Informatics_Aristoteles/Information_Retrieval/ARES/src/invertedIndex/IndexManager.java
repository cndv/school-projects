/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package invertedIndex;

import java.util.ArrayList;
import java.io.File;
import java.util.Scanner;
import vectorModel.IDF_Enum;
import vectorModel.TF_Enum;

public class IndexManager {

    private Boolean isCollectionOpened;
    private InvertedIndex index;
    private FileManager fileMgr;

    public IndexManager() {
        this.fileMgr = new FileManager();
        this.isCollectionOpened = false;
    }

    public void createEmptyCollection(String Name) {
        InvertedIndex newIndex = new InvertedIndex();
        this.fileMgr.createDir(Name, newIndex);
    }

    public Boolean openCollection(String Name) {
        if (this.isCollectionOpened) {
            return false;
        }
        this.fileMgr.openDir(Name);
        this.index = this.fileMgr.loadIndex();
        if (this.index == null) {
            this.fileMgr.closeDir();
            return false;
        }
        this.isCollectionOpened = true;
        return true;
    }

    public void closeCollection() {
        this.fileMgr.saveIndex(index);
        this.fileMgr.closeDir();
        this.isCollectionOpened = false;
        this.index = null;
    }

    public int getNumberOfDocs(String term) {
        if (this.index.termExistsInDictionary(term)) {
            return this.index.getListSize(term);
        } else {
            return 0;
        }
    }

    public int getNumberOfAllDocs() {
        return this.index.getNumberOfAllDocs();
    }

    public ArrayList<DocumentHits> getReferenceListCopy(String term) {
        if (this.index.termExistsInDictionary(term)) {
            return new ArrayList<DocumentHits>(this.index.getList(term));
        } else {
            return null;
        }
    }

    public ArrayList<String> getDocumentListCopy(String term) {
        if (this.index.termExistsInDictionary(term)) {
            ArrayList<DocumentHits> hitList = this.getReferenceListCopy(term);
            ArrayList<String> docList = new ArrayList<String>();
            for (int i = 0; i < hitList.size(); i++) {
                docList.add(hitList.get(i).getDocumentID());
            }
            return docList;
        }else{
            return null;
        }
    }
    
    public ArrayList<String> getDocumentListCopy() {
        return this.index.getDocList();
    }

    public void insert(File file) {
        if (file.isDirectory()) {
            this.insertDirectory(file);
        } else if (file.isFile()) {
            this.insertDocument(file);
        }
        this.index.finishAdding();
    }

    private void insertDocument(File file) {
        String docID = file.getName();
        if (this.fileMgr.fileExistsInCollection(docID)) {
            System.err.println("File Exists in Collection");
        } else {
            this.fileMgr.copyDocument(file);
            Scanner openedDoc = this.fileMgr.openfile(docID);
            while (openedDoc.hasNext()) {
                this.index.insertWord(openedDoc.next().toLowerCase(), docID);
            }
            openedDoc.close();
        }
    }

    private void insertDirectory(File path) {
        File[] filelist = path.listFiles();
        for (int i = 0; i < filelist.length; i++) {
            if (filelist[i].isFile() && !filelist[i].isHidden()) {
                this.insertDocument(filelist[i]);
            }
        }
    }

    public double getNorm(String docID, TF_Enum tf, IDF_Enum idf) {
        DocumentNorm temp = this.index.getAllNorm(docID);
        return temp.getNorm(tf, idf);
    }

    public void delete(String filename) {
        Scanner openedDoc = this.fileMgr.openfile(filename);
        while (openedDoc.hasNext()) {
            this.index.deleteWord(openedDoc.next().toLowerCase(), filename);
        }
        openedDoc.close();
        this.index.finishDeleting();
        this.fileMgr.deleteDocument(filename);
    }

    public void delete(String[] filelist) {
        for (int i = 0; i < filelist.length; i++) {
            this.delete(filelist[i]);
        }
    }
    //Testing

    public void printIndex() {
        this.index.print();
    }
}
