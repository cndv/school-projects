/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package invertedIndex;
	
import java.util.ArrayList;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class InvertedIndex implements Serializable {

    private HashMap<String, ArrayList<DocumentHits>> Dictionary; //term, docList
    private HashMap<String, DocumentNorm> Norms;    //DocId, docNorm
    private HashMap<String, Integer> numberOfUniqueTerms; //DocId , numberOfTerms 

    public InvertedIndex() {
        this.Dictionary = new HashMap<String, ArrayList<DocumentHits>>();
        this.Norms = new HashMap<String, DocumentNorm>();
        this.numberOfUniqueTerms = new HashMap<String, Integer>();
    }

    public int getListSize(String term) {
        return this.getList(term).size();
    }

    public void insertWord(String term, String doc) {
        ArrayList<DocumentHits> tempList;
        if (this.Dictionary.containsKey(term)) {
            tempList = this.getList(term);
        } else {
            tempList = new ArrayList<DocumentHits>();
            this.Dictionary.put(term, tempList);
        }
        tempList.add(new DocumentHits(doc));

        Integer i = this.numberOfUniqueTerms.get(doc);
        if (i == null) {
            this.numberOfUniqueTerms.put(doc, 1);
        } else {
            this.numberOfUniqueTerms.put(doc, i + 1);
        }

    }

    public void deleteWord(String term, String doc) {
        DocumentHits tempDoc = new DocumentHits(doc);
        ArrayList<DocumentHits> tempList = this.getList(term);
        if (tempList != null) {
            int pos = Collections.binarySearch(tempList, tempDoc);
            if (pos >= 0) {
                tempList.remove(pos);
                if (tempList.isEmpty()) {
                    this.Dictionary.remove(term);
                }
                int n = this.numberOfUniqueTerms.get(doc);
                if (n == 1) {
                    this.numberOfUniqueTerms.remove(doc);
                } else {
                    this.numberOfUniqueTerms.put(doc, n - 1);
                }
            }
        }

    }

    public void finishAdding() {
        for (ArrayList<DocumentHits> list : this.Dictionary.values()) {
            Collections.sort(list);
            int i = 0;
            while (i < list.size() - 1) {
                if (list.get(i).getDocumentID().equals(list.get(i + 1).getDocumentID())) {
                    list.get(i).increaseNumberOfHits();
                    list.remove(i + 1);
                    String id = list.get(i).getDocumentID();
                    this.numberOfUniqueTerms.put(id, this.numberOfUniqueTerms.get(id) - 1);
                } else {
                    i++;
                }
            }
        }
        this.calculateNorms();
    }

    public void finishDeleting() {
        this.calculateNorms();
    }

    private void calculateNorms() {
        this.Norms.clear();
        int numberOfAlldocs = this.getNumberOfAllDocs();
        for (ArrayList<DocumentHits> list : this.Dictionary.values()) {
            int numberOfDocsContainTerm = list.size();
            for (int i = 0; i < list.size(); i++) {
                String docID = list.get(i).getDocumentID();
                int tf = list.get(i).getNumberOfHits();
                DocumentNorm tempNorm = this.Norms.get(docID);
                if (tempNorm == null) {
                    tempNorm = new DocumentNorm(docID);
                    this.Norms.put(docID, tempNorm);
                }
                tempNorm.add(numberOfDocsContainTerm, tf, numberOfAlldocs);
            }
        }

        for (DocumentNorm norm : this.Norms.values()) {
            norm.finish();
        }
    }

    public ArrayList<DocumentHits> getList(String term) {
        return this.Dictionary.get(term);
    }

    public ArrayList<String> getDocList() {
        ArrayList<String> docList = new ArrayList<String>(this.Norms.keySet());
        return docList;
    }

    public int getNumberOfAllDocs() {
        return this.numberOfUniqueTerms.size();
    }

    public Boolean termExistsInDictionary(String term) {
        return this.Dictionary.containsKey(term);
    }

    public DocumentNorm getAllNorm(String docID) {
        return this.Norms.get(docID);
    }
    //Testing

    public void print() {
        Iterator it = Dictionary.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            System.out.println(pairs.getKey() + " = " + pairs.getValue());
        }

        for (DocumentNorm norm : this.Norms.values()) {
            System.out.println(norm.toString());
        }
    }
}
