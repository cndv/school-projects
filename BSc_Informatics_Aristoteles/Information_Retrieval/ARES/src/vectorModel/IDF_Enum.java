/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package vectorModel;

public enum IDF_Enum {

    BINARY, //Default
    LOGARITHMIC_1,
    LOGARITHMIC_2,
    LOGARITHMIC_3;

    public double getIDF(double all, double n) {
        double temp;
        switch (this) {
            case LOGARITHMIC_1:
                temp = Math.log(all / n);
                break;
            case LOGARITHMIC_2:
                temp = Math.log(1 + all / n);
                break;
            case LOGARITHMIC_3:
                temp = Math.log(all / n) / Math.log(all);
            case BINARY:
            default: //  BINARY
                temp = 1;

        }
        return temp;
    }
}
