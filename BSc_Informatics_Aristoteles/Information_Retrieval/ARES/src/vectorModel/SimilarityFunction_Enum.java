/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package vectorModel;

public enum SimilarityFunction_Enum {

    INNER_PRODUCT,
    COSINE,
    DICE,
    JACCARD,
    OVERLAPPING;

    public double getSimilarity(double queryNorm, double docNorm, double sum) {
        double rank;
        switch (this) {
            case COSINE:
                rank = sum / (queryNorm * docNorm);
                break;
            case DICE:
                rank = 2 * sum / (Math.pow(queryNorm, 2) * Math.pow(docNorm, 2));
                break;
            case JACCARD:
                rank = sum / (Math.pow(queryNorm, 2) + Math.pow(docNorm, 2) - sum);
                break;
            case OVERLAPPING:
                rank = sum / Math.min(Math.pow(queryNorm, 2), Math.pow(docNorm, 2));
                break;
            case INNER_PRODUCT:    //default
            default:
                rank = sum;

        }
        return rank;
    }
}
