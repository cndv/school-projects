/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package vectorModel;

public enum TF_Enum {

    BINARY, //Default
    SIMPLE,
    LOGARITHMIC;

    public double getTF(double tf) {
        double temp;
        switch (this) {
            case SIMPLE:
                temp = tf;
                break;
            case LOGARITHMIC:
                temp = 1 + Math.log(tf);
                break;    
            case BINARY:
            default: //   BINARY
                temp = 1;
        }
        return temp;
    }
}
