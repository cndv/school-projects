/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package vectorModel;

import invertedIndex.DocumentHits;
import invertedIndex.IndexManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author zisis
 */
public class VectorModel {

    private int k;
    private double threshold;
    private TF_Enum tf;
    private IDF_Enum idf;
    private SimilarityFunction_Enum sim;
    private boolean top_k;
    private IndexManager imgr;

    public VectorModel() {
        this.tf = TF_Enum.BINARY;
        this.idf = IDF_Enum.BINARY;
        this.sim = SimilarityFunction_Enum.COSINE;
        this.k = 10;
        this.threshold = 0;
        this.top_k = true;
    }

    public VectorModel(IndexManager imgr) {
        this();
        this.imgr = imgr;
    }

    /**
     *
     * @param terms
     * @return relevant documentRank sorted
     * 
     * ilopoiisi tou algorithmou euresi top-k me katalogo
     */
    public ArrayList<documentRank> runQuery(ArrayList<String> terms) {
        HashMap<String, Double> Accumulators = new HashMap<String, Double>(); //docID, TF
        double queryNorm = 0;
        int all = imgr.getNumberOfAllDocs();

        for (int i = 0; i < terms.size(); i++) {
            String term = terms.get(i);
            int numberOfDocs = imgr.getNumberOfDocs(term);

            double queryTF = this.tf.getTF(1);
            double tempIDF = this.idf.getIDF(all, numberOfDocs);
            queryNorm += Math.pow(queryTF * tempIDF, 2);

            ArrayList<DocumentHits> docs = imgr.getReferenceListCopy(term);
            for (int j = 0; j < numberOfDocs; j++) {
                DocumentHits temp = docs.get(j);
                String id = temp.getDocumentID();
                int hits = temp.getNumberOfHits();
                double tempTF = this.tf.getTF(hits);
                double ac = 0;
                if (Accumulators.containsKey(id)) {
                    ac = Accumulators.get(id);
                }
                Accumulators.put(id, ac + tempIDF * tempTF);
            }
        }
        queryNorm = Math.sqrt(queryNorm);

        ArrayList<documentRank> relevance = new ArrayList<documentRank>();
        for (Map.Entry<String, Double> entry : Accumulators.entrySet()) {
            String key = entry.getKey();
            double sum = entry.getValue();
            double docNorm = this.imgr.getNorm(key, this.tf, this.idf);
            double rank = this.sim.getSimilarity(queryNorm, docNorm, sum);
            relevance.add(new documentRank(key, rank));
        }
        
        Collections.sort(relevance);

        //epilogi twn top-k i threshold
        int pos;
        if (this.top_k) {
            pos = this.k;
        } else {
            pos = Math.abs(Collections.binarySearch(relevance, new documentRank(null, this.threshold)) + 1);
        }
        if (pos < relevance.size()) {
            relevance.subList(pos, relevance.size()).clear();
        }
        return relevance;
    }

    public IDF_Enum getIdf() {
        return idf;
    }

    public void setIdf(IDF_Enum idf) {
        this.idf = idf;
    }

    public int getK() {
        return k;
    }

    public void setK(int k) {
        this.k = k;
    }

    public TF_Enum getTf() {
        return tf;
    }

    public void setTf(TF_Enum tf) {
        this.tf = tf;
    }

    public double getThreshold() {
        return threshold;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public boolean isTop_k() {
        return top_k;
    }

    public void setTop_k(boolean top_k) {
        this.top_k = top_k;
    }

    public SimilarityFunction_Enum getSim() {
        return sim;
    }

    public void setSim(SimilarityFunction_Enum sim) {
        this.sim = sim;
    }
}
