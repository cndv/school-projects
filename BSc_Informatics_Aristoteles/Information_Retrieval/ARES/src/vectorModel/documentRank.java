/*
 * Karampaglis Zisis, AEM 1475 
 * Koniaris Iwannis, AEM 1486 
 * Xartomatzis Kwnstantinos, AEM 1553
 */
package vectorModel;

/**
 *
 * apli klasi pou krataei tin valmologia gia kathe doc
 */
public class documentRank implements Comparable<documentRank> {

    private String docID;
    private double rank;

    public documentRank(String docID, double rank) {
        this.docID = docID;
        this.rank = rank;
    }

    public String getDocID() {
        return docID;
    }

    public void setDocID(String docID) {
        this.docID = docID;
    }

    public double getRank() {
        return rank;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }

    public int compareTo(documentRank t) {
        return Double.compare(t.getRank(), this.rank);
    }

    //Ttesting
    @Override
    public String toString() {
        return "(" + this.docID + "," + this.rank + ")";
    }
}
