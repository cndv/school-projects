<?php
//require_once("../../includes/initialize.php");

define('GUSER', 'oxinai@gmail.com'); // GMail username
define('GPWD', 'O1+MbK@I'); // GMail password
	
function smtpmailer($to, $from, $from_name, $subject, $body) 
{ 
	global $error;
	$mail = new PHPMailer();  // create a new object
	$mail->IsSMTP(); // enable SMTP
	$mail->SMTPDebug = 0;  // debugging: 1 = errors and messages, 2 = messages only
	$mail->SMTPAuth = true;  // authentication enabled
	$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
	$mail->Host = 'smtp.gmail.com';
	$mail->Port = 465; 
	$mail->Username = GUSER;  
	$mail->Password = GPWD;           
	$mail->SetFrom($from, $from_name);
	$mail->Subject = $subject;
	$mail->Body = $body;
	$mail->AddAddress($to);
	if(!$mail->Send()) {
		$error = 'Mail error: '.$mail->ErrorInfo; 
		return false;
	} else {
		$error = 'Message sent!';
		return true;
	}
}


function strip_zeros_from_date( $marked_string="" ) {
  // first remove the marked zeros
  $no_zeros = str_replace('*0', '', $marked_string);
  // then remove any remaining marks
  $cleaned_string = str_replace('*', '', $no_zeros);
  return $cleaned_string;
}

/*function redirect_to( $location = NULL ) {
  if ($location != NULL) {
    header("Location: {$location}");
    exit;
  }
}
*/
function output_message($message="") {
  if (!empty($message)) { 
    return "<p class=\"message\">{$message}</p>";
  } else {
    return "";
  }
}

function __autoload($class_name) {
	$class_name = strtolower($class_name);
  $path = LIB_PATH.DS."{$class_name}.php";
  if(file_exists($path)) {
    require_once($path);
  } else {
		die("The file {$class_name}.php could not be found.");
	}
}

function include_layout_template($template="") {
	include(SITE_ROOT.DS.'public'.DS.'layouts'.DS.$template);
}

function log_action($action, $message="") {
	$logfile = SITE_ROOT.DS.'logs'.DS.'log.txt';
	$new = file_exists($logfile) ? false : true;
  if($handle = fopen($logfile, 'a')) { // append
    $timestamp = strftime("%Y-%m-%d %H:%M:%S", time());
		$content = "{$timestamp} | {$action}: {$message}\n";
    fwrite($handle, $content);
    fclose($handle);
    if($new) { chmod($logfile, 0755); }
  } else {
    echo "Could not open log file for writing.";
  }
}

function datetime_to_text($datetime="") {
  $unixdatetime = strtotime($datetime);
  return strftime("%B %d, %Y at %I:%M %p", $unixdatetime);
}

function mysql_prep( $value ) {
	$magic_quotes_active = get_magic_quotes_gpc();
	$new_enough_php = function_exists( "mysql_real_escape_string" ); // i.e. PHP >= v4.3.0
	if( $new_enough_php ) { // PHP v4.3.0 or higher
		// undo any magic quote effects so mysql_real_escape_string can do the work
		if( $magic_quotes_active ) { $value = stripslashes( $value ); }
		$value = mysql_real_escape_string( $value );
	} else { // before PHP v4.3.0
		// if magic quotes aren't already on then add slashes manually
		if( !$magic_quotes_active ) { $value = addslashes( $value ); }
		// if magic quotes are active, then the slashes already exist
	}
	return $value;
}
function redirect_to( $location = NULL ) {
  if ($location != NULL) {
    header("Location: {$location}");
    exit;
  }
}
/**************************************************************************************************************************************/
/**************************************************************************************************************************************/


function confirm_query($result_set) {
	if (!$result_set) {
		die("Database query failed: " . mysql_error());
	}
}
	
function get_all_subjects($public = true) {
	global $connection;
	$query = "SELECT * 
			FROM console_type ";
	if ($public) {
//		$query .= "WHERE visible = 1 ";
	}
	//$query .= "ORDER BY position ASC";
	$subject_set = mysql_query($query, $connection);
	confirm_query($subject_set);
	return $subject_set;
}
	
function get_pages_for_subject($subject_id, $public = true) {
	global $connection;
	$query = "SELECT * 
			FROM pages ";
	$query .= "WHERE subject_id = {$subject_id} ";
	if ($public) {
//		$query .= "AND visible = 1 ";
	}
	$query .= "ORDER BY position ASC";
	$page_set = mysql_query($query, $connection);
	confirm_query($page_set);
	return $page_set;
}
	
function get_subject_by_id($subject_id) {
	global $connection;
	$query = "SELECT * ";
	$query .= " FROM transaction INNER JOIN console_type ON transaction.ct_id = console_type.id ";
	//$query .= "WHERE id=" . $subject_id ." ";
	$query .= "LIMIT 1";
	$result_set = mysql_query($query, $connection);
	confirm_query($result_set);
	// REMEMBER:
	// if no rows are returned, fetch_array will return false
	if ($subject = mysql_fetch_array($result_set)) {
		return $subject;
	} else {
		return NULL;
	}
}

function get_page_by_id($page_id) {
	global $connection;
	$query = "SELECT * ";
	$query .= "FROM pages ";
	$query .= "WHERE id=" . $page_id ." ";
	$query .= "LIMIT 1";
	$result_set = mysql_query($query, $connection);
	confirm_query($result_set);
	// REMEMBER:
	// if no rows are returned, fetch_array will return false
	if ($page = mysql_fetch_array($result_set)) {
		return $page;
	} else {
		return NULL;
	}
}
	
function get_default_page($subject_id) {
	// Get all visible pages
	$page_set = get_pages_for_subject($subject_id, true);
	if ($first_page = mysql_fetch_array($page_set)) {
		return $first_page;
	} else {
		return NULL;
	}
}
	
function find_selected_page() {
	global $sel_subject;
	if (isset($_GET['subj'])) {
		$sel_subject = get_subject_by_id($_GET['subj']);
	} else {
		$sel_subject = NULL;
	}
}

function navigation($sel_subject, $sel_page, $public = false) {
	$output = "<ul class=\"subjects\">";
	$subject_set = get_all_subjects($public);
	while ($subject = mysql_fetch_array($subject_set)) {
		$output .= "<li";
		if ($subject["id"] == $sel_subject['id']) { $output .= " class=\"selected\""; }
		$output .= "><a href=\"edit_subject.php?subj=" . urlencode($subject["id"]) . 
			"\">{$subject["menu_name"]}</a></li>";
		$page_set = get_pages_for_subject($subject["id"], $public);
		$output .= "<ul class=\"pages\">";
		while ($page = mysql_fetch_array($page_set)) {
			$output .= "<li";
			if ($page["id"] == $sel_page['id']) { $output .= " class=\"selected\""; }
			$output .= "><a href=\"content.php?page=" . urlencode($page["id"]) .
				"\">{$page["menu_name"]}</a></li>";
		}
		$output .= "</ul>";
	}
	$output .= "</ul>";
	return $output;
}

function public_navigation($sel_subject, $public = true) 
{
	$output = "<ul class=\"subjects\">";
	$subject_set = get_all_subjects($public);
	while ($subject = mysql_fetch_array($subject_set)) 
	{
		$output .= "<li";
		if ($subject["id"] == $sel_subject['id']) 
			{
				 $output .= " class=\"selected\""; 
			 }
		$output .= "><a href=\"index.php?subj=" . urlencode($subject["id"]) . 
			"\">{$subject["name"]}</a></li>";
		/*if ($subject["id"] == $sel_subject['id']) 
		{	
			$page_set = get_pages_for_subject($subject["id"], $public);
			$output .= "<ul class=\"pages\">";
			while ($page = mysql_fetch_array($page_set)) 
			{
				$output .= "<li";
				if ($page["id"] == $sel_page['id']) 
					{
						 $output .= " class=\"selected\""; 
					}
				$output .= "><a href=\"index.php?page=" . urlencode($page["id"]) .
					"\">{$page["menu_name"]}</a></li>";
			}
			$output .= "</ul>";
		}*/
	}
	$output .= "</ul>";
	return $output;
}
function list_category_photos($sel_page)
{
	$photos = Tr2::find_all();
	foreach($photos as $photo): 
		if ($session->get_user_id() == $photo->user_id){
			
			echo $photo->image_path(); 
			
			echo $photo->t_description; 
			echo $photo->price; 
			echo $photo->photo_description; 
		
		}
	endforeach;
}
/**************************************************************************************************************************************/
/**************************************************************************************************************************************/

?>