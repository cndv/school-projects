<?php require_once("../../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php
	// must have an ID
  
	if(isset($_POST['submit'])) {
		$updated_photo                    = Tr2::find_by_id($_GET['id']);
		$updated_photo->title             = $_POST['title'];
		$updated_photo->t_description     = $_POST['t_description'];
		$updated_photo->price             = $_POST['price'];
					
		$updated_photo->user_id           = $session->get_user_id();	
					
		$updated_photo->ct_id             = $_POST['ct_id'];
		$updated_photo->action_id         = $_POST['action_id'];
		$updated_photo->photo_description = $_POST['photo_description'];
						
		$updated_photo->update();
						
		//redirect_to('index.php');
		

	}
	
  
  
  if(empty($_GET['id'])) {
  	$session->message("No photograph ID was provided.");
    redirect_to('index.php');
  }
  $photo = Tr2::find_by_id($_GET['id']);
	

  /*if($photo) {
    $session->message("The photo {$photo->filename} was deleted.");
    redirect_to('list_photos.php');
  } else {
    $session->message("The photo could not be deleted.");
    redirect_to('list_photos.php');
  }
  */
?>
<?php include_layout_template('admin_header.php'); ?>
<script type="text/javascript" src="../javascripts/dynamicform.js"></script>

<table id="structure">
<tr>
	<td id="navigation">
		<a href="../index.php">Return to Menu</a><br />
		<br />
	</td>
	
	<td id="page">




		<h2>Edit Photo</h2>

		<?php //echo output_message($message); ?>

		<form action="edit_photo.php?id=<?php echo $photo->id; ?>" enctype="multipart/form-data" method="POST">
	
			<p>Title: <input type="text" name="title" value="<?php echo $photo->title?>" /></p>
			<p>Photo Description: <input type="text" name="photo_description" value="<?php echo $photo->photo_description?>" /></p>

		    <p><label>Console type :
		 	<select name="ct_id" >
			     <option selected="selected" value=1>Xbox 360</option>	
			     <option value=2>PS3</option>
			     <option value=3>PS2</option>
			     <option value=4>PS</option>
			     <option value=5>PSP</option>
			     <option value=6>Nintendo Wii</option>
			     <option value=7>Nintendo DS</option>
			     <option value=8>Nintendo DSI</option>
			     <option value=9>Nintendo 3DS</option>
			     <option value=10>Nintendo 64</option>
			     <option value=11>Xbox</option>
			     <option value=12>Sega</option>
			     <option value=13>Pc</option>
			     <option value=14>Mac</option>
		     </select>
		 	</label></p>
		
		   <p><label>What do you want to do with this game?
 	
		<input type = "radio" name="action_id" value=4 onclick="toggleDecisions(0)" checked="checked">donate</input>
		<input type = "radio" name="action_id" value=1 onclick="toggleDecisions(0)">exchange</input>
		<input type = "radio" name="action_id" value=2 onclick="toggleDecisions(1)">sell</input>
		<input type = "radio" name="action_id" value=3 onclick="toggleDecisions(1)">buy</input>
	
	
		<div id="sellbuy" style="display:none;  margin-left:20px">
			<p>Price: <input type="text" name="price" value="<?php echo $photo->price?>" /></p>
		</div>
		</label>
		</p>
   
    
			<p>Transaction Description: <input type="text" name="t_description" value="<?php echo $photo->t_description?>" /></p>


			<input type="submit" name="submit" value="Save edited values" />
		</form>
		</tr>
  </td>
</table>  
<?php include_layout_template('admin_footer.php'); ?>

<?php if(isset($database)) { $database->close_connection(); } ?>
	
