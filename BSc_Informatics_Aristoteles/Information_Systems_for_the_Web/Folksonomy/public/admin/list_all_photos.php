<?php require_once("../../includes/initialize.php"); ?>
<?php
// Find all the photos
$photos = Tr2::find_all();
?>
<?php include_layout_template('admin_header.php'); ?>

<h2>Photographs</h2>

<?php echo output_message($message); ?>
<table class="bordered">
	<tr>
		<th>Image </th>
		<th>want to </th>
		<th>price </th>
		<th>photo description </th>
		<?php if ($session->is_logged_in()){?>
		<th>delete </th>
		<?php } ?>
		<th>username</th>
		
	</tr>
	<?php foreach($photos as $photo): ?>
		<tr>
			<td><img src="../<?php echo $photo->image_path(); ?>" width="100" /></td>
			
			<td><?php echo $photo->t_description; ?></td>
			<td><?php echo $photo->price; ?></td>
			<td><?php echo $photo->photo_description; ?></td>
			<?php if ($session->get_user_id() == $photo->user_id){?>
			<td><a href="delete_photo.php?id=<?php echo $photo->id; ?>">Delete</a></td>
			<?php }?>
			<td><?php echo $photo->user_id; ?></td>
		</tr>
	<?php 
		endforeach; ?>
</table>
<br />
<a href="tr_upload2.php">Upload a new photograph</a>

<?php include_layout_template('admin_footer.php'); ?>
