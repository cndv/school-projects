<?php require_once("../../includes/initialize.php"); ?>
<?php if (!$session->is_logged_in()) { redirect_to("login.php"); } ?>
<?php
// Find all the photos
$photos = Tr2::find_all();
?>
<?php include_layout_template('admin_header.php'); ?>
<table id="structure">
<tr>
	<td id="navigation">
		<a href="../index.php">Return to Menu</a><br />
		<br />
	</td>
	
	<td id="page">

	

		<h2>Photographs</h2>

		<?php echo output_message($message); ?>
		<table class="bordered">
			<tr>
				<th>Image </th>
				<th>want to </th>
				<th>price </th>
				<th>photo description </th>
				<th>edit</th>
				<th>delete </th>
			</tr>
			<?php foreach($photos as $photo): ?>
				<?php if ($session->get_user_id() == $photo->user_id){?>
				<tr>
					<td><img src="../<?php echo $photo->image_path(); ?>" width="100" /></td>
			
					<td><?php echo $photo->t_description; ?></td>
					<td><?php echo $photo->price; ?></td>
					<td><?php echo $photo->photo_description; ?></td>
					<td><a href="edit_photo.php?id=<?php echo $photo->id; ?>">Edit</a></td>
					<td><a href="delete_photo.php?id=<?php echo $photo->id; ?>">Delete</a></td>
				</tr>
			<?php }
				endforeach; ?>
		</table>
		<br />
	</td>
</tr>	
</table>
<a href="tr_upload2.php">Upload a new photograph</a>

<?php include_layout_template('footer.php'); ?>
