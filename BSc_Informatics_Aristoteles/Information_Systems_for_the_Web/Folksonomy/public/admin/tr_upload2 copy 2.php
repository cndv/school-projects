<?php
require_once('../../includes/initialize.php');
if (!$session->is_logged_in()) { redirect_to("login.php"); }
?>
	

<?php
$max_file_size = 1048576;   // expressed in bytes
//     10240 =  10 KB
//    102400 = 100 KB
//   1048576 =   1 MB
//  10485760 =  10 MB

if(isset($_POST['submit'])) {
	$photo = new Tr2();
	$photo->title = $_POST['title'];
	$photo->t_description = $_POST['t_description'];
	$photo->price = $_POST['price'];
	
	$photo->user_id = $session->get_user_id();	
	
	$photo->ct_id = $_POST['ct_id'];
	$photo->action_id = $_POST['action_id'];
	$photo->photo_description = $_POST['photo_description'];
//	$photo->photo_filename = $_POST['photo_filename'];				
	
	$photo->attach_file($_FILES['file_upload']);
	if($photo->save()) {
		// Success
		$session->message("Photograph uploaded successfully.");
		//redirect_to('list_photos.php');
	} else {
		// Failure
		$message = join("<br />", $photo->errors);
	}
}
	
?>

<?php include_layout_template('admin_header.php'); ?>
<html xmlns="http://www.w3.org/1999/xhtml">

	<script type="text/javascript" src="../javascripts/dynamicform.js"></script>
	<a href="../index.php">&laquo; Back</a><br />
	

<h2>Photo Upload</h2>

<?php echo output_message($message); ?>

<form action="tr_upload2.php" enctype="multipart/form-data" method="POST">

	
	<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_file_size; ?>" />
	
	<p><input type="file" name="file_upload" /></p>
	<p>title: <input type="text" name="title" value="" /></p>
	<p>photo_description: <input type="text" name="photo_description" value="" /></p>

    <p><label>Console type :
 	<select name="ct_id" >
	     <option selected="selected" value=1>Xbox 360</option>	
	     <option value=2>PS3</option>
	     <option value=3>PS2</option>
	     <option value=4>PS</option>
	     <option value=5>PSP</option>
	     <option value=6>Nintendo Wii</option>
	     <option value=7>Nintendo DS</option>
	     <option value=8>Nintendo DSI</option>
	     <option value=9>Nintendo 3DS</option>
	     <option value=10>Nintendo 64</option>
	     <option value=11>Xbox</option>
	     <option value=12>Sega</option>
	     <option value=13>Pc</option>
	     <option value=14>Mac</option>
     </select>
 	</label></p>
		
    <p><label>What do you want to do with this game?
 	
	<input type="radio" name="action_id" value=4 onclick="toggleDecisions(0)" checked="checked">donate</input>
	<input type="radio" name="action_id" value=1 onclick="toggleDecisions(0)" >exchange</input>
	<input type="radio" name="action_id" value=2 onclick="toggleDecisions(1)">sell</input>
	<input type="radio" name="action_id" value=3 onclick="toggleDecisions(1)">buy</input>
	
	
	<div id="sellbuy" style="display:none;  margin-left:20px">
		<p>price: <input type="text" name="price" value="" /></p>
	</div>
	</label>
	</p>
	
	<p>t_description: <input type="text" name="t_description" value="" /></p>
	
	
	
	<input type="submit" name="submit" value="Upload" />
</form>
  
</html>

<?php include_layout_template('footer.php'); ?>
		
