<?php
require_once('../../includes/initialize.php');
if (!$session->is_logged_in()) { redirect_to("login.php"); }
?>
	

<?php
$max_file_size = 1048576;   // expressed in bytes
//     10240 =  10 KB
//    102400 = 100 KB
//   1048576 =   1 MB
//  10485760 =  10 MB

if(isset($_POST['submit'])) {
	$photo = new Tr2();
	$photo->title = $_POST['title'];
	$photo->t_description = $_POST['t_description'];
	$photo->price = $_POST['price'];
	
	$photo->user_id = $session->get_user_id();	
	
	$photo->ct_id = $_POST['ct_id'];
	$photo->action_id = $_POST['action_id'];
	$photo->photo_description = $_POST['photo_description'];
//	$photo->photo_filename = $_POST['photo_filename'];				
	
	$photo->attach_file($_FILES['file_upload']);
	if($photo->save()) {
		// Success
		$session->message("Photograph uploaded successfully.");
		redirect_to('list_photos.php');
	} else {
		// Failure
		$message = join("<br />", $photo->errors);
	}
}
	
?>

<?php include_layout_template('admin_header.php'); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
	<script>
		window.onload = initialize;

		function initialize() 
		{
			var elem = document.getElementById("survey");
			elem.onsubmit = checkForm;
		}

		function checkForm() 
		{
			var elem = document.getElementById("question2");
			if (elem.value == null || elem.value.length == 0) {
				alert("Παρακαλώ συμπληρώστε την ερώτηση 2.");
				return false;
			} else
				return true;	
		}

		function toggleDecisions(x) 
		{
		  var elem = document.getElementById("sellbuy");
		  if (x == 1)
			  elem.style.display = "block";
			else
			  elem.style.display = "none";
		}
	</script>

<h2>Photo Upload</h2>

<?php echo output_message($message); ?>

<form action="tr_upload2.php" enctype="multipart/form-data" method="POST">

	
	<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_file_size; ?>" />
	
	<p><input type="file" name="file_upload" /></p>
	<p>title: <input type="text" name="title" value="" /></p>
	<p>photo_description: <input type="text" name="photo_description" value="" /></p>

    <p><label>Console type :
 	<select name="ct_id" >
	     <option selected="selected" value=1>Xbox 360</option>	
	     <option value=2>PS3</option>
	     <option value=3>PS2</option>
	     <option value=4>PS</option>
	     <option value=5>PSP</option>
	     <option value=6>Nintendo Wii</option>
	     <option value=7>Nintendo DS</option>
	     <option value=8>Nintendo DSI</option>
	     <option value=9>Nintendo 3DS</option>
	     <option value=10>Nintendo 64</option>
	     <option value=11>Xbox</option>
	     <option value=12>Sega</option>
	     <option value=13>Pc</option>
	     <option value=14>Mac</option>
     </select>
 	</label></p>
		
    <p><label>What do you want to do with this game?
 	<select name="action_id" >
	     <option selected="selected" value=1 onselect="toggleDecisions(0)">swap</option>	
	     <option value=2 onselected="toggleDecisions(1)">sell</option>
	     <option value=3 onselect="toggleDecisions(1)">buy</option>
	     <option value=4 oncselect="toggleDecisions(0)">donate</option>
     </select>
 	</label></p>
    
	<p>t_description: <input type="text" name="t_description" value="" /></p>
	
	<div id="sellbuy" style="display:none;  margin-left:20px">
		<p>price: <input type="text" name="price" value="" /></p>
	</div>
	
	<input type="submit" name="submit" value="Upload" />
</form>
  
</html>

<?php include_layout_template('admin_footer.php'); ?>
		
