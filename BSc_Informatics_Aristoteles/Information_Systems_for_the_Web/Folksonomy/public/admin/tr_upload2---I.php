<?php
require_once('../../includes/initialize.php');
if (!$session->is_logged_in()) { redirect_to("login.php"); }
?>
<?php
$max_file_size = 1048576;   // expressed in bytes
//     10240 =  10 KB
//    102400 = 100 KB
//   1048576 =   1 MB
//  10485760 =  10 MB

if(isset($_POST['submit'])) {
	$photo = new Tr2();
	$photo->title = $_POST['title'];
	$photo->t_description = $_POST['t_description'];
	$photo->price = $_POST['price'];
	
	$photo->user_id = $session->get_user_id;	
	
	$photo->ct_id = $_POST['ct_id'];
	$photo->action_id = $_POST['action_id'];
	$photo->photo_description = $_POST['photo_description'];
//	$photo->photo_filename = $_POST['photo_filename'];				
	
	$photo->attach_file($_FILES['file_upload']);
	if($photo->save()) {
		// Success
		$session->message("Photograph uploaded successfully.");
		redirect_to('list_photos.php');
	} else {
		// Failure
		$message = join("<br />", $photo->errors);
	}
}
	
?>

<?php include_layout_template('admin_header.php'); ?>

<h2>Photo Upload</h2>

<?php echo output_message($message); ?>

<form action="tr_upload2.php" enctype="multipart/form-data" method="POST">
	
	<input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $max_file_size; ?>" />
	
	<p><input type="file" name="file_upload" /></p>
	<p>title: <input type="text" name="title" value="" /></p>
	<p>photo_description: <input type="text" name="photo_description" value="" /></p>

	<p>t_description: <input type="text" name="t_description" value="" /></p>
	
    <p><label>What do you want to do with this game?
 	<select name="action_id" >
	     <option selected="selected" value="1">swap</option>	
	     <option value="2">sell</option>
	     <option value="3">buy</option>
	     <option value="4">donate</option>
     </select>
 	</label></p>
	

	<p>price: <input type="text" name="price" value="" /></p>

	<p>ct_id: <input type="text" name="ct_id" value="" /></p>

	<p>action_id: <input type="text" name="action_id" value="" /></p>
	
	<input type="submit" name="submit" value="Upload" />
</form>
  

<?php include_layout_template('admin_footer.php'); ?>
		
