<?php require_once("../includes/initialize.php"); ?>

<?php find_selected_page(); ?>

<?php include_layout_template('header.php'); ?>			
	<table id="structure">
		<tr>
			<td id="navigation">
				<a href="new_user.php?page=<?php echo urlencode($sel_page['id']); ?>">registration</a>
				</br>
				<a href="admin/login.php?page=<?php?>">login</a>	
				</br>
				<a href="admin/tr_upload2.php?page=<?php?>">add an item</a>	
				</br>
				<a href="admin/list_all_photos.php?page=<?php?>">list photos</a>	
				
								
								
				<?php echo public_navigation($sel_subject, $sel_page); ?>
			</td>				
			<td id="page">
				<?php if ($sel_page) { ?>
					<h2><?php echo htmlentities($sel_page['menu_name']); ?></h2>
					<div class="page-content">
						<?php echo strip_tags(nl2br($sel_page['content']), "<b><br><p><a>"); ?>
					</div>
				<?php } else { ?>
					<h2>Welcome to Widget Corp</h2>
				<?php } ?>
			</td>			
		</tr>
	</table>
<?php include_layout_template('footer.php'); ?>
