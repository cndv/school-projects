<?php require_once("../includes/initialize.php"); ?>

<?php 	
		find_selected_page(); 
		$photos = Tr2::find_all();
?>


<?php include_layout_template('header.php'); ?>			
	<table id="structure">
		<tr>
			<td id="navigation">
				<?php if (!$session->is_logged_in()){?>	
				
				<a href="new_user.php">Sign Up</a>
				</br>
				<a href="admin/login.php">Login</a>	
				</br>
				<?php }
				else {
				?>
				<a href="admin/logout.php">Logout</a>	
				</br>
				<?php }?>
				<a href="admin/tr_upload2.php">Add an Item</a>	
				</br>
				<a href="admin/list_photos.php">Show my Photos</a>	
				
								
				<p><b>Categories</b></p>	
				<?php echo public_navigation($sel_page); ?>
				
				
			</td>				
			<td id="page">
				
				<?php //if ($sel_page) { ?>
					<div class="page-content">
						<h2>Welcome to Gamekonomy</h2>
						<table class="bordered">
							<?php if ($_GET['subj']){?>	
							<tr>
								<th>Image </th>
								<th>Photo description </th>
								<th>Transaction type</th>
								<th>Transaction details </th>
								<th>Price</th>
								
								
								
							</tr>
							<?php }
							$photos = Tr2::find_all();
							foreach($photos as $photo): ?>
							<?php if ($photo->ct_id == $_GET['subj']){?>	
								<tr>
									<td>
							  			<a href="photo.php?id=<?php echo $photo->id; ?>">
										<img src="<?php echo $photo->image_path(); ?>" width="100" />
								  		</a>
									</td>	
									<td><?php echo $photo->photo_description; ?></td>									
									<td>
										<?php $action_type = Action::find_by_id($photo->action_id);
												echo $action_type->name;
										?>
									</td>		
									<td><?php echo $photo->t_description; ?></td>
									<td><?php echo $photo->price; ?></td>


									
								</tr>
							<?php 
									}
							endforeach; ?>
						</table>
						
					</div>
			</td>			
		</tr>
	</table>
<?php include_layout_template('footer.php'); ?>
