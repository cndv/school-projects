	window.onload = initialize;

	function initialize() 
	{
		var elem = document.getElementById("survey");
		elem.onsubmit = checkForm;
	}

	function checkForm() 
	{
		var elem = document.getElementById("question2");
		if (elem.value == null || elem.value.length == 0) {
			alert("Παρακαλώ συμπληρώστε την ερώτηση 2.");
			return false;
		} else
			return true;	
	}

	function toggleDecisions(x) 
	{
	  var elem = document.getElementById("sellbuy");
	  if (x == 1)
		  elem.style.display = "block";
		else
		  elem.style.display = "none";
	}
