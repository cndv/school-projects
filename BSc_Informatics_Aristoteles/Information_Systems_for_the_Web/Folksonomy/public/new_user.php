<?php require_once("../includes/initialize.php"); ?>

<?php
	//include_once("includes/form_functions.php");
	
	// START FORM PROCESSING
	if (isset($_POST['submit'])) 
	{ // Form has been submitted.
		$errors = array();

		// perform validations on the form data
		$required_fields = array('name','surname','username', 'password','email');
		$errors = array_merge($errors, check_required_fields($required_fields, $_POST));

		$fields_with_lengths = array('name' => 30,'surname' => 30, 'username' => 30, 'password' => 30,'email' => 40);
		$errors = array_merge($errors, check_max_field_lengths($fields_with_lengths, $_POST));

		$username = trim(mysql_prep($_POST['username']));
		$password = trim(mysql_prep($_POST['password']));
		//$hashed_password = sha1($password);
		$name = trim(mysql_prep($_POST['name']));
		$surname = trim(mysql_prep($_POST['surname']));
		$email = trim(mysql_prep($_POST['email']));

		if ( empty($errors) ) 
		{
			
			 $user = new User();
	   	 $user->username = $username;
	   	 $user->password = $password;
	   	 $user->name = $name;
	   	 $user->surname = $surname;
		 $user->email = $email;
	   	 $user->create();
			
			/*$query = "INSERT INTO users (
							username, password, name, surname, email
						) VALUES (
							'{$username}', '{$password}', '{$name}', '{$surname}', '{$email}'
						)";
			$result = mysql_query($query, $connection);
			if ($result) {
				$message = "The user was successfully created.";
			} else {
				$message = "The user could not be created.";
				$message .= "<br />" . mysql_error();
			}*/
		} 
		else 
		{
			if (count($errors) == 1) 
			{
				$message = "There was 1 error in the form.";
			} else 
			{
				$message = "There were " . count($errors) . " errors in the form.";
			}
		}
	} else 
	{ // Form has not been submitted.
		$username = "";
		$password = "";
		$name = "";
		$surname = "";
		$email = "";
	}
	//redirect_to("index.php");
?>
<?php include_layout_template('header.php'); ?>
<table id="structure">
	<tr>
		<td id="navigation">
			<a href="index.php">Return to Menu</a><br />
			<br />
		</td>
		<td id="page">
			<h2>Create New User</h2>
			<?php if (!empty($message)) {echo "<p class=\"message\">" . $message . "</p>";} ?>
			<?php if (!empty($errors)) { display_errors($errors); } ?>
			<form action="new_user.php" method="post">
			<table>
				<tr>
					<td>Name:</td>
					<td><input type="text" name="name" maxlength="30" value="<?php echo htmlentities($name); ?>" /></td>
				</tr>
				<tr>
					<td>Surname:</td>
					<td><input type="text" name="surname" maxlength="50" value="<?php echo htmlentities($surname); ?>" /></td>
				</tr>
				<tr>
					<td>Email:</td>
					<td><input type="text" name="email" maxlength="50" value="<?php echo htmlentities($email); ?>" /></td>
				</tr>
				
				<tr>
					<td>Username:</td>
					<td><input type="text" name="username" maxlength="30" value="<?php echo htmlentities($username); ?>" /></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><input type="password" name="password" maxlength="30" value="<?php echo htmlentities($password); ?>" /></td>
				</tr>
				<tr>
					
					<td colspan="2"><input type="submit" name="submit" value="Create user" /></td>
				</tr>
			</table>
			</form>
		</td>
	</tr>
</table>
<?php include_layout_template('footer.php'); ?>
