<?php require_once("../includes/initialize.php"); ?>
<?php// if (!$session->is_logged_in()) { redirect_to("login.php"); } ?>

<?php
if(empty($_GET['id'])) {
	$session->message("No photograph ID was provided.");
	redirect_to('index.php');
}  
$photo = Tr2::find_by_id($_GET['id']);
if(!$photo) {
	$session->message("The photo could not be located.");
	redirect_to('index.php');
}

if(isset($_POST['submit'])) {
	// $author = trim($_POST['author']);
	$body = trim($_POST['body']);
	 
	$new_comment = Comment::make($photo->id, $session->get_user_id(), $body);
	if($new_comment && $new_comment->save()) {
		// comment saved
		// No message needed; seeing the comment is proof enough.
			
		// Important!  You could just let the page render from here. 
		// But then if the page is reloaded, the form will try 
		// to resubmit the comment. So redirect instead:
		//    redirect_to("photo.php?id={$photo->id}");
		$user_id = "";
		$body = "";
	
	} else {
		// Failed
		$message = "There was an error that prevented the comment from being saved.";
	}
} else {
	$user_id = "";
	$body = "";
}
	
$comments = $photo->comments();
	
?>



<?php include_layout_template('header.php'); ?>

<table id="structure">
<tr>
	<td id="navigation">
		<a href="index.php">Return to Menu</a><br />
		<br />
	</td>
	<td id="page">
	
	<br />

	<div style="margin-left: 20px;">
		<img src="<?php echo $photo->image_path(); ?>" width="750" />
	</div>
	<!--
	***********************************************************************************
	-->
	
	<div style="margin-left: 800px;">
		<?php if ($session->is_logged_in() && !($session->user_id == $photo->user_id)) {  ?>
		<a href="sendemail2.php?id=<?php echo $photo->user_id; ?>"> email user 
		</a>
		<?php  		
			$user = User::find_by_id($photo->user_id);
			echo $user->name;
		}else {
			$user3 = User::find_by_id($photo->user_id);
			echo $user3->name."'s picture."; 
		}
		
		?>
	
	</div>

	<!--
	***********************************************************************************
	-->


	<div id="comments">
		<?php foreach($comments as $comment): ?>
			<div class="comment" style="margin-bottom: 2em;">
				<div class="author">
					<?php 
					$user2 = User::find_by_id($photo->user_id);
					echo htmlentities($user2->name); ?> wrote:
				</div>
				<div class="body">
					<?php echo strip_tags($comment->body, '<strong><em><p>'); ?>
				</div>
				
			</div>
		<?php endforeach; ?>
		<?php if(empty($comments)) { echo "No Comments."; } ?>
	</div>
		
		<?php if ($session->is_logged_in()){?>
		<div id="comment-form">
			<h3>New Comment</h3>
			<?php echo output_message($message); ?>
			<form action="photo.php?id=<?php echo $photo->id; ?>" method="post">
				<table>
					<tr>
						
						
					</tr>
					<tr>
						<td>Your comment:</td>
						<td><textarea name="body" cols="40" rows="8"><?php echo $body; ?></textarea></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td><input type="submit" name="submit" value="Submit Comment" /></td>
					</tr>
			<?php }?>
				</table>
			</form>
		</div>
		</td>
</tr>
</table>
<?php include_layout_template('footer.php'); ?>
