#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>

void printStdoutStr(char *str)
{
	char *strWrite;
	int lengthStr;

	strWrite = (char *) malloc(200 * sizeof(char));
	strWrite=str;
	lengthStr = strlen(strWrite);
     	write(1,strWrite,lengthStr);	
}

void printStdoutInt(int i)
{
	int x;
	char str[10];

	x = sprintf(str,"%d",i);
	write(1,str,x);	
	write(1,"\n\n",2);
}

/* compare the integers */
int comp(const void *i, const void *j)
{	
	return *(int *)i - *(int *)j;
}

/* compare the integers */
int comp_1(const void *i, const void *j)
{
	return *(int *)j-*(int *)i;
}

int gen_rand(void)
{
	int n;
	n=rand()%100;  
   	return(n);
}

int main(void)
{
	int numbers[10];
	int i;
	pid_t child_1;
	pid_t child_2;

	for(i=0; i<10; i++)
	{
		numbers[i]=gen_rand();
	}

	child_1 = fork();	/* first child born */

	if (child_1 < 0) 
	{	
		printStdoutStr("Error child_1\n");
	}
	else if (child_1 == 0) 
	{

		/* child id  */
		printStdoutStr("fork_id child_1 : ");
		printStdoutInt(getpid());
		/*sort items*/
		qsort(numbers,10,sizeof(int),comp) ;

		for (i=0; i<10; i++)
		{
			printStdoutInt(numbers[i]);
		}		
	}
	else
	{
		child_2 = fork(); /* second child born */
		if (child_2 < 0)
		{
			printStdoutStr("Error child_2\n");	
		}
		else if (child_2 == 0) 
		{
     			/* child id  */
			printStdoutStr("fork_id child_2 :");
			printStdoutInt(getpid());
 			/*sort items*/
			qsort(numbers,10,sizeof(int),comp_1) ;
			/*use of write() function*/
			for (i=0; i<10; i++)
			{
				printStdoutInt(numbers[i]);
			}
		}
		else/*parent waits for the children to finish*/
		{
			pid_t child_pid;
			child_pid = waitpid(child_1,(int *) 0,WUNTRACED);
			printStdoutStr("child 1 has finished : PID = ");
			printStdoutInt(child_pid);

			child_pid = waitpid(child_2,(int *) 0,WUNTRACED);

			printStdoutStr("child 2 has finished : PID = ");
			printStdoutInt(child_pid);
		}
	}
	return 0;
}