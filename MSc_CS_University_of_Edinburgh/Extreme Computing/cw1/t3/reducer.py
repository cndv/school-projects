#!/usr/bin/python
#task3
import sys
lines_count = 0
words_count = 0
for line in sys.stdin:
    line = line.strip()
    l_count, w_count = line.split('\t')
    lines_count += int(l_count)
    words_count += int(w_count)
print '%s\t%s' % (lines_count, words_count)
