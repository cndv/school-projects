#!/usr/bin/python
#task4
import sys
oldKey = None
salesTotal = 0
for line in sys.stdin:
    data = line.strip().split("\t")
    thisKey, thisSale = data
    if oldKey and oldKey != thisKey:
        print oldKey, "\t", salesTotal
        oldKey = thisKey
        salesTotal = 0
    oldKey = thisKey
    salesTotal += float(thisSale)
if oldKey:
    print oldKey, "\t", salesTotal
