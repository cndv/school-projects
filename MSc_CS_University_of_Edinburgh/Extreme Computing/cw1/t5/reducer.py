#!/usr/bin/python
#task5
from operator import itemgetter
import sys
oldKey = None
countTotal = 0

for line in sys.stdin:
    data = line.strip().split("\t")

    thisKey, tempCount = data
    if oldKey and oldKey != thisKey:
        print oldKey, "\t", countTotal
        oldKey = thisKey
        countTotal = 0

    oldKey = thisKey
    countTotal += int(tempCount)

if oldKey:
    print oldKey, "\t", countTotal