#!/usr/bin/python
# task6
import sys

import sys
for line in sys.stdin:
    d = {}
    tokens = line.split()
    for t in range(0, len(tokens)-2):
        tok = tokens[t:t + 3]
        str_v = ' '.join(tok)
        d[str_v] = d[str_v] + 1 if str_v in d.keys() else 1
    for k, v in d.iteritems():
        print k, '\t', v