#!/usr/bin/python
# task6

import sys
import heapq

oldKey = None
countTotal = 0
h = []
results = []


def push_to_heap(key, value):
    if len(h) == 20:
        heapq.heappushpop(h, (value, key))
    else:
        heapq.heappush(h, (value, key))


def print_top_k():
    for i in range(0, len(h)):
        p = heapq.heappop(h)
        results.append(p)
    for i in reversed(results):
        print i[0], '\t', i[1]


for line in sys.stdin:

    data = line.strip().split("\t")

    thisKey, tempCount = data
    if oldKey and oldKey != thisKey:
        push_to_heap(oldKey, countTotal)
        oldKey = thisKey
        countTotal = 0

    oldKey = thisKey
    countTotal += int(tempCount)

if oldKey:
    push_to_heap(oldKey, countTotal)

print_top_k()
