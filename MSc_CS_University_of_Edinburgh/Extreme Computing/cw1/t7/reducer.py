#!/usr/bin/python
# task7
from collections import defaultdict
import sys
oldKey = None

def list_to_string(l):
    str_v = ' '.join(l)
    return str_v

d = defaultdict(list)
for line in sys.stdin:
    data = line.strip().split('\t')
    thisKey, oldRow, value = data

    key = thisKey[0]
    thisKey = list_to_string(thisKey)
    if oldKey and oldKey != key:
        print list_to_string(d[oldKey])
        oldKey = key

    oldKey = key
    d[oldKey].append(value)
if oldKey:
    print list_to_string(d[oldKey])







