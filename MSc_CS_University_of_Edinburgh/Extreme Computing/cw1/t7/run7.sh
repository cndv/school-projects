hpy -input /user/kc/data/input/matrixSmall.txt \
-output /user/kc/data/output \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-jobconf stream.num.map.output.key.fields=2 \
-jobconf mapred.text.key.partitioner.options=-k1,1 \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1n -k2n" \
-jobconf mapred.reduce.tasks=1






hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
 -input /user/s1467292/data/matrixLarge.txt \
 -output /user/s1467292/t7 \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-jobconf stream.num.map.output.key.fields=2 \
-jobconf mapred.text.key.partitioner.options=-k1,1 \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1n -k2n" \
-jobconf mapred.reduce.tasks=1







hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
 -input /user/s1467292/t8/task_8.out \
 -output /user/s1467292/t9 \
 -mapper mapper.py \
 -file mapper.py \
 -reducer reducer.py \
 -file reducer.py \
 -jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
 -jobconf mapred.map.tasks=10 \
 -jobconf mapred.reduce.tasks=1