#!/usr/bin/python
# task8
import sys

count_line = 0
for line in sys.stdin:
    tokens = line.split()
    line_length = len(tokens)
    if line_length == 3:
        print int(tokens[1]),'\t',tokens[2] # id name
    else:
        print int(tokens[2]),'\t',tokens[1],'\t',int(tokens[3]) # id course mark

