#!/usr/bin/python
# task8
from collections import defaultdict
import sys


def list_to_string(l):
    str_v = ' '.join(l)
    return str_v


def print_marks(s_name, out_marks):
    s = ""
    for k, v in out_marks.iteritems():
        s = s + '(' + str(k) + str(v) + ') '
    print s_name, '-->', s


oldKey = None
name = None
course = None
marks = defaultdict(dict)
names = defaultdict()
for line in sys.stdin:
    data = line.strip().split('\t')
    data_length = len(data)
    if data_length == 3:
        newKey, course, mark = data
    else:
        newKey, name = data

    if oldKey and oldKey != newKey:
        print_marks(names[oldKey], marks[oldKey])
        oldKey = newKey

    oldKey = newKey
    if course:
        marks[oldKey][course] = int(mark)
    if name:
        names[oldKey] = name

if oldKey:
    print_marks(names[oldKey], marks[oldKey])







