#!/usr/bin/python
# task9
import sys
for line in sys.stdin:
    line = line.translate(None, '->()')
    tokens = line.split()
    line_length = len(tokens)
    if line_length >= 9:
        s = 0
        for i in range(2,line_length,2):
            s += int(tokens[i])
        num_of_courses = (line_length-1)/2
        avg = s/num_of_courses
        print avg,'\t', tokens[0] # avg, name
