#!/bin/sh


hpy -input /user/kc/data/input/test9.txt \
-output /user/kc/data/output \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.reduce.tasks=1



## cluster

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
 -input /user/s1467292/data/webLarge.txt \
 -output /user/s1467292/t1 \
 -mapper mapper.py \
 -file mapper.py \
 -reducer reducer.py \
 -file reducer.py \
 -jobconf mapred.reduce.tasks=1


#git

git add .; git commit -m "task8 finished" ;git push -u origin master