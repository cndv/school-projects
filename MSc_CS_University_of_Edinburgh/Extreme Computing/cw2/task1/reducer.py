#!/usr/bin/python

'''
    Reducer:
        Input: term , list of files that belongs and its frequency in the corresponding file
        Output: term, the number of files that the term appeared, document ids followed by the frequency of that term inside the specific file
                e.g. emit (t: C : {(di,tf(t,d), ...}, C -> count of files t belongs to.
'''

import sys
from OrderedDict import OrderedDict

oldTerm = None
counter = 0
docIds = None
ftd = OrderedDict()
oldFileName = None
for line in sys.stdin:

    data = line.split('\t')

    if len(data) != 3:
        continue

    term, docID, freq = data

    if oldTerm and oldTerm != term:
        # new term has arrived, print the old one
        docIds = "{"
        for t in ftd:
            docIds += "(" + t + "," + str(ftd[t]) + ")"
        docIds += "}"
        print oldTerm, ":\t", len(ftd), ":", docIds
        oldTerm = term

        # an ordered dictionary is used to keep the order of the document identifiers sorted,
        # not to resort since they are sorted by shuffle and sort , but an ordinary dictionary is unordered
        ftd = OrderedDict()

        docIds = ""     # initialise document identifiers

    if docID in ftd:
        ftd[docID] += int(freq)
    else:
        ftd[docID] = int(freq)

    oldTerm = term

if oldTerm != None:
    # emit the final term
    docIds = "{"
    for t in ftd:
        docIds += "(" + t + "," + str(ftd[t]) + ")"
    docIds += "}"
    print oldTerm, ":\t", len(ftd), ":", docIds