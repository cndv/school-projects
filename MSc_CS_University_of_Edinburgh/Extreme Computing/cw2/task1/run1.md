# Run command for Task 1 with secondary sorting

hpy \
-input /user/kc/data/input/p1 \
-output /user/kc/task1 \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-jobconf stream.num.map.output.key.fields=2 \
-file OrderedDict.py \
-jobconf mapred.reduce.tasks=5 \
-jobconf mapred.map.tasks=5 \

# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/ex2/task1/ \
-output /user/s1467292/task_1.out \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-file OrderedDict.py \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-jobconf stream.num.map.output.key.fields=2 \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=5 \


# WRONG Run from hadoop the definitive guide

hpy \
-input /user/kc/input \
-output /user/kc/data/output \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf stream.num.map.output.key.fields=2 \
-jobconf mapred.text.key.partitioner.options=-k1,1 \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1n -k2nr" \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \



