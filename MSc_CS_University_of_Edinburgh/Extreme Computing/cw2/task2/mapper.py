#!/usr/bin/python

'''
    Mapper:
        Input: lines of words
        Output: emit term that belongs to terms.txt, name of file, term frequency
    References:
        [1]: http://stackoverflow.com/questions/4906977/python-environment-variables
'''

import os
import string
import sys

#get terms
terms = set()
for line in file('terms.txt'):
    t = line.strip()
    terms.add(t)

for line in sys.stdin:
    # print enviromental variable , to access filename
    filepath = os.environ.get("map_input_file")  # access the names of the files through the enviromental variable map_input_file [1]
    l = filepath.split('/')
    size = len(l)
    filename = l[size - 1]      # extract the name of the file (e.g. d1.txt)
    replace_punctuation = string.maketrans(string.punctuation, ' ' * len(string.punctuation))  # remove punctuation
    document = line.translate(replace_punctuation).lower().split()  # make lowercase and split
    freq = {}
    for term in document:
        if term in terms:
            # for each term that belongs to the terms file, calculate its frequency
            if term in freq:
                freq[term] += 1
            else:
                freq[term] = 1

    for term in freq:
        print term, '\t', filename, '\t', freq[term]
