#!/usr/bin/python

'''
    Reducer:
        Input: term, document id, frequency of term on the specific document
        Output: emits t that belongs to terms.txt, and its tfidf score regarding the d1.txt document
'''

import sys
from collections import defaultdict
import math

oldTerm = None
counter = 0
docIds = None
tf = defaultdict(dict)
oldFileName = None
N = 17

tfidf = 0
scores = {}

for line in sys.stdin:
    data = line.strip().split('\t')

    if len(data) != 3:
        continue

    term, docID, freq = data

    term = term.strip()
    docID = docID.strip()

    # if term in terms:
    if docID in tf:
        tf[term][docID] += int(freq)
    else:
        tf[term][docID] = int(freq)

    d_id = 'd1.txt'
    for t in tf:
        if d_id in tf[t]:
            # only for d1.txt document calculate tfidf
            t_f = tf[t][d_id]
            idf = math.log10(N / float(1 + len(tf[t])))
            tfidf = t_f * idf
            scores[t] = tfidf

for t in scores:
    print t, ",", "d1.txt", "=", scores[t]

