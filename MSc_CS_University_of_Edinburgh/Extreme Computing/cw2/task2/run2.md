# Run command for Task 2 with secondary sorting
hpy \
-input /user/kc/t2 \
-output /user/kc/task2 \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-jobconf stream.num.map.output.key.fields=2 \
-file terms.txt \

# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/ex2/task1/ \
-output /user/s1467292/task_2.out \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-file terms.txt \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-jobconf stream.num.map.output.key.fields=2 \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=5 \