#!/usr/bin/python

'''
    Mapper:
        Input:  logs (host, timestamp,request, reply,bytes)
        Output: emits (page , 1)
'''

import sys
import re

for line in sys.stdin:

    result = re.search('"(.*)"', line)      # extracts the request
    if result:
        file = result.group(1)

        data = file.strip().split()
        if len(data) == 3:
            page = data[1]      # extracts the page from the request
            print page, "\t", 1
