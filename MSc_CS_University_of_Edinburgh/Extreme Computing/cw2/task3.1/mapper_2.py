#!/usr/bin/python

'''
    Mapper:
        Input: page, hits
        Output: page with the maximum hits (page, hitsMax)

        In the Shuffle and sort phase, the values are sorted in reverse order
'''

import sys

max = 0
max_page = None

for line in sys.stdin:

    page, hits = line.strip().split()
    hits = int(float(hits.strip()))
    page = page.strip()
    if page and hits:
        if max == None:
            max = hits
            max_page = page
        else:
            if hits > max:
                max = hits
                max_page = page

print max, "\t", max_page
