#!/usr/bin/python

'''
Pseudoce:
    Reducer:
        Input: page , list of ones
        Output: page, count of hits (sum of ones)

'''

import sys

totalSum = 0
oldPage = None

for line in sys.stdin:
    data = line.strip().split("\t")
    if len(data) != 2:
        # Something has gone wrong. Skip this line.
        continue

    newPage, newValue = data
    if oldPage and oldPage != newPage:
        print oldPage, "\t", totalSum
        oldPage = newPage
        totalSum = 0

    oldPage = newPage
    totalSum += float(newValue)

if oldPage != None:     # emit the last page
    print oldPage, "\t", totalSum
