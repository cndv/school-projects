#!/usr/bin/python

'''
Pseudoce:
    Reducer:
        Input: from each mapper receive the local page with the maximum hits number
        Output: emit the first page, maxHits, since it's the most popular page

'''

import sys

once = 0

for line in sys.stdin:
    if once == 0:
        if line:
            data = line.strip().split('\t')
            if len(data) != 2:
                # Something has gone wrong. Skip this line.
                continue

            hits, page = data
            print page
            once += 1
