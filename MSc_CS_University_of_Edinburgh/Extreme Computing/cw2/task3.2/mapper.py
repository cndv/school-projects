#!/usr/bin/python

'''
    Mapper:
        Input: logs (host, timestamp,request, reply,bytes)
        Output: emit host, one for finding a 404
'''

import sys

for line in sys.stdin:

    data = line.strip().split()

    if len(data) == 10:

        host = data[0]

        reply = int(data[8].strip())

        if reply == 404:
            print host, "\t", 1
