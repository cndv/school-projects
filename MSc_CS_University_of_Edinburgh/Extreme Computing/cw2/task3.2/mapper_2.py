#!/usr/bin/python

'''
    Mapper:
        Input: Host, Count of its 404 errors
        Output: the local top 10 hosts with the most 404 errors ( count, host)

        In the Shuffle and sort phase, sorting is done reverse
'''

import sys
import heapq

results = []
h = []


def push_to_heap(key, value):
    if len(h) == 10:
        heapq.heappushpop(h, (value, key))
    else:
        heapq.heappush(h, (value, key))


def print_top_k():
    for i in range(0, len(h)):
        p = heapq.heappop(h)
        results.append(p)
    for i in results:
        print i[0], '\t', i[1]


for line in sys.stdin:

    host, hits = line.strip().split()

    if host and hits:
        hits = int(float(hits.strip()))
        push_to_heap(host, hits)

print_top_k()