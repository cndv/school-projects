#!/usr/bin/python

'''
    Reducer:
        Input: host, list of ones
        Output: host , totalCount of 404
'''

import sys

totalErrors = 0
oldHost = None

for line in sys.stdin:
    data = line.strip().split("\t")
    if len(data) != 2:
        # Something has gone wrong. Skip this line.
        continue

    newHost, newValue = data
    if oldHost and oldHost != newHost:
        print oldHost, "\t", totalErrors
        oldHost = newHost
        totalErrors = 0

    oldHost = newHost
    totalErrors += float(newValue)

if oldHost != None:
    print oldHost, "\t", totalErrors
