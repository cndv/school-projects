#!/usr/bin/python

'''
Pseudoce:
    Reducer:
        Input: receive the top 10 hosts from each mapper in reverse order
        Output: print the first 10 hosts (host, count of errors)
'''

import sys

count = 0

for line in sys.stdin:
    if count < 10:
        data = line.strip().split("\t")
        if len(data) != 2:
            # Something has gone wrong. Skip this line.
            continue

        hits, host = data
        print host, hits
        count += 1
