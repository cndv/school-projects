#!/usr/bin/python

'''
    Mapper:
        Input: logs (host, timestamp,request, reply,bytes)
        Output: host, timestamp
'''

import sys
from datetime import datetime

for line in sys.stdin:

    data = line.strip().split()

    if len(data) != 10:
        continue

    host = data[0]

    timestampdate = data[3].strip('[')
    timestamp = datetime.strptime(timestampdate, "%d/%b/%Y:%H:%M:%S")       # create a timestamp from the received data
    print host, "\t", timestamp
