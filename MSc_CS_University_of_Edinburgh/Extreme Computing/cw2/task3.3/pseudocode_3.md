# task 3.1
get file

map
    emit file, freq
    
reduce
    emit freqSum, file
    
    sort in reverse
reduce2
    emit 1st k,v
        
        
# task 3.2

map
    emit host, 1 if reply is 404

reduce 
    emit sum, host
    
    
 map2
    emit sum, host in reverse sort
    
 reduce
    emit top 10 break
    
    
# task 3.3

map
    emit host, timestamp

reduce 
    emit host , timeStampDifference