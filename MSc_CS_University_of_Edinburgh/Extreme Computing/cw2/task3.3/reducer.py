#!/usr/bin/python

'''
    Reducer:
        Input: host , list of timestamps
        Output: emits host, difference if more than one visits else timestamp of first visit
'''

import sys
from datetime import datetime

timeDiff = 0
oldHost = None
oldTimeStamp = None
firstVisit = None

for line in sys.stdin:
    data = line.strip().split("\t")
    if len(data) != 2:
        # Something has gone wrong. Skip this line.
        continue

    newHost, timestamp = data
    tstamp = datetime.strptime(timestamp, "%Y-%m-%d %H:%M:%S")      # create a time stamp from the received data

    if oldHost == None:
        firstVisit = tstamp

    if oldHost and oldHost != newHost:
        if oldTimeStamp == firstVisit:
            # only one visit
            print oldHost, "\t", oldTimeStamp
        else:
            # more than one visits
            timeDiff = oldTimeStamp - firstVisit
            print oldHost, "\t", timeDiff
        oldHost = newHost
        firstVisit = tstamp

    oldHost = newHost
    oldTimeStamp = tstamp

if oldHost != None:
    if oldTimeStamp == firstVisit:
        # only one visit
        print oldHost, "\t", oldTimeStamp
    else:
        # more than one visits
        timeDiff = oldTimeStamp - firstVisit
        print oldHost, "\t", timeDiff
