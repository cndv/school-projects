# Run command for Task 3.1 with second job
# 1st job
hpy \
-input /user/kc/data/input/p2 \
-output /user/kc/task3 \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \

# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/ex2/task2/ \
-output /user/s1467292/task_3_1_1.out \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=5 \

# 2nd job
hpy \
-input /user/kc/task3/in.txt \
-output /user/kc/task3_1/ \
-mapper mapper_2.py -file mapper_2.py \
-reducer reducer_2.py -file reducer_2.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.reduce.tasks=1

# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/task_3_1_1.out/part-0000* \
-output /user/s1467292/task_3_1_2.out \
-mapper mapper_2.py -file mapper_2.py \
-reducer reducer_2.py -file reducer_2.py \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=1 \



# locally 2 jobs
$ cat logsTiny.txt | python mapper.py | sort | python reducer.py > part-00000
$ cat part-00000 | python mapper_2.py | sort -r |python reducer_2.py

# Run command for Task 3.2 with second job same as above

# local
$ cat logsTiny.txt | python mapper.py | sort | ./reducer.py |./mapper_2.py | sort -r | ./reducer_2.py

# 1st job
hpy \
-input /user/kc/data/input/p2 \
-output /user/kc/task3_2_1 \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \

# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/ex2/task2/ \
-output /user/s1467292/task_3_2_1.out \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=5 \

 
# 2nd job
hpy \
-input /user/kc/task3_2_1/part-00000 \
-output /user/kc/task3_2_2/ \
-mapper mapper_2.py -file mapper_2.py \
-reducer reducer_2.py -file reducer_2.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.reduce.tasks=1


# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/task_3_2_1.out/part-0000* \
-output /user/s1467292/task_3_2_2.out \
-mapper mapper_2.py -file mapper_2.py \
-reducer reducer_2.py -file reducer_2.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=1 \
 
# Run command for Task 3.3 with secondary sorting
$ cat logsTiny.txt | ./mapper.py | sort | ./reducer.py  
 
hpy \
-input /user/kc/data/input/p2 \
-output /user/kc/task3_3 \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-jobconf stream.num.map.output.key.fields=2 \


# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/ex2/task2/ \
-output /user/s1467292/task_3_3.out \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-partitioner org.apache.hadoop.mapred.lib.KeyFieldBasedPartitioner \
-jobconf stream.num.map.output.key.fields=2 \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=5 \


$ houtput
in24.inetnebr.com  	1995-08-01 00:00:01
ix-esc-ca2-07.ix.netcom.com  	1995-08-01 00:00:09
piweba4y.prodigy.com  	1995-08-01 00:00:10
slppp6.intermind.net  	0:00:02
uplherc.upl.com  	0:00:03