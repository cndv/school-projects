#!/usr/bin/python

'''
    Mapper:
        Input: an xml row of a post

        finds the local top 10 and emits them

        In the Shuffle and sort phase, values are sorted in reverse order

        Output: viewcount , question id
'''

import sys
import re
import heapq

results = []
h = []


def push_to_heap(key, value):
    '''
    Add posts to priority heap,
    only stores the current top 10
    :param key: post id
    :param value: view count
    '''
    if len(h) == 10:
        heapq.heappushpop(h, (value, key))      # Push item on the heap, then pop and return the smallest item from the heap.
    else:
        heapq.heappush(h, (value, key))


def print_top_k():
    '''
    Remove and print each element in the priority heap
    '''
    for i in range(0, len(h)):
        p = heapq.heappop(h)
        results.append(p)
    for i in results:
        print i[0], '\t', i[1]      # emit ViewCount, post id


def getAttribute(data, attribute):
    '''
    This functions searches for an attribute in a data row of type string, and returns its value
    :param data: a string that contains the row
    :param attribute: attribute of the row requested
    :return value: this is the attributes value that has beeen requested
    '''
    s = data.strip()
    match = re.search(r'(%s)=\"(.+?)\"' % attribute, s)  # regex for finding the attribute
    a = match.group()
    match2 = re.search(r'\d+', a)  # get the number value of the above attribute
    value = int(match2.group())
    return value


for line in sys.stdin:

    data = line.strip()
    Id = getAttribute(data, 'Id')
    PostTypeId = getAttribute(data, 'PostTypeId')
    if PostTypeId == 1:     # if it is a question
        ViewCount = getAttribute(data, 'ViewCount')
        if ViewCount:
            push_to_heap(Id, ViewCount)     # add to priority heap

print_top_k()
