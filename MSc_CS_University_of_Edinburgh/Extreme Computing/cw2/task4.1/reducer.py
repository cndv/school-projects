#!/usr/bin/python

'''
Pseudoce:
    Reducer:
        Input:  gets the local top 10 from each mapper in reverse order
        Output: emits the global top 10 (post id, ViewCount)
'''

import sys

count = 0

for line in sys.stdin:

    if count < 10:
        data = line.strip().split('\t')
        if len(data) != 2:
            # Something has gone wrong. Skip this line.
            continue

        ViewCount, Id = data
        print Id, ',', ViewCount
        count += 1
