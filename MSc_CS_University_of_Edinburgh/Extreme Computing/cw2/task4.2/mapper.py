#!/usr/bin/python

'''
    Mapper:
        Input: an xml row of a post
        Output: owner id, id of an answer post
'''

import sys
import re


def getAttribute(data, attribute):
    s = data.strip()
    match = re.search(r'(%s)=\"(.+?)\"' % attribute, s)
    if match == None:
        return match
    a = match.group()
    match2 = re.search(r'\d+', a)
    value = int(match2.group())
    return value


for line in sys.stdin:

    data = line.strip()
    Id = getAttribute(data, 'Id')
    PostTypeId = getAttribute(data, 'PostTypeId')
    if PostTypeId == 2:
        if getAttribute(data, 'OwnerUserId') == None:
            continue
        else:
            OwnerUserId = getAttribute(data, 'OwnerUserId')

        print OwnerUserId, '\t', Id
