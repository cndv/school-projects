#!/usr/bin/python

'''
Pseudoce:
    Reducer:
        Input: owner id, list of answer post ids
        Output: owner id, count of answered posts, ids of answered posts
'''

import sys

totalSum = 0
old_user_id = None
ids = []
for line in sys.stdin:
    data = line.strip().split('\t')
    if len(data) != 2:
        # Something has gone wrong. Skip this line.
        continue

    user_id, post_id = data
    if old_user_id and old_user_id != user_id:
        print old_user_id, "\t", len(ids), '\t', ' '.join(ids)  # conversion to string
        old_user_id = user_id
        ids = []

    ids.append(post_id)
    old_user_id = user_id

if old_user_id != None:
    print old_user_id, "\t", len(ids), '\t', ' '.join(ids)  # conversion to string

