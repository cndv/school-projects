#!/usr/bin/python

'''
Pseudoce:
    Reducer:
        Input: maximum count of answered posts, owner id, list of his post ids
        Output: owner id with the total maximum count of answered posts and the ids of his answered posts
'''

import sys

for line in sys.stdin:
    data = line.strip().split('\t')
    if len(data) != 3:
        # Something has gone wrong. Skip this line.
        continue

    count, user_id, post_ids = data
    print user_id, '\t', post_ids
    break