#!/usr/bin/python
'''
    Mapper:
        Input: an xml row of a post
        Output: id of an owner of a post, accepted answer id of a post
'''
import sys
import re

accepted = set()  # a set that stores all the accepted answer ids,


def getAttribute(data, attribute):
    '''
    This functions searches for an attribute in a data row of type string, and returns its value
    :param data: a string that contains the row
    :param attribute: attribute of the row requested
    :return value: this is the attributes value that has beeen requested
    '''
    s = data.strip()
    match = re.search(r'(%s)=\"(.+?)\"' % attribute, s)  # regex for finding the attribute
    if match == None:
        return None
    a = match.group()
    match2 = re.search(r'\d+', a)  # get the number value of the above attribute
    value = int(match2.group())
    return value


for line in sys.stdin:

    data = line.strip()
    Id = getAttribute(data, 'Id')  # get the Id
    PostTypeId = getAttribute(data, 'PostTypeId')  # get the PostTypeId
    if PostTypeId == 1:  # check if it is a question
        if getAttribute(data, 'AcceptedAnswerId') == None:
            continue  # Something has gone wrong. Skip this line.
        else:
            AcceptedAnswerId = getAttribute(data, 'AcceptedAnswerId')
            accepted.add(AcceptedAnswerId)  # add AcceptedAnswerId to the accepted set
    elif PostTypeId == 2:
        if Id in accepted:  # check if this answer post has been accepted
            OwnerUserId = getAttribute(data, 'OwnerUserId')
            if OwnerUserId:
                print OwnerUserId, '\t', Id  # emit the id of the owner, and the answer id
                accepted.remove(Id)  # remove answer post id from the accepted set
