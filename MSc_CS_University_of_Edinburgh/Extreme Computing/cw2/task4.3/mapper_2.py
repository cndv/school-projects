#!/usr/bin/python

'''
    Mapper:
        Input: owner id, count of answered questions, ids of the answered questions
        Output: One value of the maximum count of answered questions, the user id, the ids of his answers

        In the Shuffle and Sort phase we sort in reverse order
'''

import sys

maxValue = 0
u_id = None
p_ids = None
for line in sys.stdin:

    data = line.strip().split('\t')
    if len(data) != 3:
        continue

    user_id, count, post_ids = data

    if count:

        count = int(count.strip())

        if maxValue == 0:
            maxValue = count
            user_id = u_id
            p_ids = post_ids
        else:
            if count > maxValue:
                maxValue = count
                u_id = user_id
                p_ids = post_ids

print maxValue, '\t', u_id, '\t', p_ids