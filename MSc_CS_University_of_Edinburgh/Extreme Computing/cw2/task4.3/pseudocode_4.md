# task 4.1

# 1st job
map
    emit question id, viewcount if q_id == 1, sort in reverse

reduce
    emit 10 first

# task 4.2
# 1st job

map 
    emit user_id post_id
reduce 
    emit 
        use_id count post_ids
        
# 2nd job
map
    emit max_count user_id post_ids reverse sort 
reduce one reducer
    emit 1st value
    
    
$ cat stackTiny.txt | ./mapper.py |sort |./reducer.py 
1       1       12
13      2       21 73
17      2       51 68
23      1       52
34      1       53
35      1       65
37      1       58
39      2       56 63
40      1       77
41      1       60
45      1       62
49      1       71
50      1       76  
9       1       7


# second job

emit only max from each mapper to one reducer

$ cat stackTiny.txt | ./mapper.py |sort |./reducer.py |./mapper_2.py |sort -r |./reducer_2.py 
13      2       21 73


# task 4.3
# 1st job

mapper
    answer_ids = set()    
    if id = type1:
        answer_ids.add(id_ans)
    else if id =type2
        if id in answer_ids:
            print own_id id
            d.remove(id)
    

reducer
    print max_count user_id ans ids
    
    
# 2nd job
mapper
    emit max_count user_id ans_ids 
    
    reverse sorting one reducer
    
reducer
    emit 1st value