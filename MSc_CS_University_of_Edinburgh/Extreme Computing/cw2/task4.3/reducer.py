#!/usr/bin/python

'''
    Reducer:
        Input: user_id , list of post_id (post_id = answer_id that has been accepted)
        Output: user_id, count of accepted answers, ids of his accepted answers
'''

import sys

old_user_id = None
answer_ids = []  # list with accepted answers that belongs to a user

for line in sys.stdin:
    data = line.strip().split('\t')
    if len(data) != 2:
        continue  # Something has gone wrong. Skip this line.

    user_id, post_id = data

    if old_user_id and old_user_id != user_id:
        # new user has come, so print the old one
        print old_user_id, "\t", len(answer_ids), '\t', ' '.join(answer_ids)  #conversion to string
        old_user_id = user_id
        answer_ids = []  # initialise answer list

    answer_ids.append(post_id)  # add answer post id to answers_id
    old_user_id = user_id

if old_user_id != None:
    print old_user_id, "\t", len(answer_ids), '\t', ' '.join(answer_ids)  # conversion to string

