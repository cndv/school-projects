#!/usr/bin/python

'''
    Reducer:
        Input: Maximum Value, owner id, his accepted answers
        Output: the user with the total maximum accepted answers
'''

import sys

for line in sys.stdin:
    data = line.strip().split('\t')
    if len(data) != 3:
        # Something has gone wrong. Skip this line.
        continue

    count, user_id, post_ids = data
    print user_id, '\t', count, '\t', post_ids
    break