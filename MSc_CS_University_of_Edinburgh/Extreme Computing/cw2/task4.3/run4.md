# task 4.1

hpy \
-input /user/kc/data/input/p3/ \
-output /user/kc/task4_1 \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.reduce.tasks=1 \
-jobconf mapred.map.tasks=4 \


# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/ex2/task3/ \
-output /user/s1467292/task_4_1.out \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=1 \

# task 4.2
# 1st job
hpy \
-input /user/kc/data/input/p3/ \
-output /user/kc/task4_2_1 \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf mapred.reduce.tasks=4 \

# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/ex2/task3/ \
-output /user/s1467292/task_4_2_1.out \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=5 \

# 2nd job
hpy \
-input /user/kc/task4_2_1 \
-output /user/kc/task4_2_2 \
-mapper mapper_2.py -file mapper_2.py \
-reducer reducer_2.py -file reducer_2.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.reduce.tasks=4 \
-jobconf mapred.reduce.tasks=1 \


# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/task_4_2_1.out/part-0000* \
-output /user/s1467292/task_4_2_2.out \
-mapper mapper_2.py -file mapper_2.py \
-reducer reducer_2.py -file reducer_2.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=1 \

# task 4.3

$ cat stackTiny.txt | ./mapper.py |sort |./reducer.py | ./mapper_2.py | sort -r | ./reducer_2.py
39      4       51 56 63 71

# 1st job

hpy \
-input /user/kc/data/input/p3/ \
-output /user/kc/task4_3_1 \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf mapred.map.tasks=4 \
-jobconf mapred.reduce.tasks=4 \

# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/ex2/task3/ \
-output /user/s1467292/task_4_3_1.out \
-mapper mapper.py -file mapper.py \
-reducer reducer.py -file reducer.py \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=5 \

# 2nd job

hpy \
-input /user/kc/task4_3_1/ \
-output /user/kc/task4_3_2 \
-mapper mapper_2.py -file mapper_2.py \
-reducer reducer_2.py -file reducer_2.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.map.tasks=4 \
-jobconf mapred.reduce.tasks=1

# cluster 

hadoop jar /opt/hadoop/hadoop-0.20.2/contrib/streaming/hadoop-0.20.2-streaming.jar \
-input /user/s1467292/task_4_3_1.out/part-0000* \
-output /user/s1467292/task_4_3_2.out \
-mapper mapper_2.py -file mapper_2.py \
-reducer reducer_2.py -file reducer_2.py \
-jobconf mapred.output.key.comparator.class=\
org.apache.hadoop.mapred.lib.KeyFieldBasedComparator \
-jobconf mapred.text.key.comparator.options="-k1nr" \
-jobconf mapred.map.tasks=5 \
-jobconf mapred.reduce.tasks=1 \