/*

Initially we start by assigning the ranges as pairs in the bag. 
Then the farmer iterates until the bag becomes empty.
In the meantime it reiceives from the workers either partial results, 
or a tag with value of zero to create two new tasks. 
If The farmer receives partial results, 
adds them to the total area, or if the approximation fails, it creates 
two new tasks and puts them back to the bag. 
When all tasks have been completed and the bag is empty, the farmer kills 
all the workers by providing a tag of 1.

The worker receives the data, which is an array of doubles which is the range of the area
to be calculated. If the result of the calculation is satisfactory, then it returns
the data with an tag of 1. Otherwise, it sends back the range, with a tag of 0,
to indicate that this range should be splitted by the farmer, and create two 
new tasks. The worker is processing data, until a tag of 1 is received, and ends its process.

*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "stack.h"
#include <unistd.h>

#define EPSILON 1e-3
#define F(arg)  cosh(arg)*cosh(arg)*cosh(arg)*cosh(arg)
#define A 0.0
#define B 5.0
#define true 1

#define SLEEPTIME 1

int *tasks_per_process;

double farmer(int);

void worker(int);

int main(int argc, char **argv ) {
    int i, myid, numprocs;
    double area, a, b;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    
    if(numprocs < 2) {
        fprintf(stderr, "ERROR: Must have at least 2 processes to run\n");
        MPI_Finalize();
        exit(1);
    }
    
    if (myid == 0) { // Farmer
        tasks_per_process = (int *) malloc(sizeof(int)*(numprocs));
        for (i=0; i<numprocs; i++) {
            tasks_per_process[i]=0;
        }
    }
    
    if (myid == 0) { // Farmer
        area = farmer(numprocs);
    } else { //Workers
        worker(myid);
    }
    
    if(myid == 0) {
        fprintf(stdout, "Area=%lf\n", area);
        fprintf(stdout, "\nTasks Per Process\n");
        for (i=0; i<numprocs; i++) {
            fprintf(stdout, "%d\t", i);
        }
        fprintf(stdout, "\n");
        for (i=0; i<numprocs; i++) {
            fprintf(stdout, "%d\t", tasks_per_process[i]);
        }
        fprintf(stdout, "\n");
        free(tasks_per_process);
    }
    MPI_Finalize();
    return 0;
}


double farmer(int numprocs)
{
    // initialize parameters
    double left, right, mid;
    double temp[2];
    int workingID = 0;
    int  i;
    double total  = 0.0;
    MPI_Status status;
    stack *bag = new_stack();
    double data[2] = {A,B};
    // insert initial data to bag
    push(data, bag);
    // iterate while the bag has tasks
    while (!is_empty(bag))
    {
        // keep track of the worker that are working
        int currentlyWorking = 0;
        workingID = 1;
        while(workingID < numprocs)
        {
            tasks_per_process[workingID]++;
            // exit loop if bag there are no more tasks
            if (is_empty(bag)) break;
            // get data
            double * tempData = pop(bag);
            // send task to worker
            MPI_Send(tempData, 2, MPI_DOUBLE,
                     workingID, 0, MPI_COMM_WORLD);
            // increase the number of workers that are occupied
            currentlyWorking++;
            workingID++;
        }
        
        workingID = 1;
        // iterate through the occupied workers
        while (workingID <= currentlyWorking)
        {
            // receive back data
            MPI_Recv(data, 2, MPI_DOUBLE,
                     workingID, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            // tag 1 means , this is a partial area
            if (status.MPI_TAG == 1) {
                total += (double)data[0];
            }
            else // tag 0 means, two new tasks
            {
                left = (double)data[0];
                right = (double)data[1];
                mid = (left + right)/2;
                data[0] = left; data[1] = mid;
                push(data, bag);
                data[0]= mid; data[1] = right;
                push(data, bag);
            }
            workingID++;
        }
        
    }
    
    workingID = 1;
    double junk[2];
    // kill all the workers
    while (workingID < numprocs)
    {
        junk[0] = 0.0;
        junk[1] = 0.0;
        MPI_Send(junk, 2, MPI_DOUBLE,
                 workingID, 1, MPI_COMM_WORLD);
        workingID++;
    }
    return total;
}


void worker(int mypid)
{
    while(true)
    {
        // initialize variables
        MPI_Status status;
        unsigned int tag;
        double left, right, fleft, fright, larea, rarea, mid, fmid;
        double data[2];
        // receive data
        MPI_Recv(data, 2, MPI_DOUBLE,
                 MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        
        tag = status.MPI_TAG;
        if (tag == 1) break; // tag 1 means kill the worker
        
        usleep(SLEEPTIME);
        
        left = (double)data[0];
        right = (double)data[1];
        mid = (left + right)/2;
        fleft = F(left);
        fright = F(right);
        fmid = F(mid);
        larea = (fleft+fmid) * (mid - left) /2;
        rarea = (fmid+fright) * (right - mid)/2;
        
        double lrarea = (fleft + fright) * (right - left) / 2;
        
        if (fabs((larea+rarea) - lrarea) > EPSILON)
        {
            // send two new tasks
            data[0] = left;
            data[1] = right;
            
            MPI_Send(data, 2, MPI_DOUBLE,
                     0, 0, MPI_COMM_WORLD);
        }
        else
        {   // send result
            data[0] = larea + rarea;
            MPI_Send(data, 2, MPI_DOUBLE,
                     0, 1, MPI_COMM_WORLD);
        }
        
    }
}



