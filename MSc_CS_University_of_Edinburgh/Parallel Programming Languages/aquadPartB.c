/* 
The farmer assigns each worker with an equal chunk of input to work with. The workers receive their data
in an array, which is the range (left and right boundary ) of the partial area they have to calculate.
Then, the area is calculated, by calling the quad function. 
The quad is extended, to keep track the number of times it has been called, by each worker.
Next, the partial area and number of tasks , are being returned back to the farmer, 
and the job of the worker is finished, since each worker is assigned only one range.
The total area is calculated by the summation of the partial areas of the returned values of the workers.
 */


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>
#include "stack.h"
#include <unistd.h>

#define EPSILON 1e-3
#define F(arg)  cosh(arg)*cosh(arg)*cosh(arg)*cosh(arg)
#define A 0.0
#define B 5.0

#define SLEEPTIME 1

int *tasks_per_process;

double farmer(int);
void worker(int);
double quad(double left, double right, double fleft,
            double fright, double lrarea, int* numberOfTasks);

int main(int argc, char **argv ) {
    int i, myid, numprocs;
    double area, a, b;
    
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD,&myid);
    
    if(numprocs < 2) {
        fprintf(stderr, "ERROR: Must have at least 2 processes to run\n");
        MPI_Finalize();
        exit(1);
    }
    
    // Initialization
    printf("rank is %d \n", myid);
    
    if (myid == 0) { // Farmer
        // init counters
        tasks_per_process = (int *) malloc(sizeof(int)*(numprocs));
        for (i=0; i<numprocs; i++) {
            tasks_per_process[i]=0;
        }
    }
    
    // Main functionality
    
    if (myid == 0) { // Farmer
        printf("init farmer \n");
        area = farmer(numprocs);
    } else { //Workers
        printf("init worker \n");
        worker(myid);
    }
    
    // Output results
    
    if(myid == 0) {
        fprintf(stdout, "Area=%lf\n", area);
        fprintf(stdout, "\nTasks Per Process\n");
        for (i=0; i<numprocs; i++) {
            fprintf(stdout, "%d\t", i);
        }
        fprintf(stdout, "\n");
        for (i=0; i<numprocs; i++) {
            fprintf(stdout, "%d\t", tasks_per_process[i]);
        }
        fprintf(stdout, "\n");
        free(tasks_per_process);
    }
    MPI_Finalize();
    return 0;
}


double farmer(int numprocs)
{
    // initialize variables
    double total;
    double data[2];
    double left, right;
    // calculate chunk
    double chunk = (B - A) / (numprocs - 1);
    int workerID = 1;
    // assign each worker an equal chunk
    while (workerID < numprocs) // assign tasks to workers
    {
        // get range by multiplying chunk by the index of the worker
        left = A + chunk *(workerID - 1);
        right = A + chunk * workerID;
        data[0] = left;
        data[1] = right;
        MPI_Send(data, 2, MPI_DOUBLE,
                 workerID, 0, MPI_COMM_WORLD);
        workerID++;
    }

    workerID = 1;
    
    while (workerID < numprocs) // get data from workers
    {
        MPI_Recv(data, 2, MPI_DOUBLE,
                 workerID, MPI_ANY_TAG, MPI_COMM_WORLD, NULL);
        total += (double) data[0]; // add partial integrals
        tasks_per_process[workerID] = (int) data[1];
        workerID++;
    }
    return total;
}


void worker(int mypid)
{
    // initialize variables
    double data[2], buff[2];
    double left, mid, right, fleft, fright, partial, larea, rarea;
    // receive task
    MPI_Recv(data, 2, MPI_DOUBLE,
             MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, NULL);
    
    left = data[0];
    right = data[1];
    fleft = F(left);
    fright = F(right);
    partial = (fleft + fright) * (right - left) / 2;
    int numberOfTasks = 0;
    // calculate partial area, and get number of tasks completed
    double partialArea = quad(left, right, fleft, fright,
                              partial, &numberOfTasks);
    buff[0] = partialArea;
    buff[1] = (double) numberOfTasks ;
    // send back results
    MPI_Send(buff, 2, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
}

// extended quad function to calculate also the number of tasks that
// this worker has completed
double quad(double left, double right, double fleft,
            double fright, double lrarea, int * numberOfTasks) {
    double mid, fmid, larea, rarea;
    (*numberOfTasks)++;
    mid = (left + right) / 2;
    fmid = F(mid);
    larea = (fleft + fmid) * (mid - left) / 2;
    rarea = (fmid + fright) * (right - mid) / 2;
    if( fabs((larea + rarea) - lrarea) > EPSILON ) {
        larea = quad(left, mid, fleft, fmid, larea, numberOfTasks);
        rarea = quad(mid, right, fmid, fright, rarea, numberOfTasks);
    }
    return (larea + rarea);
}

