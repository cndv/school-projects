__author__ = 'Konstantinos Chartomatzis'

"""
Name: Konstantinos Chartomatzis
Email: s1467292@sms.ed.ac.uk
"""

import string, math, operator
from collections import defaultdict
from nltk import PorterStemmer

# read documents
def read_docs(filename):
    f_docs = open(filename, 'r')
    docs_to_lines = f_docs.readlines()
    f_docs.close()
    return docs_to_lines


#read queries
def read_qrys(filename):
    f_qrys = open(filename, 'r')
    qrys_to_lines = f_qrys.readlines()
    f_qrys.close()
    return qrys_to_lines


#read stop words
def read_stop_words(filename):
    f_stop = open(filename, 'r')
    stopwords_list = f_stop.read().replace('\\', ' ').split()
    return stopwords_list


#read documents per line, construct document frequency index
def get_dict_docs(docs_to_lines):
    dict_docs = defaultdict(dict)
    for line in docs_to_lines:
        replace_punctuation = string.maketrans(string.punctuation, ' ' * len(string.punctuation))
        line = line.translate(replace_punctuation).lower().split()
        doc_id = line[0]
        for t in line[1:]:
            t = PorterStemmer().stem_word(t).encode('ascii', 'ignore')
            if t not in dict_docs[doc_id].keys():
                dict_docs[doc_id][t] = 1
            else:
                dict_docs[doc_id][t] += 1
    return dict_docs


# construct inverse document index
def get_inv_dict_docs(dict_docs):
    inv_dict_docs = defaultdict(dict)
    for k in dict_docs.keys():
        for kk in dict_docs[k].keys():
            inv_dict_docs[kk][k] = dict_docs[k][kk]
    return inv_dict_docs


# construct query frequency index
def init_dict_qrys(qrys_to_lines):
    dict_qrys = defaultdict(dict)
    for line in qrys_to_lines:
        replace_punctuation = string.maketrans(string.punctuation, ' ' * len(string.punctuation))
        line = line.translate(replace_punctuation).lower().split()
        q_id = line[0]
        for t in line[1:]:
            t = PorterStemmer().stem_word(t).encode('ascii', 'ignore')
            if t not in stopwords_set:
                if t not in dict_qrys[q_id].keys():
                    dict_qrys[q_id][t] = 1
                else:
                    dict_qrys[q_id][t] += 1
    return dict_qrys


# get average document size
def get_avg_d(dict_docs):
    s = 0
    for d in dict_docs.keys():
        s += len(dict_docs[d].keys())
    avg_d = s / float((len(dict_docs.keys())))
    return avg_d


# get average query size
def get_avg_q(dict_qrys):
    s2 = 0
    for q in dict_qrys.keys():
        s2 += len(dict_qrys[q].keys())
    avg_q = s2 / float((len(dict_qrys.keys())))
    return avg_q


# get top k frequent terms of a document
def top_k_terms(dict_of_terms):
    topw = sorted(dict_of_terms.items(), key=operator.itemgetter(1), reverse=True)
    top_k = []
    for i in range(len(dict_of_terms.items())):
        top_k.append(topw[i][0])
    return top_k  # list with four items


# get top k similar document of a query
def top_k_docs(k):
    topd = defaultdict(list)
    top_k = defaultdict(list)
    top_docs_k_id = defaultdict(list)
    for q in sorted(final.keys()):
        topd[q] = sorted(final[q].items(), key=operator.itemgetter(1), reverse=True)
        top_k[q] = topd[q][0:k]  # top docs

    for q in top_k.keys():
        for i in range(len(top_k[q])):
            top_docs_k_id[q].append(top_k[q][i][0])
    return top_docs_k_id


# change tfw with dw
def change_weights_docs(dict_docs, inv_dict_docs):
    for d in dict_docs.keys():
        for w in dict_docs[d].keys():
            tf_wd = inv_dict_docs[w][d]
            length_of_d = len(dict_docs[d].keys())
            df_w = len(inv_dict_docs[w].keys())
            dict_docs[d][w] = (tf_wd / float((tf_wd + k * length_of_d / float(avg_d)))) * math.log(c / float(df_w))
    return dict_docs


# change tfq with qw
def change_weights_qrys(dic_qrys):
    for q in dict_qrys.keys():
        for w in dict_qrys[q].keys():
            tf_wq = dict_qrys[q][w]
            length_of_q = len(dict_qrys[q].keys())
            df_w = len(inv_dict_docs[w].keys())
            if df_w > 0:
                qw = (tf_wq / float((tf_wq + k * length_of_q / float(avg_q)))) * math.log(c / float(df_w))
            else:
                qw = 0
            dict_qrys[q][w] = qw
    return dict_qrys


# construct similarity table
def similarity_table():
    # similarity
    final = defaultdict(dict)
    for q in dict_qrys.keys():  # for every query
        sum_squared_qw = 0

        length_of_q = len(dict_qrys[q].keys())

        for w in dict_qrys[q].keys():
            sum_squared_qw += dict_qrys[q][w] ** 2

        for d in dict_docs.keys():
            sum_qw_dot_dw = 0
            sum_squared_dw = 0
            for w in dict_docs[d].keys():
                sum_squared_dw += dict_docs[d][w] ** 2

            for w in dict_qrys[q].keys():  # for every word
                if w not in stopwords_set:
                    if w in dict_docs[d].keys():
                        qw = dict_qrys[q][w]
                        dw = dict_docs[d][w]
                        sum_qw_dot_dw += qw * dw

            similarity = sum_qw_dot_dw / float(math.sqrt(sum_squared_qw) * math.sqrt(sum_squared_dw))
            if similarity > 0:
                final[q][d] = similarity
    return final


# expand queries with terms
def expand_qrys():
    dict_qrys = init_dict_qrys(qrys_to_lines)  # re initialise queries
    for q in top_k_id.keys():
        for d in top_k_id[q]:
            l = top_k_terms(dict_docs[d])
            for w in l:
                if w not in dict_qrys[q].keys():
                    dict_qrys[q][w] = 1
                else:
                    dict_qrys[q][w] += 1
    return dict_qrys


# write to best.top
def write_to_file(filename):
    # print similarity table
    output_list = []
    for q in sorted(final.keys()):
        for d in sorted(final[q].keys()):
            l = [q, 0, d, 0, final[q][d], 0]
            output_list.append(l)

    f_out = open(filename, 'w')
    for l in output_list:
        # convert list to string for writing
        s = " ".join(str(e) for e in l)
        f_out.write("%s \n" % s)

    f_out.close()


docs_to_lines = read_docs('docs.txt')  # read docs
qrys_to_lines = read_qrys('qrys.txt')  # read queries
stopwords_list = read_stop_words('stopwords')  # read stopwords
stopwords_set = set(stopwords_list)  # add stop words to a set
dict_docs = get_dict_docs(docs_to_lines)  # read document per line
inv_dict_docs = get_inv_dict_docs(dict_docs)  # construct inverse document index
dict_qrys = init_dict_qrys(qrys_to_lines)  # construct query index by tokenizing then stemming

k = 2
c = len(dict_docs.keys())  # total number to documents
avg_d = get_avg_d(dict_docs)  # average document size
avg_q = get_avg_q(dict_qrys)  # average query size
dict_docs = change_weights_docs(dict_docs, inv_dict_docs)  # update weights for every document
dict_qrys = change_weights_qrys(dict_qrys)  # update weight for every query
final = similarity_table()  # calculate similarity table
top_k_id = top_k_docs(9)  # calculate top nine documents per query
dict_qrys = expand_qrys()  # expand query by adding terms from top nine documents
dict_qrys = change_weights_qrys(dict_qrys)  # recalculate query weights
final = similarity_table()  # recalculate similarity table
write_to_file('best.top')  # write to file
