__author__ = 'Konstantinos Chartomatzis'

"""
Name: Konstantinos Chartomatzis
Email: s1467292@sms.ed.ac.uk
References :
    - Strip punctuation from a string: http://stackoverflow.com/questions/265960/best-way-to-strip-punctuation-from-a-string-in-python
    - Sort dictionary by key: http://stackoverflow.com/questions/9001509/python-dictionary-sort-by-key
"""

import string
from collections import defaultdict

f_docs = open('docs.txt', 'r')
f_qrys = open('qrys.txt', 'r')
docs_to_lines = f_docs.readlines()
qrys_to_lines = f_qrys.readlines()
f_docs.close()
f_qrys.close()


# read docs and queries and save them in a dictionary of sets

# dictionary of sets
dict_of_docs = defaultdict(set)
# line is a list
for line in docs_to_lines:
    # remove punctuation, make lowercase, and split
    replace_punctuation = string.maketrans(string.punctuation, ' ' * len(string.punctuation))
    line = line.translate(replace_punctuation).lower().split()
    key = line[0]
    for j in line:
        dict_of_docs[key].add(j)

dict_of_qrys = defaultdict(set)
for line in qrys_to_lines:
    # remove punctuation, make lowercase, and split
    replace_punctuation = string.maketrans(string.punctuation, ' ' * len(string.punctuation))
    line = line.translate(replace_punctuation).lower().split()
    key = line[0]
    for j in line:
        dict_of_qrys[key].add(j)

# make list of scores
output_list = []
for j in sorted(dict_of_qrys):
    for jj in sorted(dict_of_docs):
        score = len(dict_of_qrys[j].intersection(dict_of_docs[jj]))
        if score > 0:
            s = '{0} 0 {1} 0 {2} 0 '.format(j, score, jj)
            l = [j, 0, jj, 0, score, 0]
            output_list.append(l)

# write scrores to output file
f_out = open('overlap.top', 'w')
for l in output_list:
        #convert list to string for writing
        s = " ".join(str(e) for e in l)
        f_out.write("%s \n" % s)

f_out.close()



