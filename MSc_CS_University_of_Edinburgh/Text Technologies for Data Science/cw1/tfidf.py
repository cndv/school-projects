__author__ = 'Konstantinos Chartomatzis'

"""
Name: Konstantinos Chartomatzis
Email: s1467292@sms.ed.ac.uk
References :
    - Strip punctuation from a string: http://stackoverflow.com/questions/265960/best-way-to-strip-punctuation-from-a-string-in-python
    - Sort dictionary by key: http://stackoverflow.com/questions/9001509/python-dictionary-sort-by-key
"""

import string
import math
from collections import defaultdict


def read_docs(filename):
    f_docs = open(filename, 'r')
    docs_to_lines = f_docs.readlines()
    f_docs.close()
    return docs_to_lines

#read queries
def read_qrys(filename):
    f_qrys = open(filename, 'r')
    qrys_to_lines = f_qrys.readlines()
    f_qrys.close()
    return qrys_to_lines

# dictionary of dicts
def get_dict_docs(docs_to_lines):
    dict_docs = defaultdict(dict)
    for line in docs_to_lines:# line is a list
        # remove punctuation, make lowercase, and split
        replace_punctuation = string.maketrans(string.punctuation, ' ' * len(string.punctuation))
        line = line.translate(replace_punctuation).lower().split()
        key = line[0]
        for j in line[1:]:
            if j not in dict_docs[key].keys():
                dict_docs[key][j] = 1
            else:
                dict_docs[key][j] += 1
    return dict_docs


# construct inverse document index
def get_inv_dict_docs(dict_docs):
    inv_dict_docs = defaultdict(dict)
    for k in dict_docs.keys():
        for kk in dict_docs[k].keys():
            inv_dict_docs[kk][k] = dict_docs[k][kk]
    return inv_dict_docs


# dict of qrys
def init_dict_qrys(qrys_to_lines):
    dict_qrys = defaultdict(dict)
    for line in qrys_to_lines:
        replace_punctuation = string.maketrans(string.punctuation, ' ' * len(string.punctuation))
        line = line.translate(replace_punctuation).lower().split()
        key = line[0]
        for l in line[1:]:
            if l not in dict_qrys[key].keys():
                dict_qrys[key][l] = 1
            else:
                dict_qrys[key][l] += 1
    return dict_qrys

# similarity table
def similarity_table():

    k = 2
    c = len(dict_docs.keys())
    # avg_d
    s = 0
    for d in dict_docs.keys():
        s += len(dict_docs[d].keys())

    avg_d = s/(len(dict_docs.keys()))

    final = defaultdict(dict)
    for q in sorted(dict_qrys.keys()):
        for w in dict_qrys[q].keys():
            tf_wq = dict_qrys[q][w]
            df_w = len(inv_dict_docs[w].keys())

            for d in inv_dict_docs[w].keys():
                tf_wd = inv_dict_docs[w][d]
                length_of_d = len(dict_docs[d].keys())
                similarity = tf_wq * (tf_wd / float((tf_wd + k * length_of_d/ avg_d))) * math.log(c / float(df_w))
                if similarity > 0:
                    if d in final[q].keys():
                        final[q][d] += similarity
                    else:
                        final[q][d] = similarity
    return final


def write_to_file(filename):
    output_list = []
    for q in sorted(final.keys()):
        for d in final[q].keys():
            l = [q,0,d,0,final[q][d],0]
            output_list.append(l)

    f_out = open(filename, 'w')
    for l in output_list:
        #convert list to string for writing
        s = " ".join(str(e) for e in l)
        f_out.write("%s \n" % s)
    f_out.close()


docs_to_lines = read_docs('docs.txt')# read docs
qrys_to_lines = read_qrys('qrys.txt')# read queries
dict_docs = get_dict_docs(docs_to_lines)
inv_dict_docs = get_inv_dict_docs(dict_docs)
dict_qrys = init_dict_qrys(qrys_to_lines)
final = similarity_table()
write_to_file('tfidf.top')