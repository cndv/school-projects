__author__ = 'Konstantinos Chartomatzis'
"""
"""

idf_fix = 13.6332
threshold = 0.2
s_doc = []  # square sumd of docs


def read_idf(filename):
    f_docs = open(filename, 'r')
    idf_d = {}
    for l in f_docs:
        l = l.split()
        idf_d[l[1]] = round(float(l[0]), 4)
    f_docs.close()
    return idf_d


def cos_sim(doc_a, doc_b, i, j):  # both are dictionaries i = index of doc_a, j index of doc_b
    sum_qw_dot_dw = 0
    keys_a = set(doc_a.keys())
    keys_b = set(doc_b.keys())
    intersection = keys_a & keys_b
    for k in intersection:  # for every word
        qw = doc_a[k]
        dw = doc_b[k]
        sum_qw_dot_dw += qw * dw
    if s_doc[i] == 0 or s_doc[j] == 0:
        similarity = 0
    else:
        similarity = sum_qw_dot_dw / float(pow(s_doc[i] * s_doc[j], 0.5))
    return similarity


def write_to_file(filename, i, b):
    f_out = open(filename, 'a')
    s = str(i) + ' ' + str(b) + '\n'
    f_out.write(s)
    f_out.close()


def pairs(docs, a):
    l = len(docs)
    b = 0
    if l > 0:
        cos_a_b = cos_sim(a, docs[b], l, b)
    else:
        cos_a_b = 0
    for i in range(1, l):
        cos_a_i = cos_sim(a, docs[i], l, i)
        if cos_a_i >= cos_a_b:
            b = i
            cos_a_b = cos_a_i
    if cos_a_b > threshold:
        b += 1
        write_to_file('brute.txt', l + 1, b)


def read_file(filename, count):
    f = open(filename, 'r')
    lines = f.readlines()
    docs = []
    for l in range(count):
        sqr_sum = 0
        lines[l] = lines[l].lower().split()
        a = {}
        for t in lines[l][1:]:
            if t not in a.keys():
                a[t] = 1
            else:
                a[t] += 1
        for k in a.keys():
            if k in idf_d.keys():
                a[k] *= idf_d[k]
            else:
                a[k] *= idf_fix
            sqr_sum += a[k] ** 2

        s_doc.append(sqr_sum)
        pairs(docs, a)
        docs.append(a)
    f.close()
    return docs


idf_d = read_idf('news.idf')
docs = read_file('news.txt', 10000)  # read docs
