__author__ = 'Konstantinos Chartomatzis'
"""
Personal Details:
Name: Konstantinos Chartomatzis
Email: s1467292@sms.ed.ac.uk

References:
[1] class AutoVivification: http://stackoverflow.com/questions/651794/whats-the-best-way-to-initialize-a-dict-of-dicts-in-python
[2] def keywithmaxval: http://stackoverflow.com/questions/268272/getting-key-with-maximum-value-in-dictionary
[3] Sort dictionary by value: http://stackoverflow.com/questions/613183/sort-a-python-dictionary-by-value
"""

s_doc = []  # square sum of docs
final_pairs = []


class AutoVivification(dict):  # [1]
    """Implementation of perl's autovivification feature."""

    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value


def read_idf(filename):
    f_docs = open(filename, 'r')
    idf_d = {}
    for l in f_docs:
        l = l.split()
        idf_d[l[1]] = round(float(l[0]), 4)
    f_docs.close()
    return idf_d


def write_to_file(filename, i, b):
    f_out = open(filename, 'a')
    s = str(i) + ' ' + str(b) + '\n'
    f_out.write(s)
    f_out.close()


def keywithmaxval(d):  # [2]
    """ a) create a list of the dict's keys and values;
        b) return the key with the max value"""
    v = list(d.values())
    k = list(d.keys())
    return k[v.index(max(v))]


def pairs(inv_docs, a, a_index):
    threshold = 0.2
    scores = {}
    for t in a.keys():
        k = 0
        for d in sorted(inv_docs[t], key=inv_docs[t].get, reverse=True): # [3]
            if d not in scores.keys():
                scores[d] = a[t] * inv_docs[t][d]
            else:
                scores[d] += a[t] * inv_docs[t][d]
            if k < 3:
                k += 1
            else:
                break

    if scores:
        for k, v in scores.items():
            scores[k] = v / float(pow(s_doc[k] * s_doc[a_index], 0.5))
        max_score = keywithmaxval(scores)
        max_val = scores[max_score]
    else:
        max_val = 0

    if max_val > threshold:
        s = str(a_index + 1) + " " + str(max_score + 1) + '\n'
        final_pairs.append(s)
        if 20 == len(final_pairs):
            write_to_file('pairs.out')
            final_pairs[:] = []
            # print a_index + 1, max_score + 1


def add_doc_to_inverted_index(a, inv_docs, index):
    for k in a.keys():
        inv_docs[k][index] = a[k]


def read_file(filename, count):
    idf_fix = 13.6332
    f = open(filename, 'r')
    lines = f.readlines()
    inv_docs = AutoVivification()
    for l in range(count):  # read count number of documents
        sqr_sum = 0
        lines[l] = lines[l].lower().split()  #get line
        a = {}
        for t in lines[l][1:]:  # get all terms
            if t not in a.keys():
                a[t] = 1
            else:
                a[t] += 1
        for k in a.keys():
            if k in idf_d.keys():
                a[k] *= idf_d[k]
            else:
                a[k] *= idf_fix
            sqr_sum += a[k] ** 2

        s_doc.append(sqr_sum)
        pairs(inv_docs, a, l)
        add_doc_to_inverted_index(a, inv_docs, l)
    f.close()


def write_to_file(filename):
    s = ""
    for i in final_pairs:
        s = s + i
    f_out = open(filename, 'a')
    f_out.write(s)
    f_out.close()


idf_d = read_idf('news.idf')
read_file('news.txt', 10000)
write_to_file('pairs.out')