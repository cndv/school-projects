'''
Name: Konstantinos Chartomatzis
UNN: s1467292
References:
    - [1]  Split list into chunks: http://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks-in-python
    - [2]  Sum list items by column: http://stackoverflow.com/questions/3223043/how-do-i-sum-the-columns-in-2d-list
'''
from nltk import PorterStemmer
import string
import hashlib
import zlib
import re
from collections import defaultdict
import math
import cProfile
from datetime import datetime


# get the key of the document (doc id)
def get_key(line):
    m = re.search(r't\d{1,10}', line)
    if m:
        return str(m.group())


# get the document body
def get_body(line):
    m = re.sub(r'[\d{1,10}]', r'', line).strip()
    return m


# read stop words
def read_stop_words(filename):
    f_stop = open(filename, 'r')
    stopwords_list = f_stop.read().replace('\\', ' ').split()
    return stopwords_list


def preprocess(document):  # document is a string, basically preprocess body
    replace_punctuation = string.maketrans(string.punctuation, ' ' * len(string.punctuation))  # remove punctuation
    document = document.translate(replace_punctuation).lower().split()  # lowercase and split
    processed_doc = {}
    for w in document:
        if w not in stopwords_set:  # remove stopwords
            w = PorterStemmer().stem_word(w.decode('utf-8')).encode('ascii', 'ignore')  # stem
            if w in processed_doc:  # compute tf
                processed_doc[w] += 1
            else:
                processed_doc[w] = 1
    return processed_doc


def export_type_1(id_fake, id_original):
    f_out = open('type1.dub', 'a')
    s = str(id_fake) + ' ' + str(id_original) + '\n'
    f_out.write(s)
    f_out.close()


def export_type_2(id_fake, id_original):
    f_out = open('type2.dub', 'a')
    s = str(id_fake) + ' ' + str(id_original) + '\n'
    f_out.write(s)
    f_out.close()


def output_result(doc_a_id, doc_b_id):
    id1_num = int(re.search(r'\d{1,7}', doc_a_id).group())
    id2_num = int(re.search(r'\d{1,7}', doc_b_id).group())
    if id1_num < id2_num:
        export_type_2(doc_a_id, doc_b_id)  # write to file
    else:
        export_type_2(doc_b_id, doc_a_id)


def output_result1(doc_a_id, doc_b_id):
    id1_num = int(re.search(r'\d{1,7}', doc_a_id).group())
    id2_num = int(re.search(r'\d{1,7}', doc_b_id).group())
    if id1_num < id2_num:
        export_type_1(doc_a_id, doc_b_id)  # write to file
    else:
        export_type_1(doc_b_id, doc_a_id)



''' [1] '''


def chunks(bin_doc, k):
    """ Yield successive k-sized chunks from bin_doc.
        must be converted to list
    """
    for i in xrange(0, len(bin_doc), k):
        yield bin_doc[i:i + k]


def fingerprint(doc):  #returns a list of a fingerprint of 128 bits
    h = hashlib.md5()
    max_hash_code = h.digest_size * 8  #size in bits
    hash_dict = {}
    for k, v in doc.iteritems():
        h.update(k)  # produce hash code which is hex
        b_finprint = bin(int(h.hexdigest(), 16))[2:]  #convert to decimal then to binary
        tf_finprint = [v if x == '1' else -v for x in b_finprint]  # 0 -> -tf, 1 -> tf
        len_finprint = len(tf_finprint)
        difference = max_hash_code - len_finprint
        zero_list = []
        for i in range(0, difference):
            zero_list.append(-v)
        tf_finprint = zero_list + tf_finprint
        hash_dict[k] = tf_finprint  # add binary hash to dict, do not print 0b in front of binary num

    total_fins = hash_dict.values()
    complete_doc_fingerprint = [sum(x) for x in zip(*total_fins)]  # ''' [2] '''
    final_doc_fingerprint = [0 if int(float(x)) < 0 else 1 for x in complete_doc_fingerprint]  # x < 0 - > 0, x > 0 -> 1

    return final_doc_fingerprint  # list


def compute_cos(document_a_id_list, document_b_id, pre_processed_body):  #these are ids
    doc_b = pre_processed_body
    success = 0
    for document_a_id in document_a_id_list:
        doc_a = documents[document_a_id]
        dotproduct = 0
        sum_sqr_a = square_sum[document_a_id]
        sum_sqr_b = square_sum[document_b_id]
        keys_a = set(doc_a.keys())
        keys_b = set(doc_b.keys())
        intersection = keys_a & keys_b
        for k in intersection:
            dotproduct += doc_a[k] * doc_b[k]

        score = dotproduct / float(math.sqrt(sum_sqr_a * sum_sqr_b))
        if score > 0.90 and score < 1.0:
            output_result(document_a_id, document_b_id)
            print score
            success = 1
            return success
        elif score == 1.0:
            output_result1(document_a_id, document_b_id)
            success = 1
            return success

    return success


def add_doc_to_hash(finger_to_chunks, doc_id):
    count_chunks = len(finger_to_chunks)
    for i in range(0, count_chunks - 1):
        str_finger = str(finger_to_chunks[i])
        if str_finger in dict_of_hashes[i]:
            dict_of_hashes[i][str_finger].append(doc_id)
        else:
            dict_of_hashes[i][str_finger] = [doc_id]


def compare_docs(finger_to_chunks, doc_id, pre_processed_body):
    success = 0
    count_chunks = len(finger_to_chunks)
    for i in range(0, count_chunks):
        str_finger = str(finger_to_chunks[i])
        if str_finger in dict_of_hashes[i]:
            doc_a_id_list = dict_of_hashes[i][str_finger]
            doc_b_id = doc_id
            success = compute_cos(doc_a_id_list, doc_b_id, pre_processed_body)
            if success == 1:
                return 1
    add_doc_to_hash(finger_to_chunks, doc_id)
    return 0


documents = {}
dict_of_hashes = defaultdict(dict)
square_sum = {}

# open a file , return lines
def read_docs(filename, k):
    f = open(filename, 'r')
    for d in f:
        doc_id = get_key(d)
        body = get_body(d)
        pre_processed_body = preprocess(body)
        square_sum[doc_id] = 0
        for w in pre_processed_body:
            square_sum[doc_id] += pre_processed_body[w] ** 2
        if bool(documents) == False:  #dict empty
            documents[doc_id] = pre_processed_body
            doc_fingerprint = fingerprint(pre_processed_body) 
            finger_to_chunks = list(chunks(doc_fingerprint, k))
            add_doc_to_hash(finger_to_chunks, doc_id)
        else:
            result = find_near_duplicate(doc_id, pre_processed_body, k) 
            if result == 0:
                documents[doc_id] = pre_processed_body
    f.close()


def find_near_duplicate(doc_id, pre_processed_body, k): 
    doc_fingerprint = fingerprint(pre_processed_body)  
    finger_to_chunks = list(chunks(doc_fingerprint, k))  # split finger into 10bits
    result = compare_docs(finger_to_chunks, doc_id, pre_processed_body)
    return result


stopwords_set = set(read_stop_words('stopwords'))  # read stopwords


k = 8
print 'k = ', k
read_docs('data.test', k) 
