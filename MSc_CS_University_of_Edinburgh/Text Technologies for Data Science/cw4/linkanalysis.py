'''
Name: Konstantinos Chartomatzis
UNN: s1467292
Email: s1467292@sms.ed.ac.uk

References:
    - [1] Get top k values from a dictionary http://stackoverflow.com/questions/12266617/finding-top-k-largest-keys-in-a-dictionary-python
    - [2] Counting occurrences in a python list http://stackoverflow.com/questions/6046387/counting-occurrences-in-a-python-list
'''

from collections import defaultdict
import copy, math
from heapq import nlargest
from operator import itemgetter

#get the unique number of emails
def number_of_addresses():
    return len(addresses)

#load graph in memory
def load_graph(filename):
    graph = open(filename, 'r')
    for n_line in graph:
        data = n_line.split()
        sender = data[1]
        receiver = data[2]
        if sender != receiver:  # exclude A-->A
            # load addresses
            addresses.add(sender)
            addresses.add(receiver)
            # load incoming hash
            incoming[receiver].append(sender)
            # load outlink hash
            outlink[sender].append(receiver)
            # load outlink counter hash
            if sender not in outlink_counter:
                outlink_counter[sender] = 1
            else:
                outlink_counter[sender] += 1
            # load linlink counter hash
            if receiver not in inlink_counter:
                inlink_counter[receiver] = 1
            else:
                inlink_counter[receiver] += 1
    graph.close()

#compute hubs and authorities, Hits algorithm
def hits(num_of_iter):
    hub = {}
    auth = {}
    sqrt_addr_count = float(math.sqrt(number_of_addresses()))
    for a in addresses:
        hub[a] = 1.0 / sqrt_addr_count
        auth[a] = 1.0 / sqrt_addr_count
    for i in range(0, num_of_iter):
        norm = 0
        for sender in addresses:
            hub[sender] = 0
            for receiver in outlink[sender]:
                hub[sender] += auth[receiver]
            norm += math.pow(hub[sender], 2)
        norm = math.sqrt(norm)
        for a in addresses:
            hub[a] /= float(norm)

        norm = 0
        for receiver in addresses:
            auth[receiver] = 0
            for sender in incoming[receiver]:
                auth[receiver] += hub[sender]
            norm += math.pow(auth[receiver], 2)
        norm = math.sqrt(norm)
        for a in addresses:
            auth[a] /= float(norm)
    sanity_hits(auth, hub)
    top_k_hubs = top_k(hub, 100)
    top_k_auths = top_k(auth, 100)
    export_results(top_k_auths, 'auth.txt')
    export_results(top_k_hubs, 'hubs.txt')

#compute pagerank
def pagerank(num_of_iter):
    new_pg = {}
    old_pg = {}
    for a in addresses:
        if a not in incoming:
            incoming[a] = {}
        old_pg[a] = 1.0 / N
        if a not in outlink_counter:
            sinks.add(a)
    for i in range(0, num_of_iter):
        sigma = 0
        for sink in sinks:  # sink nodes
            sigma += old_pg[sink]
        for receiver in old_pg:  # tails + sink
            rank = (1 - lamda + lamda * sigma) / N
            for sender in incoming[receiver]:  # heads
                if sender in outlink_counter:
                    rank += lamda * old_pg[sender] / outlink_counter[sender]
            new_pg[receiver] = rank
        old_pg = copy.deepcopy(new_pg)
    top_k_pr = top_k(new_pg, 100)
    subgraph(top_k_pr)
    export_results(top_k_pr, 'pr.txt')
    return new_pg

#sanity checks for Pagerank
def sanity_pr(pg_test):
    k = 'jeff.dasovich@enron.com'
    v = 0.002059
    if v == round(pg_test[k], 6):
        result = True
    else:
        result = False
    print '\n#Pagerank'
    print 'Pagerank score of', k, 'is', round(pg_test[k], 6), 'and should be', v, '.'
    print 'Sanity Pagerank Test:', result, '\n'

#sanity checks for Hits
def sanity_hits(auth, hub):
    k = 'jeff.dasovich@enron.com'
    hv = 0.001005
    av = 0.000210
    if hv == round(hub[k], 6) and av == round(auth[k], 6):
        result = True
    else:
        result = False

    print '#Hits'
    print 'Hub score of', k, 'is', round(hub[k], 6), 'and should be 0.001006.'
    print 'Auth score of', k, 'is', round(auth[k], 6), 'and should be', av, '.'
    print 'Sanity Hits Result:', result, '\n'

#Useful General Statistics
def printStats():
    print 'Sum of all Pageranks', sum(pr.values())
    print 'Total number of emails', number_of_addresses()
    print 'Total number of sinks', len(sinks)

#export results to file
def export_results(dict_of_data, filename):
    f = open(filename, 'w')
    for k in sorted(dict_of_data, key=dict_of_data.get, reverse=True):
        s = str(round(dict_of_data[k], 6)) + ' ' + str(k) + '\n'
        f.write(s)
    f.close()


'''[1]'''
#return top k values of a dictionary
def top_k(dict_of_data, k):
    top = {}
    for name, score in nlargest(k, dict_of_data.iteritems(), key=itemgetter(1)):
        top[name] = score
    return top

#extract the top k nodes from the graph
def subgraph(d):
    top_ten = top_k(d, 10)
    prune(top_ten)

#prune the list by keeping the top 2 addresses that have been sent the most emails
def prune(t_10):
    d_graph = {}
    out = defaultdict(list)
    for k, v in t_10.iteritems():
        out[k] = outlink[k]
    for k in out:
        ''' [2] '''
        l_counts = [(out[k].count(x), x) for x in set(out[k])]
        l_counts.sort(reverse=True)
        l_result = [y for x, y in l_counts]
        if l_result:
            d_graph[k] = l_result[0:2]


outlink_counter = {}  # sent email counter
inlink_counter = {} # incoming emails counter
incoming = defaultdict(list)  # hash of inlinks eg incoming emails per person
outlink = defaultdict(list)  # hash of outlinks eg sent emails per person
addresses = set()  # unique nodes intersection of senders and receivers
sinks = set() # sink nodes

lamda = 0.8
iterations = 10

load_graph('graph.txt')
N = number_of_addresses()
pr = pagerank(iterations)
sanity_pr(pr) #sanity check for pagerank
printStats() #print some statistics
hits(iterations)