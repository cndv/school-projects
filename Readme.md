#School Projects
## Thesis
- BSc Informatics, Aristoteles University of Thessaloniki
    - Thesis: https://bitbucket.org/cndv/treasure-hunt/overview
- MSc Computer Science, University of Edinburgh
    - Thesis: https://bitbucket.org/cndv/vig-db
    
## Academic Projects 
 
### Postgraduate Projects:  
- Implementation of several database algorithms using atticaDB: External MergeSort, Hash-based Grouping, Merge-join, Grace Hash-join. JAVA  • Implementation of Log processing, relational database operations, and information retrieval algorithms in Hadoop. Python
- Information Retrieval Projects such as Scientific Document Retrieval with TFIDF, Pseudo Relevance Feedback, Similarity Detection. Link Analysis of Enron Email dataset using (Hits, PageRank) Python
- Parallel Implementation of Adaptive Quadrature, Bag of Tasks pattern. C, Open MPI
- Light algorithms and Normal vector transformations applied to a 3D Bunny model.  C++, OpenGL
 
 
### Bachelor Projects:  
- Detecting Distance-Based Outliers by analyzing Streams of Data algorithm implementation. JAVA
- Information Retrieval using Boolean model and a Parser. Java
- Database System Implementation project: processing queries, creating records, tables, and relations. C++
- Design and implementation of Prototype CMS E-Government Process Management Website. HTML/CSS, JavaScript, jQuery, PHP/MySQL (Codeigniter), MVC
- Creation of Sierpinski Triangle, a rotated cube and a house. C, OpenGL
- Slotted Aloha protocol simulation. C
 
 